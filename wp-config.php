<?php
define( 'WPCACHEHOME', '/home/vagrant/code/national-tyre-and-wheel/wp-content/plugins/wp-super-cache/' );
define( 'WP_CACHE', true ); // Added by WP Rocket

/**
 * NOTE: ensure you have run composer to install necessary packages
 */

require_once ABSPATH . 'wp-content/themes/ntaw/vendor/autoload.php';

// set up environment variables
$dotenv = Dotenv\Dotenv::create(ABSPATH);
$dotenv->load();
$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASSWORD'])->notEmpty();



// DB Config
//---------------------------------

define('DB_HOST', getenv('DB_HOST'));
define('DB_NAME', getenv('DB_NAME'));
define('DB_USER', getenv('DB_USER'));
define('DB_PASSWORD', getenv('DB_PASSWORD'));
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
define('DB_PREFIX', getenv('DB_PREFIX'));

$table_prefix = DB_PREFIX;



// Debug Config
//---------------------------------

define('APP_ENV', (string)getenv('APP_ENV'));
define('WP_DEBUG', (bool)(int)getenv('WP_DEBUG'));
define('WP_DEBUG_DISPLAY', (bool)(int)getenv('WP_DEBUG_DISPLAY'));
define('WP_DEBUG_LOG', (bool)(int)getenv('WP_DEBUG_LOG'));


// Logging
//---------------------------------

define('ERROR_EMAIL', getenv('ERROR_EMAIL'));
define('LOG_FILE_NAME', getenv('LOG_FILE_NAME'));
define('LOG_DB', (bool)(int)getenv('LOG_DB'));
define('LOG_DB_NAME', getenv('LOG_DB_NAME'));


// set up outputting of nice displaying of errors - https://github.com/filp/whoops
if (class_exists('\Whoops\Run') && defined('APP_ENV') && APP_ENV === 'local') {
    $whoops = new \Whoops\Run;
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    $whoops->register();
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '3{5E.<feWjc4|@>d&zQz1e|:|L^VzOl:@> V2LvL7!EkRgBy~vN2#IG-fn+BKT^v');
define('SECURE_AUTH_KEY', '=(8_ .Q&)|/66!~^Bz6 <A.e7f.n};Q 50KqTw7$zdx`4bVY_h`r%3D?T{E1pn-L');
define('LOGGED_IN_KEY', 'uWb.lV%l0_KaF8TcTrSEot]fX_Q10|uhf)gL|LaPR28]-)h!M9h<v|dT68!]kU7S');
define('NONCE_KEY', 'Op?5CfzA?p9gbsl-}TQaSzp8ri<>:r^4aBjtp/6JNn*?t1`T(5}!YF gS4QC?h7G');
define('AUTH_SALT', 'Y6mKQ=GM>#1L54m*-u(#9id(c;_-g&7(Wl_7X||U nLh=!&TU]HmSAT,7fT:8QCa');
define('SECURE_AUTH_SALT', '{E!Rb6<Dy2M#+A&#6:oDM|MCKJ6O8!5f({E9FAZS~#Rs*p2+iN|u-#B}T[X|Ty<k');
define('LOGGED_IN_SALT', 'JY|p9HP<LL|asmZscC1> dI?]KxYUd+NvoKm(O8SU*k&nNJ??V{v$~O,s|[FoOHI');
define('NONCE_SALT', 'I;-*j2(mb;&8~A:aLINzg?#f/@]ty<gyULp2*|xgU18;8EFeel1Qn+x5)}Qs?wXg');



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');



define('DISALLOW_FILE_EDIT', true);