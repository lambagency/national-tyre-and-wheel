const fs = require("fs");

const pkg = require("./package.json");

let oldVersion = "1.0.0";
const { version } = pkg;
const theme = pkg.name;

const files = ["README.md", `wp-content/themes/${theme}/style.css`];

async function getOldVersion() {
  return fs.readFile("README.md", "utf8", (e, data) => {
    if (e) {
      return console.log(e);
    }
    [oldVersion] = data.match(/(?<=version:) ?([\d.]+)/);
    oldVersion = oldVersion.replace(".", "\\.");
    return oldVersion;
  });
}

async function bumpVersion() {
  await getOldVersion();
  files.map(file =>
    fs.readFile(file, "utf8", (e, data) => {
      if (e) {
        return console.log(e);
      }

      const output = data.replace(new RegExp(oldVersion, "g"), version);
      return fs.writeFile(file, output, "utf8", er => {
        if (er) console.log(er);
      });
    })
  );
}

bumpVersion().then(() => console.log(`Updated versions to ${version}\n`));
