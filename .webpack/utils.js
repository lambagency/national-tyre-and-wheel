const path = require("path");

const pkg = require("../package.json");

const babelrc = require("../.babelrc");

const themePath = `wp-content/themes/${pkg.name}`;

const entry = {
  production: `./${themePath}/interface/js/main.js`
};

const outPath = path.resolve(process.cwd(), `${themePath}/interface/build`);

const resolve = {
  extensions: [".js", ".json", ".scss", ".svg"],
  // Should be kept up to date with eslint resolver moduleDirectory
  alias: {
    icons: path.resolve(process.cwd(), `${themePath}/interface/build/icons`),
    plugins: path.resolve(process.cwd(), `${themePath}/interface/js/plugins`),
    class: path.resolve(process.cwd(), `${themePath}/interface/js/class`)
  },
  symlinks: false
};

const rules = [
  {
    test: /(.js)$/,
    exclude: /node_modules/,
    use: [
      {
        loader: "babel-loader",
        options: {
          cacheDirectory: true,
          ...babelrc
        }
      }
    ]
  },
  {
    test: /\.hbs$/,
    loader: "handlebars-loader"
  },
  {
    test: /\.svg$/,
    use: [
      // TODO: investigate svg-sprite-loader for prod
      {
        loader: "svg-url-loader"
      },
      "svg-transform-loader"
    ]
  }
];

const sassPaths = [
  path.resolve(process.cwd(), `${themePath}/interface/scss/vendor`),
  path.resolve(process.cwd(), `${themePath}/interface/scss/settings`)
];

const GLOBALS = {
  // Keep up to date with ESLint Globals
  JSON_URL: JSON.stringify("/wp-json/la/v1/")
};

module.exports = { entry, outPath, resolve, rules, sassPaths, GLOBALS };
