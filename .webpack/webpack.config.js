const path = require("path");

const webpack = require("webpack");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const ManifestPlugin = require('webpack-manifest-plugin');

const sass = require("dart-sass");
const Fiber = require("fibers");

const pkg = require("../package.json");

const util = require("./utils");

module.exports = {
  mode: "development",
  devtool: "inline-cheap-module-source-map",
  context: path.resolve(process.cwd()),
  entry: util.entry,
  output: {
    filename: "[name].js",
    chunkFilename: "[name].bundle.js",
    path: util.outPath,
    publicPath: "http://localhost:8080/"
  },
  module: {
    rules: [
    ...util.rules,
    {
      test: /\.scss$/,
      use: [
        "style-loader",
        {
          loader: "css-loader",
          options: {
            sourceMap: true
          }
        },
        {
          loader: "postcss-loader",
          options: {
            sourceMap: true
          }
        },
            "cache-loader",
        {
          loader: "sass-loader",
          options: {
            includePaths: util.sassPaths,
            implementation: sass,
            fiber: Fiber,
            sourceMap: true
          }
        }
      ]
    }
  ]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        foundation: {
          test: /^node_modules\/foundation-sites.*/,
          chunks: "all",
          reuseExistingChunk: true
        }
      }
    }
  },
  plugins: [
    new BundleAnalyzerPlugin(),
    new webpack.DefinePlugin({
      ...util.GLOBALS
    }),
    new webpack.HotModuleReplacementPlugin(),
    new ManifestPlugin({
      writeToFileEmit: true
    })
  ],
  resolve: util.resolve,
  devServer: {
    disableHostCheck: true,
    headers: { "Access-Control-Allow-Origin": "*" },
    hot: true,
    compress: true
  }
};
