const path = require('path');

const webpack = require('webpack');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const ManifestPlugin = require('webpack-manifest-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const sass = require("dart-sass");
const ExtractCSS = require("mini-css-extract-plugin");
const OptimizeCSS = require("optimize-css-assets-webpack-plugin");
const Fiber = require('fibers');

const Uglify = require("uglifyjs-webpack-plugin");

const pkg = require("../package.json");

const util = require("./utils");

// Grabs "$BASE_PATH" from the command line to use as the public path of the site
const BASE = process.env.BASE_PATH || "";

module.exports = {
  mode: "production",
  context: path.resolve(process.cwd()),
  entry: util.entry,
  output: {
    filename: '[name].[contenthash:6].js',
    chunkFilename: "[name].[contenthash:6].bundle.js",
    path: util.outPath,
    publicPath: `${BASE}/wp-content/themes/${pkg.name}/interface/build/`,
  },
  module: {
    rules: [
      ...util.rules,
      {
        test: /\.scss$/,
        use: [
          ExtractCSS.loader,
          "css-loader",
          "postcss-loader",
          {
            loader: "sass-loader",
            options: {
              includePaths: util.sassPaths,
              implementation: sass,
              fiber: Fiber
            },
          },
        ],
      },
    ]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        default: false,
        style: {
          name: "style",
          test: /\.s?[ca]ss$/,
          chunks: 'all',
          priority: 10,
          enforce: true
        }
      }
    },
    minimizer: [
      new Uglify({
        extractComments: true,
        parallel: true,
        cache: true,
      }),
      new OptimizeCSS({}),
    ],
  },
  plugins: [
    new CleanWebpackPlugin(
      [util.outPath],
      {
        root: path.resolve(process.cwd()),
        exclude: `${util.outPath}/icons/** ${util.outPath}/build/templates/**`
      }
    ),
    new ExtractCSS({
      filename: "style.[contenthash:6].min.css",
      chunkFilename: "style.[contenthash:6].min.css"
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: "static"
    }),
    new webpack.DefinePlugin({
      JSON_URL: JSON.stringify(`${BASE}/wp-json/la/v1/`)
    }),
    new webpack.HashedModuleIdsPlugin(),
    new ManifestPlugin(),
  ],
  resolve: util.resolve
}
