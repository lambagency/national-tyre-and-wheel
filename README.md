<!--- version:0.0.1 -->

# Lamb Wordpress Build 0.0.1

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

## About

This is a Wordpress boilerplate designed to allow usage of ACF-based modules with modern OOP-based backing.

Module design is intended to align with https://projects.invisionapp.com/d/main#/projects/prototypes/13090924

## Setup

- Run build.sh from inside your VM (Homestead recommended)
  - You may have to use VIM to change the line endings so that it's readable on Linux systems (see [this article](https://lambagency.atlassian.net/wiki/spaces/LAMB/pages/11540176/Setup+a+New+WordPress+Project+WIP) "Resolving Line Ending Issue" section)
- Enter relevant details during the install process
- Once the new project has been created, install project dependencies
  - `composer install` from project root in VM
  - `npm install` from project root in host machine
- Setup site in Homestead
  - Under 'sites' add a new entry:

```
- map: sitename.local
  to: /home/vagrant/code/project-folder
  php: "7.3"
```

- You're ready to go! Just run `npm start` from the host machine in the project root to start developing

## New Blocks

- This site uses Gutenberg with ACF blocks. In order to easily bootstrap blocks, a WP CLI command is included.
- Run `wp block new "module name"` to create the PHP template, PHP class, ACF JSON group, SCSS and JS files
- Several blocks can be bootstrapped at once `wp block new x y`
- Structure can be simplified by specifying files to ignore when creating blocks: `wp block new cat --ignore=all`
- More options can be found by running `wp help block new`

## Import Blocks

- This site uses optional modules that are imported from our Opt-in Modules repo. To use the import feature, follow the steps below:
- Setup - clone the repo from https://bitbucket.org/lambagency/opt-in-modules/src/master/ into your work folder. It must be in the same root folder as your other projects for the import command to function.
- Run `wp block import *MODULE FOLDER NAME HERE*` to import the module into you modules folder
- Import multiple blocks by including their folder names separated by a space: `wp block import accordion wayfinder othername`
- Once your modules are imported, remember to click 'Sync Available' in the Wordpress ACF admin to complete the import.
- The importer will notify you in the command line if there are any issues.

## Icons

- SVGs will be loaded by svg-url-loader if they are included as `url()` arguments in `.scss` files and the icons folder is aliased to `~icons` so they can be added like: `background-image: url(~icons/icons_angle-down.svg)`
- SVG backgrounds can be manipulated by using `-svg-` prefixed css variables (see: https://www.npmjs.com/package/svg-transform-loader)

## Staging Deploy

We use Bitbucket Pipelines for our CI deployments. This repo comes with a a pre-built config for Pipelines that will install dependencies, build files and deploy to a staging server. Before this will work, a few things will need to be configured.

- Each site should be deployed to its own hosting account - request a new hosting area from Nick.
- Add the hosting domain to Pipelines' known hosts
  - Bitbucket > Repository > Settings > Pipelines/SSH Keys
  - Add domain into "Host Address" field (eg white.fweb.com.au)
  - Click "Fetch"
  - Click "Add Host" after it finishes fetching
- Add Host variables to Pipelines' staging environment
  - Bitbucket > Repository > Settings > Pipelines/Deployments
  - Expand "Staging"
  - Add new variables to the environment
    - USR | {{ enter username here }} [x] Secured
    - HOST | {{ enter host domain here (no http: required) }} [ ] Secured
- Pipelines will not deploy a database, .htaccess, .env or wp-config.php file - these will have to be updated through FTP or CPanel.

## Usage Notes

- Webpack Dev Server is set to hot-reload scripts, but there will be items like the form and banner animation that do not work with hot reload
- Set `APP_ENV = local` in the .env file for development and `APP_ENV = production` for production
- You can easily update the version in `package.json`, `README.md` and in `theme-name/style.css` by using `npm version` (https://docs.npmjs.com/cli/version)
- Code style in JS and SCSS files are loosely enforced by `eslint` and `stylelint`. Check how well you've done keeping to the standards by running `npm run lint:css` or `npm run lint:js` - syntax fixes can be done with Prettier by running `npm run format`

## TODO

See https://lambagency.atlassian.net/browse/WB for the current state of affairs.
