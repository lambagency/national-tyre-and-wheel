<?php
    get_header();

    $queried_object = get_queried_object();

    if ( $queried_object ) {
        $post_id = $queried_object->ID;
       
        // Fetch all data of member
        $member_data = new \LambAgency\PostType\TeamMemberGlobal($post_id);
        $button_colour_scheme = "";
        $has_company_theme = false;
        $attached_company = $member_data->company;

        if (have_rows('vcard_list', 'options')) {
            while( have_rows('vcard_list', 'options')) {
                $row = the_row(true);
    
                if ($row['enable_theme'] && $row['company_name'] == $attached_company) {
                    $has_company_theme = true;
                    //company theme
                    $company = array(
                        'company_name'          => $row['company_name'],
                        'header_color_scheme'   => $row['header_settings']['header_color_scheme'],
                        'header_image'          => $row['header_settings']['header_image'],
                        'footer_color_scheme'   => $row['footer_settings']['footer_color_scheme'],
                        'footer_image'          => $row['footer_settings']['footer_image'],
                        'button_colour_scheme'  => $row['button_colour_scheme'],
                        'primary_font_name'     => $row['primary_font_name'],
                        'secondary_font_name'   => $row['secondary_font_name'],

                        'page_header_background'  => $row['background_color'],
                        'page_header_logo'  => $row['logo'],
                        'page_logo_link'  => $row['logo_link']

                    );                    
                    
                }
            }
        }

        $page_header_background = get_field( 'background_color', 'option');
        $page_header_logo = get_field( 'page_logo', 'option' );
        $page_logo_link = get_field( 'logo_link', 'option' );

        if ($has_company_theme && !is_null($company)) {

            // Custome header setup start 
            $page_header_background = $company['page_header_background'];
            $page_header_logo = $company['page_header_logo'];
            $page_logo_link = !empty($company['page_logo_link']) ? $company['page_logo_link'] : get_field( 'logo_link', 'option' );

            // Primary Font
            if ($company['primary_font_name'] && $company['primary_font_name'] != "") {
                $p_font_url = str_replace(' ', '+', trim($company['primary_font_name']));
                $p_font_name = str_replace('+', ' ', trim($company['primary_font_name']));

                echo '<link href="https://fonts.googleapis.com/css2?family='. $p_font_url .':wght@600&display=swap" rel="stylesheet">';
                echo '<style>
                        .vcard-body { font-family: ' . $p_font_name .';'. 'font-weight: 600;'.'}
                        .vcard-body .pusher, .single-team-member .vcard-body .address a,
                        .single-team-member .vcard-body .contact .button,
                        .single-team-member .vcard-body .contact .button.download
                        { font-family: inherit; font-weight: 600; }
                    </style>';
            }
            // Secondary Font
            if ($company['secondary_font_name'] && $company['secondary_font_name'] != "") {
                $s_font_url = str_replace(' ', '+', trim($company['secondary_font_name']));
                $s_font_name = str_replace('+', ' ', trim($company['secondary_font_name']));

                echo '<link href="https://fonts.googleapis.com/css2?family='. $s_font_url .':wght@300&display=swap" rel="stylesheet">';
                echo '<style>
                .single-team-member .vcard-body .general .small-label, .single-team-member .vcard-body .address1 
                { font-family: ' . $s_font_name .';'. 'font-weight: 300;'.'}
                .single-team-member .vcard-body .address p { font-family: inherit; }
                </style>';
            }

            // Header styles
            $header_style = "background-image:  linear-gradient(0deg, " . 
                            $company['header_color_scheme'] .", ". 
                            $company['header_color_scheme'] . "), 
                            url(". $company['header_image'] . ");";
            // Footer Styles
            $footer_style = "background-color: ". $company['footer_color_scheme'] . 
                            "; background-image: url(". $company['footer_image'] . ");";
            
            // Button Styles
            $button_colour_scheme = $company['button_colour_scheme'];
  
        } 

        $name = $member_data->title;

        // Define class list for social icons
        $social_class_list = 'medium-2 small-3';
        
        // If all 3 socials have data, make them larger
        if (!empty($member_data->teams) && !empty($member_data->linkedin) && !empty($member_data->mobile_no)) {
            $social_class_list = 'medium-4 small-3';
        }

        // Convert featured image to base64
        $path = $member_data->getFeaturedImage('square-medium');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = base64_encode($data);

        // Create new image property for members object
        $member_data->image = $base64;
        $member_data->description = '';
        $member_data->brandUrl = $page_logo_link;

        $member = json_encode($member_data, 15, 512);

    }
    function sanitizeText($param){
        return strlen($param)? $param . ', ':'';
    }
?>

<header data-autoload-snippet="header">
    <div class="header-top v-header-top" style="background:<?php echo $page_header_background ?>;">
        <div class="grid-container">
            <div class="grid-x align-middle align-center header-top-grid">
                <div class="header-logo">
                    <a href="<?php echo $page_logo_link ?>" title="<?php echo $company_name ?>">
                        <img src="<?php echo $page_header_logo ?>" height="72px" alt="<?php echo $company_name ?>">
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Custome header setup end -->


<div class="single-team-member <?php echo $member_data->company; ?>" data-autoload-single="single-team-member">
    <input type="hidden" id="member" value='<?php echo $member ?>' />
    <div class="vcard text-center">
        <div class="vcard-header" style ="<?php (!is_null($header_style)) ? print_r($header_style) : ''; ?>">
        </div>
       


        <div class="vcard-body">
            <div class="grid-container">
                <div class="general">
                    <div class="profile-picture-wrapper">
                        <?php if (strlen($path) > 0) { ?>
                            <img src="<?php echo $path; ?>" alt="<?php echo $name; ?>">
                        <?php } else {?>
                            <!-- <img src="https://ntaw.com.au/wp-content/uploads/2022/06/no.png" alt=""> -->
                        <?php } ?>

                    </div>
                    <h2><?php echo $name; ?></h2>
                    <p class="small-label"><?php echo $member_data->position; ?></p>
                </div>

                <div class="contact">
                    <div class="grid-x align-center">
                        <div class="cell medium-8 small-12">
                            <div class="grid-x grid-margin-x grid-margin-y">
                                <div class="cell medium-3 small-4">
                                    <a class="button theme-secondary mobile" href="tel:<?php echo $member_data->mobile_no; ?>">Mobile</a>
                                </div>

                                <div class="cell medium-3 small-4">
                                    <a class="button theme-secondary email" href="mailto:<?php echo $member_data->email; ?>">Email</a>
                                </div>

                                <div class="cell medium-3 small-4">
                                    <a class="button theme-secondary map" href="https://www.google.com/maps/place/<?php echo sanitizeText($member_data->street) .
                                    sanitizeText($member_data->suburb) . $member_data->state . ' ' . $member_data->postcode . ' ' . $member_data->country; ?>">Visit</a>
                                </div>

                                <div class="cell medium-3 small-12">
                                    <a class="button theme-primary download" style="background: <?php echo $button_colour_scheme;?>">Download</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="address">
                    <div class="grid-x align-middle align-center grid-margin-x">
                        <div class="cell medium-3 small-5">
                            <div class="grid-x align-center">
                                <div class="qr">
                                    <canvas id="qrcode"></canvas>
                                </div>
                            </div>
                        </div>

                        <div class="cell medium-4 small-7">
                            <div class="grid-y">
                                <a href="sms:<?php echo $member_data->office_no; ?>"><?php echo $member_data->office_no; ?></a>

                                <div class="pusher" style="color: <?php echo $button_colour_scheme;?>">
                                    <?php echo sanitizeText($member_data->street) . '<br>'.
                                    sanitizeText($member_data->suburb) . $member_data->state . ' ' . $member_data->postcode . ' ' . $member_data->country; ?>
                                </div>
                            
                                <div class="address1">
                                    <div>
                                        <p><?php echo sanitizeText($member_data->postal_street) ?></p>
                                    </div>
                                    <div>
                                        <p><?php echo sanitizeText($member_data->postal_suburb) . $member_data->postal_state . ' ' . $member_data->postal_postcode . ' ' . $member_data->postal_country; ?></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="vcard-footer" style ="<?php (!is_null($footer_style)) ? print_r($footer_style) : ''; ?>">
            <div class="grid-container">
                <div class="grid-x align-center">
                    <div class="cell medium-8 small-12">
                        <div class="grid-x align-center">
                            <?php if (!empty($member_data->teams)) {?>
                                <div class="cell <?php echo $social_class_list?>">
                                    <a class="teams" href="<?php echo $member_data->teams; ?>"></a>
                                </div>
                            <?php } ?>
                            <?php if (!empty($member_data->linkedin)) {?>
                                <div class="cell <?php echo $social_class_list?>">
                                    <a class="linkedin" href="<?php echo $member_data->linkedin; ?>"></a>
                                </div>
                            <?php } ?>
                            <?php if (!empty($member_data->mobile_no)) {?>
                                <div class="cell <?php echo $social_class_list?>">
                                    <a class="sms" href="sms:<?php echo $member_data->mobile_no; ?>"></a>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer();?>
