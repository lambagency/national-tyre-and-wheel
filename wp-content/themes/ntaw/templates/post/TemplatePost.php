<?php

namespace LambAgency\Template;

use LambAgency\Utility;

/**
 * Class Post
 *
 * @package LambAgency\Template
 */
class Post extends Template
{

    /** @var string The name of the template */
    public static $name = 'Post';

    /**
     * Path to handlebars template from templates directory, exclude leading slash
     *
     * @var string
     * @example post/post.hbs
     */
    public $templatePath = 'post/post.hbs';

    /** @var array Post types which template is available for */
    public static $availablePostTypes = [
        'post'
    ];


    /**
     * Set the template data for use in the handlebars template
     */
    public function setTemplateData()
    {
        $image = Utility::getFeaturedImageArray($this->post);
        $imageURL = $image ? $image['sizes']['square-medium'] : '';

        $this->templateData = [
            'title'     => get_the_title($this->post),
            'image'     => $imageURL,
            'date'      => get_the_date('d F Y', $this->post),
            'permalink' => get_permalink($this->post)
        ];
    }
}
