<?php

namespace LambAgency\Template;

/**
 * Class Post
 *
 * @package LambAgency\Template
 */
class Team extends Template
{

    /** @var string The name of the template */
    public static $name = 'Team Member';

    /**
     * Path to handlebars template from templates directory, exclude leading slash
     *
     * @var string
     * @example team/team.hbs
     */
    public $templatePath = 'team/team.hbs';

    /** @var array Post types which template is available for */
    public static $availablePostTypes = [
        'team-member'
    ];

    /**
     * Set the template data for use in the handlebars template
     */
    public function setTemplateData()
    {
        $teamMember = new \LambAgency\PostType\TeamMember($this->post);
        $image = $teamMember->getFeaturedImage('square-medium');

        $this->templateData = [
            'name'          =>  $teamMember->title,
            'position'      =>  $teamMember->getPosition(),
            'description'   =>  $teamMember->getDescription(),
            'image'         =>  $image,
            'permalink'     =>  $teamMember->getPermalink()
        ];
    }
}
