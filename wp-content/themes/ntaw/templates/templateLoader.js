export default class TemplateLoader {
  constructor({ template, data }) {
    this.template = template;
    this.data = data;
  }

  async getTemplate(template = this.template) {
    // Leverages handlebars-loader
    return import(/* webpackChunkName: "template-[request]" */ `./${template}/${template}.hbs`).then(
      t => t.default
    );
  }

  async getCompiled() {
    return this.getTemplate(this.template).then(t => t(this.data));
  }
}
