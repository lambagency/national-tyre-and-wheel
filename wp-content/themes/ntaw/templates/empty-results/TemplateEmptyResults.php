<?php

namespace LambAgency\Template;

class EmptyResults extends Template
{

    /** @var string The name of the template */
    public static $name = 'Empty Results';

    /**
     * Path to handlebars template from templates directory, exclude leading slash
     *
     * @var string
     * @example post/post.hbs
     */
    public $templatePath = 'empty-results/empty-results.hbs';

    /** @var array Post types which template is available for */
    public static $availablePostTypes = [];

    /**
     * Set the template data for use in the handlebars template
     */
    public function setTemplateData()
    {
        $this->templateData = [
            'message'     => "Sorry, we could't find any posts matching your search.",
        ];
    }
}
