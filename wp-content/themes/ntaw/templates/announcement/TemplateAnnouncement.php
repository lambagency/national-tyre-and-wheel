<?php

namespace LambAgency\Template;

/**
 * Class Post
 *
 * @package LambAgency\Template
 */
class Announcement extends Template
{

    /** @var string The name of the template */
    public static $name = 'Announcement';

    /**
     * Path to handlebars template from templates directory, exclude leading slash
     *
     * @var string
     * @example team/team.hbs
     */
    public $templatePath = 'announcement/announcement.hbs';

    /** @var array Post types which template is available for */
    public static $availablePostTypes = [
        'announcement'
    ];

    /**
     * Set the template data for use in the handlebars template
     */
    public function setTemplateData()
    {
        $announcement = new \LambAgency\PostType\Announcement($this->post);
        $category = get_the_category($announcement->getPostID());

        $category[0]->name == 'Corporate Governance' ? $tab = $announcement->getFields()['subcategory'] : $tab = substr($announcement->getFields()['date'], -4);

        $category[0]->name == 'Annual Report' ? $date = null : $date = $announcement->getFields()['date'];

        $this->templateData = [
            'tab'       => $tab,
            'category'  => $category[0]->name,
            'date'      => $date,
            'title'     => html_entity_decode($announcement->getTitle()),
            'file'      => $announcement->getFields()['file']['url'],
        ];
    }
}
