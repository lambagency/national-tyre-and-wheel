import AbstractSingle from "class/AbstractSingle";
import VCard from 'vcard-creator'
import QRious from "qrious";

export default class SingleTeamMember extends AbstractSingle {
    constructor(single) {
        super(single);

        this.single = this.single;

        this.initCustomHeader();
        this.initVCard();
        this.initQRCode();
        this.initCustomFooter();
    }

    /**
     * Modify default header accordion ot vCard design requirement
     */
    initCustomHeader() {
        const header = this.single.parentElement.previousElementSibling
        header.classList.add('hide')
        // const headerTop = header.querySelector('.header-top')

        // header.classList.add('p-absolute')
        // headerTop.classList.add('v-header-top')
    }

    /**
     * Initialize vCard
     */
    initVCard() {    
        // Fetch member's data
        const member = JSON.parse(this.single.querySelector('#member').value)
        const name = member.title.split(" ");
        const fullname = member.title;

        // Define a new vCard
        const myVCard = new VCard()
    
        // Add data
        myVCard
            .addName(name.pop(), name[0], '', '', '') // lastname, firstname, additional, prefix, suffix
            .addPhoto(member.image, 'PNG')
            .addCompany('National Tyre and Wheel')
            .addJobtitle(member.position)
            // .addRole('Data Protection Officer')
            .addEmail(member.email, 'WORK')
            .addPhoneNumber(member.office_no, 'WORK')
            .addPhoneNumber(member.mobile_no)
            .addAddress('', '', member.postal_street, member.postal_suburb, member.postal_state, member.postal_postcode, member.postal_country, 'POSTAL')
            .addAddress('', '', member.street, member.suburb, member.state, member.postcode, member.country, 'WORK')
            .addURL(member.brandUrl)
            // console.log(myVCard.toString(), member)
    
        // Download file
        const downloadLink = this.single.querySelector('.download')
    
        downloadLink.addEventListener('click', (e) => {
            e.target.href = 'data:attachment/text,' + encodeURI(myVCard)
            e.target.target = '_blank'
            e.target.download = `${fullname}.vcf`
        })
    }

    /**
     * Modify default footer accordion ot vCard design requirement
     */
     initQRCode() {    
        new QRious({
            element: this.single.querySelector('#qrcode'),
            value: window.location.href
        });
    }

    /**
     * Modify default footer accordion ot vCard design requirement
     */
    initCustomFooter() {
        const footer = this.single.parentElement.nextElementSibling
        
        footer.classList.add('hide')
    }
}
