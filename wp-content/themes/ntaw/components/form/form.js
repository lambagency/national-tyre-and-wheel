import AbstractComponent from "class/AbstractComponent";

import Fields from "./fields";

/**
 * Form Component Class
 */
export default class ComponentForm extends AbstractComponent {
  constructor(component) {
    super(component);
    this.initFields();
  }

  /**
   * Initialise the form fields
   */
  initFields() {
    this.fields = new Fields(this.component);
  }
}
