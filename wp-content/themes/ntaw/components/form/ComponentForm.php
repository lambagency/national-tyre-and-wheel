<?php

namespace LambAgency\Components;

/**
 * Class Form
 *
 * @package LambAgency\Components
 */
class Form extends Component
{
    private $form;
    private $formButtonTheme = '';
    private $componentClass = [];


    public function __construct($component)
    {
        parent::__construct();

        $this->setupForm($component);
        $this->setFormButtonTheme($component);
        $this->setFormButtonClass();
    }


    /**
     * Setup the form
     * @param $component
     */
    private function setupForm($component)
    {
        $this->form = $component['cf_form'];
    }


    /**
     * Get the gravity form ID
     *
     * @return int
     */
    public function getGravityFormID()
    {
        return $this->form['id'];
    }


    /**
     * Returns the component's classes in string format
     *
     * @param array|string $addClasses
     *
     * @return string
     */
    public function getComponentClass($addClasses = '')
    {
        if (is_array($addClasses)) {
            $addClasses = implode(' ', $addClasses);
        }

        return trim(implode(' ', $this->componentClass) . ' ' . $addClasses);
    }


    /**
     * Get the form button theme
     *
     * @return string
     */
    public function getFormButtonTheme()
    {
        return $this->formButtonTheme;
    }



    /**
     * Get the form button theme
     *
     * @param $component
     */
    public function setFormButtonTheme($component)
    {
        $this->formButtonTheme = !empty($component['cf_form_button_theme']) ? $component['cf_form_button_theme'] : $this->formButtonTheme;
    }


    /**
     * Adds to form class array if form theme exists
     */
    private function setFormButtonClass()
    {
        $submitButtonFilter = 'gform_submit_button_' . $this->getGravityFormID();
        add_filter($submitButtonFilter, array(&$this, 'gravityFormButtonTheme'), 10, 2);
    }


    /**
     * Theme the gravity form submit button
     * @param       $button
     * @param array $form
     * @return string
     */
    public function gravityFormButtonTheme($button, array $form)
    {
        $button = str_replace("class='", "class='button " . $this->getFormButtonTheme() . ' ', $button);

        return $button;
    }


    /**
     * Get the gravity form
     *
     * @return mixed|string
     */
    public function getGravityForm()
    {
        $gravityFormString = '';
        if (function_exists('gravity_form')){
            $gravityFormString = gravity_form($this->getGravityFormID(), false, false, false, [], false, 1, false);
        }

        return $gravityFormString;
    }



    /**
     * Echos the HTML string for the component
     */
    public function renderForm()
    {
        include locate_template(COMPONENTS . '/form/form.php');
    }


    /**
     * Dynamically populates form button theme options for the form button theme field
     *
     * @param array $field
     *
     * @return array
     */
    public static function formButtonThemeChoices(array $field)
    {
        $field['choices'] = [];

        foreach (\LambAgency\Components\Button::$buttonThemes as $key => $value) {
            $field['choices'][$key] = $value;
        }

        return $field;
    }
}

// Populate the form button theme choices
add_filter('acf/load_field/name=cf_form_button_theme', __NAMESPACE__ . '\Form::formButtonThemeChoices');
