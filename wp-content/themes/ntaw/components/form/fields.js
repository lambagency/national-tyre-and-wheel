import Field from "./field";

export default class Fields {
  constructor(scope = document) {
    this.scope = scope;
    this.autoloadFields();
  }

  autoloadFields() {
    this.fields = this.scope.querySelectorAll("input, textarea, select");

    // Reversed loops are faster. IE can't forEach on nodeLists.
    for (let i = this.fields.length - 1; i >= 0; i -= 1) {
      const field = this.fields[i];
      new Field(field);
    }
  }
}
