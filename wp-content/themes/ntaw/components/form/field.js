export default class Field {
  constructor(field) {
    this.listeners = [];
    this.field = field;
    this.wrapper = field.parentNode.parentNode;
    this.addFieldListener();
  }

  addFieldListener() {
    let keyup;
    let keydown;
    let change;
    this.field.addEventListener("keyup", (keyup = () => this.fieldListener()));
    this.field.addEventListener(
      "keydown",
      (keydown = () => this.fieldListener())
    );
    this.field.addEventListener(
      "change",
      (change = () => this.fieldListener())
    );
    this.fieldListener(); // Run on initial load

    if (module.hot) {
      this.listeners.push(
        {
          type: "keyup",
          element: this.field,
          listener: keyup
        },
        {
          type: "keydown",
          element: this.field,
          listener: keydown
        },
        {
          type: "change",
          element: this.field,
          listener: change
        }
      );
    }
  }

  fieldListener() {
    if (this.field.checked || this.field.value) {
      this.field.classList.add("dirty");
      this.wrapper.classList.add("dirty");
    } else {
      this.field.classList.remove("dirty");
      this.wrapper.classList.remove("dirty");
    }
  }
}
