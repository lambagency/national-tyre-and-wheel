<?php
/** @var \LambAgency\Components\Form $this */
?>

<div data-autoload-component="form"
     data-autoload-priority="1"
     class="component-form <?php echo esc_attr($this->getComponentClass()); ?>">
    <?php echo $this->getGravityForm(); ?>
</div>
