<?php
/** @var \LambAgency\Components\Button $this */

$buttonLink = $this->getButtonLink();
?>

<?php if (!empty($buttonLink)) : ?>
    <a href="<?php echo esc_url($buttonLink->getURL()); ?>"
        class="button <?php echo esc_attr($this->getButtonClasses()); ?>"
        data-autoload-component="button">
        <span><?php echo esc_html($buttonLink->getTitle());?></span>
    </a>
<?php endif; ?>
