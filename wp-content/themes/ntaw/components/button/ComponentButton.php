<?php

namespace LambAgency\Components;

/**
 * Class Button
 *
 * @package LambAgency\Components
 */
class Button extends Component
{

    /**
     * Map of button themes
     * Key should match the button theme scss class name
     * Value should be the pretty name to display to the end user
     *
     * @var array
     */
    public static $buttonThemes = [
        'theme-primary'     => 'Primary',
        'theme-secondary'   => 'Secondary',
    ];


    /** @var  \LambAgency\Components\Link */
    protected $buttonLink;

    protected $buttonTheme;

    protected $buttonClasses = [];


    public function __construct($component)
    {
        parent::__construct();

        $this->setButtonLink($component);
        $this->setButtonTheme($component);

        $this->setButtonClasses();
    }


    /**
     * Get the button link
     *
     * @return \LambAgency\Components\Link
     */
    public function getButtonLink()
    {
        return $this->buttonLink;
    }


    /**
     * Set the button link
     *
     * @param $component
     */
    private function setButtonLink($component)
    {
        $this->buttonLink = new Link($component['cb_link']);
    }


    /**
     * Get the button theme
     *
     * @return string
     */
    public function getButtonTheme()
    {
        return $this->buttonTheme;
    }


    /**
     * Set the button theme
     *
     * @param $component
     */
    public function setButtonTheme($component)
    {
        $this->buttonTheme = !empty($component['cb_theme']) ? $component['cb_theme'] : $this->buttonTheme;
    }


    /**
     * Get the button classes
     *
     * @return string
     */
    public function getButtonClasses()
    {

        $buttonClassesString = trim(implode(' ', $this->buttonClasses));
        $buttonClassesString = $buttonClassesString ? ' ' . $buttonClassesString : '';

        return $buttonClassesString;
    }


    /**
     *  Sets the button classes
     */
    private function setButtonClasses()
    {
        // Button Theme
        !empty($this->buttonTheme) ? $this->buttonClasses[] = $this->buttonTheme : false;

        // Button Style
        !empty($this->buttonStyle) ? $this->buttonClasses[] = 'style-' . $this->buttonStyle : false;
    }


    /**
     * Echos the HTML string for the current button
     */
    public function renderButton()
    {
        include locate_template(COMPONENTS . '/button/button.php');
    }


    /**
     * Dynamically populates button theme options for the button theme field
     *
     * @param array $field
     *
     * @return array
     */
    public static function buttonThemeChoices(array $field)
    {
        $field['choices'] = [];

        foreach (self::$buttonThemes as $key => $value) {
            $field['choices'][$key] = $value;
        }

        return $field;
    }
}

// Populate the button theme choices
add_filter('acf/load_field/name=cb_theme', __NAMESPACE__ . '\Button::buttonThemeChoices');
