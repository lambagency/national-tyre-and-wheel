import AbstractComponent from "class/AbstractComponent";

export default class Icon extends AbstractComponent {
  constructor(component) {
    super(component);
    // console.log('initialising icon', this);
    this.iconName = this.component.getAttribute("data-icon-name");
    // this.initIcon();
  }

  initIcon() {
    // import(/* webpackChunkName: "icon-[request]" */ `icons/${
    //   this.iconName
    // }`).then(icon => {
    //   this.iconURI = decodeURIComponent(
    //     icon.default.match(/(?<=data:image\/svg\+xml,)[^"]+/)[0]
    //   );
    //   this.domElement = document
    //     .createRange()
    //     .createContextualFragment(this.iconURI);
    //   this.domElement.querySelector("svg").classList.add("icon");
    //   this.component.parentNode.replaceChild(this.domElement, this.component);
    //   this.component = this.domElement;
    // });
  }
}
