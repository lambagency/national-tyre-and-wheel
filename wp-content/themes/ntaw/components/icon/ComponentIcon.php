<?php

namespace LambAgency\Components;

/**
 * Class Icon
 *
 * @package LambAgency\Components
 */
class Icon extends Component
{
    private static $iconsDir = INTERFACE_BUILD_PATH . 'icons/';

    private $icon = '';

    /**
     * Arguments to be passed into wp_kses to sanitise SVGs.
     * https://wordpress.stackexchange.com/a/316943
     *
     * @var array $svg_args
     */
    public static $svg_args = [
      'svg'   => [
        'class' => true,
        'xmlns' => true,
        'width' => true,
        'height' => true,
        'viewbox' => true,
      ],
      'g'     => [ 'id' => true ],
      'title' => [ 'title' => true ],
      'path'  => [
        'd' => true,
        'fill' => true,
        'class' => true,
      ],
    ];


    public function __construct($component)
    {
        parent::__construct();

        $this->setIcon($component);

        $this->registerACF();
    }


    /**
     * Set the icon
     *
     * @param $component
     */
    public function setIcon($component)
    {
        $this->icon = !empty($component['ci_icon']) ? $component['ci_icon'] : $this->icon;
    }


    /**
     * Render the SVG icon
     */
    public function render()
    {
        $this::renderSVG($this->icon);
    }


    /**
     * Render an SVG icon from the icons build directory
     *
     * @param string $iconName
     * @param bool $return
     *
     * @return string
     */
    public static function renderSVG($iconName, $return = false)
    {
        $output = '';

        $paths = array_merge([self::$iconsDir], self::getSubfolders());

        $iconPath = null;

        foreach ($paths as $path) {
            $thisIconPath = $path . $iconName . '.svg';
            if (file_exists($thisIconPath) && is_readable($thisIconPath)) {
                $iconPath = $thisIconPath;
                break;
            }
        }

        if ($iconPath !== null) {
            $iconXML = file_get_contents($iconPath); // Suppress warnings
            $output = $iconXML ? : '';
        } else {
            $output = '<svg data-autoload-component="icon" class="icon" data-icon-name="' . $iconName . '"></svg>';
        }

        if ($return) {
            return wp_kses( $output, self::$svg_args );
        } else {
            echo wp_kses( $output, self::$svg_args );
        }
    }

    /**
     * Gets an array of folders within the icon directory
     *
     * @return array
     */
    public static function getSubfolders()
    {
        $cached = wp_cache_get('icon_folders', __NAMESPACE__);

        if ($cached) {
            return $cached;
        }

        $folders = [];
        $files = scandir(self::$iconsDir);

        foreach ($files as $path) {
            if ($path === "." || $path === "..") {
                continue;
            }
            $folder = self::$iconsDir . $path;

            if (is_dir($folder)) {
                $folders[$path] = $folder . '/';
            }
        }

        wp_cache_set('icon_folders', $folders, __NAMESPACE__, 60);

        return $folders;
    }

    /**
     * Fetch all SVGs that are a direct descendent of a specified folder
     *
     * @param string $folder
     *
     * @return array
     */
    public static function getIconsFromFolder($folder)
    {
        $icons = [];

        if (!file_exists($folder)) {
            return $icons;
        }

        $files = scandir($folder);

        if ($files) {
            foreach ($files as $icon) {
                if ($icon === "." || $icon === "..") {
                    continue;
                }
                // check for '.svg' file extension
                $fileParts = pathinfo(self::$iconsDir . $icon);

                if (isset($fileParts['extension']) && $fileParts['extension'] == 'svg') {
                    $icons[] = [
                        'value' => $fileParts['filename'],
                        'label' => ucwords(str_replace('-', ' ', $fileParts['filename']))
                    ];
                }
            }
        }

        return $icons;
    }

    /**
     * Get a list of all icons in the icons dist folder
     *
     * @return array
     */
    public static function getIcons()
    {
        $icons = [];

        // Get icons from icon directory root
        $icons = array_merge($icons, self::getIconsFromFolder(self::$iconsDir));

        $files = scandir(self::$iconsDir);

        // Put icons from subfolders into the array at the 'folders' index
        if ($files) {
            foreach (self::getSubfolders() as $pathName => $path) {
                $icons['folders'][str_replace("-", " ", $pathName)] = self::getIconsFromFolder($path);
            }
        }

        return $icons;
    }


    /**
     * Dynamically populates icon options for the icon field
     *
     * @param array $field
     *
     * @return array
     */
    public static function iconChoices()
    {
        $cached = wp_cache_get('icon_choices', __NAMESPACE__);

        if ($cached) {
            return $cached;
        }

        $field = [];

        $icons = self::getIcons();

        if (!empty($icons)) {
            // ACF renders sub-arrays as <optgroup>s
            if (!empty($icons['folders'])) {
                foreach ($icons['folders'] as $folderName => $folderedIcons) {
                    foreach ($folderedIcons as $icon) {
                        $label = self::renderSVG($icon['value'], true) . '&nbsp;&nbsp;&nbsp;&nbsp;' . $icon['label'];
                        $label = str_replace('<svg', '<svg style="width: 10px;height: 10px;" ', $label);
                        $field[ucfirst($folderName)][$icon['value']] = $label;
                    }
                }
            }
            foreach ($icons as $key => $icon) {
                if ($key === 'folders') {
                    continue;
                }
                $label = self::renderSVG($icon['value'], true) . '&nbsp;&nbsp;&nbsp;&nbsp;' . $icon['label'];
                $label = str_replace('<svg', '<svg style="width: 10px;height: 10px;" ', $label);
                $field[$icon['value']] = $label;
            }
        }

        // There's a lot of logic here, so probably good to only run it once per page load.
        wp_cache_set('icon_choices', $icons, __NAMESPACE__, 60);

        return $field;
    }

    public static function registerACF() {
      if( !defined('WP_CLI') && function_exists('acf_add_local_field_group') ) {

        acf_add_local_field_group([
          'key' => 'group_5bab0e925f048',
          'title' => 'Component Icon',
          'fields' => [
            [
              'key' => 'field_5bab0e9b2c85b',
              'label' => 'Icon',
              'name' => 'ci_icon',
              'type' => 'select',
              'choices' => self::iconChoices(),
              'ui' => 1
            ],
          ],
          'location' => [
            [
              [
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'post',
              ],
            ],
          ],
          'active' => false,
        ]);
      }
    }
}


// Populate the button theme choices
add_filter('acf/init', __NAMESPACE__ . '\Icon::registerACF');
