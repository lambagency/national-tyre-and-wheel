<?php
/** @var  \LambAgency\Components\Image $this */
// TODO: change image to use picture tag?
// TODO: investigate overlay handling
?>

<?php if ($this->hasImage() || $this->isShowOverlay()) : ?>
    <div class="component-image" data-autoload-component="image">
        <?php if ($this->hasImage()) :
            $backgroundImage = $this->getImage();
            ?>
            <div class="image lazy-image"
                 data-src="<?php echo esc_url($backgroundImage['sizes']['module-bg']); ?>"
                 data-src-mobile="<?php echo esc_url($backgroundImage['sizes']['module-bg-mobile']); ?>">
        <?php endif; ?>
            <div class="overlay"></div>
        <?php if ($this->hasImage()) : ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
