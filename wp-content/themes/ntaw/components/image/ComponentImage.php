<?php

namespace LambAgency\Components;

class Image extends Component
{
    protected $image = [];

    public function __construct(array $image = [])
    {
        parent::__construct();

        $this->setImage($image);
    }


    /**
     * Check if has background image
     *
     * @return bool
     */
    public function hasImage()
    {
        return !empty($this->image);
    }


    /**
     * Set the background image
     *
     * @param $image
     */
    public function setImage($image)
    {
        $this->image = !empty($image) ? $image : $this->image;
    }


    /**
     * Get the background image
     *
     * @return array
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     * Render the image component
     */
    public function renderImage()
    {
        include locate_template('components/image/image.php');
    }
}
