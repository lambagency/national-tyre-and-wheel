<?php

namespace LambAgency\Components;


class SocialShare extends Component
{

    private static $availableShares = [
        'facebook' => [
            'share'     => 'facebook',
            'label'     => 'Facebook',
            'logo'      => 'licon licon-facebook'
        ],
        'twitter' => [
            'share'     => 'twitter',
            'label'     => 'Twitter',
            'logo'      => 'licon licon-twitter'
        ],
        'linkedin' => [
            'share'     => 'linkedin',
            'label'     => 'LinkedIn',
            'logo'      => 'licon licon-linkedin'
        ],
        'googleplus' => [
            'share'     => 'googleplus',
            'label'     => 'Google Plus',
            'logo'      => 'licon licon-google'
        ],
        'pinterest' => [
            'share'     => 'pinterest',
            'label'     => 'Pinterest',
            'logo'      => 'licon licon-pinterest'
        ],
        'instagram' => [
            'share'     => 'instagram',
            'label'     => 'Instagram',
            'logo'      => 'licon licon-instagram'
        ],
        'youtube' => [
            'share'     => 'youtube',
            'label'     => 'YouTube',
            'logo'      => 'licon licon-youtube'
        ]
    ];


    private $shares;


    function __construct(array $shareButton = [])
    {
        parent::__construct();

        $this->setShares($shareButton);
    }


    /**
     * Set the shares
     *
     * @param $shareButton
     */
    public function setShares($shareButton = [])
    {
        $shares = get_field('social_share', 'option');

        if (!empty($shareButton['shares'])) {
            $shares = is_array($shareButton['shares']) ? $shareButton['shares'] : [$shareButton['shares']];
        }

        if ($shares && is_array($shares)) {
            foreach ($shares as $key) {
                if (array_key_exists($key, self::$availableShares)) {
                    $this->shares[] = self::$availableShares[$key];
                }
            }
        }
    }


    /**
     * Get the shares
     *
     * @return mixed
     */
    public function getShares()
    {
        return $this->shares;
    }


    /**
     * Populate the social share choices
     */
    public static function populateSocialShareChoices($field)
    {
        $field['choices'] = [];

        foreach (self::$availableShares as $key => $socialShare) {
            $field['choices'][$key] = $socialShare['label'];
        }

        return $field;
    }


    /**
     * Renders HTML output of share button
     */
    public function renderShareButton()
    {
        include locate_template(COMPONENTS . '/social-share/social-share.php');
    }

}

add_filter('acf/load_field/name=social_share', __NAMESPACE__ . '\SocialShare::populateSocialShareChoices');