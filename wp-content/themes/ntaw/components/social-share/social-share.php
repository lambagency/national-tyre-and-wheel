<?php
/** @var  \LambAgency\Components\SocialShare $this */

use \LambAgency\Components\Icon;
?>

<?php if ($shares = $this->getShares()) : ?>
    <div data-autoload-component="social-share" class="component component-social-share flex-container align-middle align-center">
        <h3 class="small-label social-share-header"><?php esc_html_e('SHARE THIS'); ?></h3>
        <div class="shares">
          <?php
          $permalink = urlencode(get_permalink());
          $shareTitle = urlencode(get_the_title());
          $thumbnail = urlencode(get_the_post_thumbnail_url());
          foreach ($shares as $share) {
              switch ($share['share']) {
                case 'facebook':
                  ?>
                  <a
                    class='share-button-wrapper'
                    href="<?php echo esc_url(sprintf('https://facebook.com/sharer/sharer.php?u=%s', $permalink)); ?>"
                    target='_blank'
                    rel='nofollow noopener'
                    aria-label='<?php echo esc_attr__('Share on Facebook'); ?>'
                  >
                    <?php Icon::renderSVG('facebook'); ?>
                  </a>
                  <?php
                  break;
                case 'twitter':
                  ?>
                  <a
                    class='share-button-wrapper'
                    href="<?php echo esc_url(sprintf('https://twitter.com/intent/tweet/?text=%s&amp;url=%s', $shareTitle, $permalink)); ?>"
                    target='_blank'
                    rel='nofollow noopener'
                    aria-label='<?php echo esc_attr__('Share on Twitter'); ?>'
                  >
                    <?php Icon::renderSVG('twitter'); ?>
                  </a>
                  <?php
                  break;
                case 'linkedin':
                  ?>
                  <a
                    class='share-button-wrapper'
                    href="<?php echo esc_url(sprintf('https://www.linkedin.com/shareArticle?mini=true&amp;url=%s&amp;title=%s&amp;source=%s', $permalink, $shareTitle, $permalink)); ?>"
                    target='_blank'
                    rel='nofollow noopener'
                    aria-label='<?php echo esc_attr__('Share on LinkedIn'); ?>'
                  >
                    <?php Icon::renderSVG('linkedin'); ?>
                  </a>
                  <?php
                  break;
                case 'pinterest':
                  ?>
                  <a
                    class='share-button-wrapper'
                    href="<?php echo esc_url(sprintf('https://pinterest.com/pin/create/button/?url=%s&amp;media=%s&amp;description=%s', $permalink, $thumbnail, $shareTitle, $permalink)); ?>"
                    target='_blank'
                    rel='nofollow noopener'
                    aria-label='<?php echo esc_attr__('Share on Pinterest'); ?>'
                  >
                    <?php Icon::renderSVG('pinterest'); ?>
                  </a>
                  <?php
                  break;
                case 'instagram':
                  ?>
                  <a
                    class='share-button-wrapper'
                    href="<?php echo esc_url(sprintf('https://instagram.com/?url=%s&amp;media=%s&amp;description=%s', $permalink, $thumbnail, $shareTitle, $permalink)); ?>"
                    target='_blank'
                    rel='nofollow noopener'
                    aria-label='<?php echo esc_attr__('Share on Instagram'); ?>'
                  >
                    <?php Icon::renderSVG('instagram'); ?>
                  </a>
                  <?php
                  break;
                case 'youtube':
                  ?>
                  <a
                    class='share-button-wrapper'
                    href="<?php echo esc_url(sprintf('https://youtube.com/pin/create/button/?url=%s&amp;media=%s&amp;description=%s', $permalink, $thumbnail, $shareTitle, $permalink)); ?>"
                    target='_blank'
                    rel='nofollow noopener'
                    aria-label='<?php echo esc_attr__('Share on YouTube'); ?>'
                  >
                    <?php Icon::renderSVG('youtube'); ?>
                  </a>
                  <?php
                  break;
              }
          }?>
        </div>
    </div>
<?php endif; ?>
