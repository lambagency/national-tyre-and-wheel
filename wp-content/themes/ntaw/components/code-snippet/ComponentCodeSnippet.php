<?php

namespace LambAgency\Components;

/**
 * Class CodeSnippet
 *
 * @package LambAgency\Components
 */
class CodeSnippet
{

    private static $instance = null;


    /**
     * The tags separated by position
     *
     * @var array
     */
    private $snippets = [
        'header'    => [],
        'body'      => [],
        'footer'    => []
    ];


    function __construct()
    {
        $this->setSnippets();
    }


    /**
     * Get the the snippets for the specified tag position
     *
     * @param string $position The tag position (header|body|footer)
     *
     * @return array
     */
    public function getSnippets($position = 'header')
    {
        if (array_key_exists($position, $this->snippets)) {
            return $this->snippets[$position];
        }

        return [];
    }


    /**
     * Set the code snippets
     * Include both global and page specific code snippets
     * Hides scripts if user is an administrator and logged in
     */
    private function setSnippets()
    {
        if (function_exists("get_field") && !in_array('administrator', wp_get_current_user()->roles)) {

            $tagGroups = [
                get_field('code_snippets', 'option'),
                get_field('code_snippets')
            ];


            foreach ($tagGroups as $tags) {

                if (isset($tags['ccs_code_snippet']) && is_array($tags['ccs_code_snippet'])) {

                    foreach ($tags['ccs_code_snippet'] as $tag) {

                        $liveOnly   = $tag['live_environment_only'];
                        $script     = $tag['snippet'];

                        $includeScript = true;

                        // Hide the script if live only and Dev mode is activated
                        if ($liveOnly && (!defined('APP_ENV') || APP_ENV !== 'production')) {
                            $includeScript = false;
                        }

                        if ($includeScript) {
                            $this->snippets[$tag['position']][] = $script;
                        }
                    }
                }
            }
        }
    }


    /**
     * Render the snippets by specified position
     *
     * @param string $position The tag position (header|body|footer)
     */
    public function renderSnippets($position = '')
    {
        $snippets = $this->getSnippets($position);

        foreach ($snippets as $snippet) {
            echo $snippet;
        }
    }


    /**
     * Creates or returns an instance of this class.
     *
     * @return CodeSnippet single instance of this class.
     */
    public static function getInstance()
    {
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }
}
