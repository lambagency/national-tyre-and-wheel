<?php

namespace LambAgency\Components;

class TitleBlock extends Component
{

    /** @var string  */
    protected $subtitle;

    /** @var string  */
    protected $title;

    /** @var string  */
    protected $intro;


    public function __construct($component = [])
    {
        parent::__construct();

        $this->setSubtitle($component);
        $this->setTitle($component);
        $this->setPosition($component);
    }


    /**
     * Get the subtitle
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }


    /**
     * Set the subtitle
     * @param $component
     */
    public function setSubtitle($component)
    {
        $this->subtitle = !empty($component['ctb_subtitle']) ? $component['ctb_subtitle'] : '';
    }


    /**
     * Get the title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set the title
     *
     * @param $component
     */
    public function setTitle($component)
    {
        $this->title = !empty($component['ctb_title']) ? $component['ctb_title'] : '';
    }


    /**
     * Get the intro
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }


    /**
     * Set the intro
     *
     * @param $component
     */
    public function setPosition($component)
    {
        $this->position = !empty($component['ctb_position']) ? $component['ctb_position'] : '';
    }


    /**
     * Return if first module
     */
    public function isInFirstLayout()
    {
        global $layoutIndex;

        return $layoutIndex == 0;
    }


    /**
     * Check if title block should be shown
     *
     * @return bool
     */
    public function showTitleBlock()
    {
        return ($this->title || $this->subtitle || $this->intro);
    }


    /**
     * Renders HTML output of title block
     */
    public function renderTitleBlock()
    {
        include locate_template(COMPONENTS . '/title-block/title-block.php');
    }
}
