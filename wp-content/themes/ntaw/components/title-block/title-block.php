<?php
/** @var  \LambAgency\Components\TitleBlock $this */
$subtitle = $this->getSubtitle();
$title = $this->getTitle();
$position = $this->getPosition();
?>

<?php if ($this->showTitleBlock()) : ?>
    <div class="title-block <?php echo ($position == 'left') ? 'text-left' : 'text-center'; ?>" data-autoload-component="title-block">
        <?php if (!empty($subtitle)) : ?>
            <p class="title-block-subtitle pusher"><?php echo esc_html($subtitle); ?></p>
        <?php endif; ?>

        <?php if (!empty($title)) : ?>
            <h2 class="title-block-title h2 <?php echo ($position == 'left') ? 'dash' : 'dash-center'; ?>"><?php echo esc_html($title); ?></h2>
        <?php endif; ?>
    </div>
<?php endif; ?>
