<?php

namespace LambAgency\Components;


class Video extends Component
{
    /** @var array $video */
    protected $video = [];

    /** @var array $image*/
    protected $image = [];


    function __construct(array $video)
    {
        parent::__construct();

        $this->setVideo($video);
        $this->setImage($video);
    }



    /**
     * Check if video set
     *
     * @return bool
     */
    public function hasVideo()
    {
        return !empty($this->video);
    }


    /**
     * Set the background video
     *
     * @param $video
     */
    public function setVideo($video)
    {
        $this->video = !empty($video['video']) ? $video['video'] : $this->video;
    }


    /**
     * Get the background video
     *
     * @return array
     */
    public function getVideo()
    {
        return $this->video;
    }


    /**
     * Get the video source URL
     *
     * @return string
     */
    public function getVideoSource()
    {

        if (!empty($this->video) && !empty($this->video['url'])) {
            return $this->video['url'];
        }

        return '';
    }


    /**
     * Get the video attributes
     *
     * @return array
     */
    public function getVideoAttributes()
    {

        $attributes = [
            'width' => $this->video['width'],
            'height' => $this->video['height']
        ];

        return json_encode($attributes);
    }


    /**
     * Check if image set
     *
     * @return bool
     */
    public function hasImage()
    {
        return !empty($this->image);
    }


    /**
     * Set the background image
     *
     * @param $video
     */
    public function setImage($video)
    {
        $this->image = !empty($video['image']) ? $video['image'] : $this->image;
    }


    /**
     * Get the background image
     *
     * @return array
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     * Render the video component
     */
    public function renderVideo()
    {
        include locate_template('components/video/video.php');
    }
}