<?php
/** @var  \LambAgency\Components\Video $this */
?>

<?php if ($this->hasVideo()) : ?>
    <div data-autoload-component="video" data-autoload-priority="1" class="component-video" data-attributes='<?php echo esc_attr($this->getVideoAttributes()); ?>'>

        <video playsinline autoplay muted loop="true" width="1280" height="720">
            <source type="video/mp4" src="<?php echo esc_url($this->getVideoSource()); ?>">
            <?php if ($this->hasImage()) :
                $backgroundImage = $this->getImage();
            ?>
                <div class="image" style="background-image: url(<?php echo esc_url($backgroundImage['sizes']['module-bg']); ?>);"></div>
            <?php endif; ?>
        </video>

    </div>
<?php endif; ?>
