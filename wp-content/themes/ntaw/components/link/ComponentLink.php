<?php

namespace LambAgency\Components;


class Link extends Component
{
    public $type = 'link';
    public $title = '';
    public $url = '';
    public $target = '';


    function __construct($component)
    {
        parent::__construct();

        $this->initLink($component);
    }


    /**
     * Initialise the link field
     *
     * @param $component
     */
    private function initLink($component)
    {
        if (!empty($component['cl_link_type'])) {
            switch ($component['cl_link_type']) {
                case 'link':
                    $this->title    = $component['cl_title'];

                    if (!empty($component['cl_link'])) {
                        $this->url      = $component['cl_link']['url'];
                        $this->target   = $component['cl_link']['target'];
                    }
                    break;
                case 'file':
                    $this->title    = $component['cl_title'];

                    if (!empty($component['cl_file'])) {
                        $this->url      = $component['cl_file']['url'];
                        $this->target   = '_blank';
                    }
                    break;
                default:
                    $this->link = '';
            }
        }
    }


    /**
     * Get the title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    

    /**
     * Get the link url
     *
     * @return mixed
     */
    public function getURL()
    {
        return $this->url;
    }


    /**
     * Get the link target attribute
     *
     * @return bool
     */
    public function getTarget()
    {
        return $this->target;
    }
}
