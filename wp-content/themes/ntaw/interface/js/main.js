import Blazy from "blazy";

import "../scss/theme.scss";

import Autoloader from "./autoloader";

import "promise-polyfill/src/polyfill";

/**
 * Core framework class
 */
class WordpressApp {

  constructor() {
    this.initLazyImages();
    this.initAutoload();
    this.initTabCheck();

    /**
     * For hot reload
     * Removes modules, snippers, components from autoloaded when a change is made and then initialise elements
     * and listeners again
     */
    if (module.hot) {
      setTimeout(() => this.refreshLazyImages(), 100);
      module.hot.accept("./autoloader", () => {
        this.autoloader.unMountAll();
        this.autoloader = new Autoloader();
        this.autoloader.loadAll();
      });
    }
  }

  initAutoload() {
    this.autoloader = new Autoloader();
    this.autoloader.loadAll();
    requestAnimationFrame(() => {
      this.refreshLazyImages();
    });
  }

  /**
   * Initialise lazy loading of images
   */
  initLazyImages() {
    this.lazyImages = new Blazy({
      selector: ".lazy-image",
      successClass: "lazy-success",
      breakpoints: [
        {
          width: 768, // max-width
          src: "data-src-mobile"
        }
      ]
    });
  }

  /**
   * Refresh the lazy images
   * Call this when new images have been added to the DOM
   */
  refreshLazyImages() {
    this.lazyImages.revalidate();
  }

  /**
   * initialise outlines if the use is using tabbed/accessibility browsing
   */
  initTabCheck() {
    document.body.classList.remove("no-js");
    this.tabHandler = this.handleTab.bind(this);
    this.mouseHandler = this.handleMouseDown.bind(this);
    window.addEventListener("keydown", this.tabHandler, false);
  }

  handleTab(e) {
    if (e.keyCode === 9) { // the "I am a keyboard user" key
      document.body.classList.add("user-is-tabbing");
      window.removeEventListener("keydown", this.tabHandler, false);
      window.addEventListener("mousedown", this.mouseHandler, false);
    }
  }

  handleMouseDown() {
    document.body.classList.remove("user-is-tabbing");
    window.addEventListener("keydown", this.tabHandler, false);
    window.removeEventListener("mousedown", this.mouseHandler, false);
  }
}

window.WordpressApp = new WordpressApp();
