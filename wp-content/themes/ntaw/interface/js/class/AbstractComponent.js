import unMountListeners from "./unMountListeners";
import unMountElements from "./unMountElements";

export default class AbstractComponent {
  constructor(component) {
    // Set the component element
    this.component = component;
    this.listeners = [];
    this.mountedElements = [];
  }

  unmount() {
    this.component.component = null;
    unMountListeners(this.listeners);
    unMountElements(this.mountedElements);
  }
}
