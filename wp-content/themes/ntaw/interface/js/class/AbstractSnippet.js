import unMountListeners from "./unMountListeners";
import unMountElements from "./unMountElements";

export default class AbstractSnippet {
  constructor(snippet) {
    // Set the snippet element
    this.snippet = snippet;
    this.listeners = [];
    this.mountedElements = [];
  }

  unmount() {
    this.snippet.snippet = null;
    unMountListeners(this.listeners);
    unMountElements(this.mountedElements);
  }
}
