import unMountListeners from "./unMountListeners";
import unMountElements from "./unMountElements";

export default class AbstractSnippet {
  constructor(single) {
    // Set the snippet element
    this.single = single;
    this.listeners = [];
    this.mountedElements = [];
  }

  unmount() {
    this.single.single = null;
    unMountListeners(this.listeners);
    unMountElements(this.mountedElements);
  }
}
