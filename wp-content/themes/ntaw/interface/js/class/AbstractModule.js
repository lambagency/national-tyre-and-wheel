import unMountListeners from "./unMountListeners";
import unMountElements from "./unMountElements";

export default class AbstractModule {
  constructor(module) {
    // Set the module element
    this.module = module;
    this.listeners = [];
    this.mountedElements = [];
  }

  unmount() {
    this.module.module = null;
    unMountListeners(this.listeners);
    unMountElements(this.mountedElements);
  }
}
