export default function unMountListeners(listeners) {
  listeners.forEach(({ type, element, listener }) => {
    element.removeEventListener(type, listener);
  });
}
