export default function unMountElements(elements) {
  elements.forEach(element => {
    element.parentNode.removeChild(element);
  });
}
