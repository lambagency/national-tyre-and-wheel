/* eslint-disable new-cap, no-return-assign, no-unused-vars */
export default class Autoloader {
  constructor() {
    this.modules = [];
    this.snippets = [];
    this.components = [];
    this.icons = [];
    this.singles = [];
  }

  // Add JS and SCSS to loaded files for modules, components, snippets that are loaded to the page
  loadAll() {
    this.loadModules();
    this.loadIcons();
    this.loadComponents();
    this.loadSnippets();
    this.loadTemplates();
    this.loadSingles(); // for single custom post type (team-member)
  }

  // Remove elements and listeners from modules, components, and snippets for hot reload
  unMountAll() {
    if (module.hot) {
      this.unMountModules();
      this.unMountComponents();
      this.unMountSnippets();
      this.unMountSingles();
    }
  }

  loadModules() {
    const modules = document.querySelectorAll("[data-autoload-module]");

    for (let i = modules.length - 1; i >= 0; i -= 1) {
      const moduleDOM = modules[i];
      const moduleName = moduleDOM.getAttribute("data-autoload-module");

      import(/* webpackChunkName: "module-[request]", webpackInclude: /\.js$/ */
      `../../modules/${moduleName}/${moduleName}`)
        .then(m => this.modules.push(new m.default(moduleDOM)))
        .catch(error => {
          // console.warn("Couldn't find module to autoload", error);
        });

      import(/* webpackChunkName: "module-[request]-styles"  */
      `../../modules/${moduleName}/${moduleName}.scss`);
    }
  }

  unMountModules() {
    this.modules.forEach(module => {
      module.unmount();
      this.modules = this.modules.slice(1);
    });
  }

  loadIcons() {
    const icons = document.querySelectorAll(".icon");

    for (let i = icons.length - 1; i >= 0; i -= 1) {
      const iconDOM = icons[i];

      import(/* webpackChunkName: "component-icon" */ "../../components/icon/icon.js")
        .then(c => this.icons.push(new c.default(iconDOM)))
        .catch(error => {
          // console.warn("Couldn't find component to autoload", error);
        });
    }
    if (icons.length > 0) {
      import(/* webpackChunkName: "component-icon-styles" */ "../../components/icon/icon.scss");
    }
  }

  loadComponents() {
    const components = document.querySelectorAll("[data-autoload-component]");

    for (let i = components.length - 1; i >= 0; i -= 1) {
      const componentDOM = components[i];
      const componentName = componentDOM.getAttribute(
        "data-autoload-component"
      );

      import(/* webpackChunkName: "component-[request]", webpackInclude: /\.js$/ */
      `../../components/${componentName}/${componentName}`)
        .then(c => this.components.push(new c.default(componentDOM)))
        .catch(error => {
          // console.warn("Couldn't find component to autoload", error);
        });

      import(/* webpackChunkName: "component-[request]-styles" */
      `../../components/${componentName}/${componentName}.scss`);
    }
  }

  unMountComponents() {
    this.components.forEach(component => {
      component.unmount();
      this.components.slice(1);
    });
  }

  loadSnippets() {
    import(/* webpackChunkName: "snippet-header-header", webpackMode: "eager" */
    "../../snippets/header/header.js");
    import(/* webpackChunkName: "snippet-menu-menu", webpackMode: "eager" */
    "../../snippets/menu/menu.js");

    const snippets = document.querySelectorAll("[data-autoload-snippet]");

    for (let i = snippets.length - 1; i >= 0; i -= 1) {
      const snippetDOM = snippets[i];
      const snippetName = snippetDOM.getAttribute("data-autoload-snippet");

      import(/* webpackChunkName: "snippet-[request]", webpackInclude: /\.js$/ */
      `../../snippets/${snippetName}/${snippetName}`)
        .then(s => this.snippets.push(new s.default(snippetDOM)))
        .catch(error => {
          // console.warn("Couldn't find snippet to autoload", error);
        });

      import(/* webpackChunkName: "snippet-[request]-styles" */
      `../../snippets/${snippetName}/${snippetName}.scss`);
    }
  }

  unMountSnippets() {
    this.snippets.forEach(snippet => {
      snippet.unmount();
      this.snippets.slice(1);
    });
  }

  loadTemplates() {
    this.templates = document.querySelectorAll("[data-autoload-template]");

    for (let i = this.templates.length - 1; i >= 0; i -= 1) {
      const templateDOM = this.templates[i];
      const templateName = templateDOM.getAttribute("data-autoload-template");

      import(/* webpackChunkName: "template-[request]-styles" */
      `../../templates/${templateName}/${templateName}.scss`).then(() => {
        if (module.hot) {
          setTimeout(() => window.WordpressApp.refreshLazyImages(), 100);
        }
      });
    }
  }

  loadSingles() {
    const singles = document.querySelectorAll("[data-autoload-single]");

    for (let i = singles.length - 1; i >= 0; i -= 1) {
      const singleDOM = singles[i];
      const singleName = singleDOM.getAttribute("data-autoload-single");

      import(/* webpackChunkName: "module-[request]", webpackInclude: /\.js$/ */
      `../../singles/${singleName}/${singleName}`)
        .then(m => this.singles.push(new m.default(singleDOM)))
        .catch(error => {
          // console.warn("Couldn't find module to autoload", error);
        });

      import(/* webpackChunkName: "module-[request]-styles"  */
      `../../singles/${singleName}/${singleName}.scss`);
    }
  }

  unMountSingles() {
    this.singles.forEach(single => {
      single.unmount();
      this.singles = this.singles.slice(1);
    });
  }
}
