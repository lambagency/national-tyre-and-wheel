export const throttle = function throttle(func, delay) {
  let timer = null;

  return function throttled(...args) {
    if (timer === null) {
      timer = setTimeout(() => {
        func.apply(this, args);
        timer = null;
      }, delay);
    }
  };
};

export default throttle;
