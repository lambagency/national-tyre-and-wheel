<?php
/**
 * @var LambAgency\Modules\Module $module
 * @var array $fields
 */
?>
<div class="<?php echo esc_attr($module->getModuleClass()); ?>" data-autoload-module="call-to-action">

    <div class="grid-container">
        <div class="grid-x align-center align-middle text-center">

            <div class="rte cell medium-10 large-8">
                <?php if (!empty($fields['title'])) : ?>
                    <h2 class="cta-title h2 dash-center"><?php echo esc_html($fields['title']); ?></h2>
                <?php endif; ?>
                <?php if (!empty($fields['content'])) : ?>
                    <p class="cta-lead"><?php echo wp_kses_post($fields['content']); ?></p>
                <?php endif; ?>
            </div>

            <div class="cell grow">
                <?php if (!empty($fields['buttons'])) : ?>
                    <div class="button-group button-group-margin-top-small align-center">
                        <?php
                        foreach ($fields['buttons'] as $buttonFields) :
                            $button = new \LambAgency\Components\Button($buttonFields);
                            $button->renderButton();
                        endforeach;
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php
    if ($fields['background_image']) :
        $module->getBackground()->renderImage();
    endif;
    ?>

</div>
