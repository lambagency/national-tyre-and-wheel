<?php
namespace LambAgency\Modules;

use LambAgency\Components\Image;

/**
 * Class ModuleCallToAction
 *
 * @package LambAgency\Modules
 */
class ModuleCallToAction extends AbstractModule
{
    public static $icon = "admin-links";

    private $background = null;

    public function __construct($block)
    {
        parent::__construct($block);

        $this->setBackground();
    }

    private function setBackground()
    {
        if ($this->fields['background_image']) {
            $this->background = new Image($this->fields['background_image']);
        }
    }

    /**
     * @return Image|null
     */
    public function getBackground()
    {
        return $this->background;
    }
}
