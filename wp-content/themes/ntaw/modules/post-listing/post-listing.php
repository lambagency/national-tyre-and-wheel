<?php
/**
 * @var LambAgency\Modules\ModulePostListing $module
 * @var array $fields
 */
?>

<div data-autoload-module="post-listing" class="<?php echo esc_attr($module->getModuleClass()); ?> theme-light">


    <?php
    $titleBlock = $module->getTitleBlock();

    if ($titleBlock->showTitleBlock()) : ?>
        <div class="grid-container">
            <div class="grid-x text-center">
                <div class="cell"><?php $titleBlock->renderTitleBlock(); ?></div>
            </div>
        </div>
    <?php endif; ?>

    <?php
        if (array_key_exists('announcement', $module->getPostArguments()['post_types'])) {
            get_template_part(MODULES . '/post-listing/partials/tabs');
        }
        
        get_template_part(MODULES . '/post-listing/partials/filter');
        get_template_part(MODULES . '/post-listing/partials/post-list');
    ?>

</div>