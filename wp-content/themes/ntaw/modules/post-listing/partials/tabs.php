<?php
/*
$announcements = $module->getFirstPagePosts()['data'];
$tabs = array();

foreach($announcements as $announcement) {
    array_push($tabs, $announcement['templateData']['tab']);
}

$tabs = array_unique($tabs);
rsort($tabs);
*/
$category = $module->getPostArguments()['taxonomies']['category'][0];
$years = array();
$year = date("Y");

for($y = $year; $y >= 2015 ; $y--) {
    array_push($years, $y);
}

$year = date("Y");

$category == 'Corporate Governance' ? $tabs = ['Charters', 'Policies', 'Other Documents'] : $tabs = $years;
?>

<div class="grid-container">
    <div class="announcement-tabs">
        <?php foreach($tabs as $tab): ?>
            <button class="announcement-tabs-box"><?php echo $tab; ?></button>
        <?php endforeach; ?>
    </div>
</div>