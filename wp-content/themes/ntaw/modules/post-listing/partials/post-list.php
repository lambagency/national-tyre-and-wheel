<?php
/**
 * @var \LambAgency\Modules\ModulePostListing $module
 */

$module_posts = $module->getFirstPagePosts();

$postArguments = $module->getPostArguments();
?>

<div class="post-list" data-post-data="<?php echo $module->getPostArguments('json'); ?>">

    <div class="grid-container">
        <div class="post-list-container grid-x grid-margin-x grid-margin-y container ">
            <?php echo $module_posts['html']; ?>
        </div>
    </div>

    <?php if ($postArguments['load_more'] == 'pagination') : ?>
        <div class="pagination-container">
            <?php echo $module_posts['pagination']; ?>
        </div>
    <?php endif; ?>

</div>
