<?php
/**
 * @var \LambAgency\Modules\ModulePostListing $module
 */
?>
<?php if ($module->isShowFilter()) : ?>
    <div class="grid-container">
        <div class="row-filter grid-x align-center">

            <form class="filter form-themed theme-light" autocomplete="off">

                <div class="grid-x grid-margin-x grid-margin-y align-center">

                    <?php
                        $postArguments = $module->getPostArguments();
                    ?>

                    <?php if ($module->showFilterKeyword()) : ?>
                        <div class="cell small-12 medium-4">
                            <input type="text" name="keyword" placeholder="Search..."
                                   value="<?php echo !empty($postArguments['keyword']) ? $postArguments['keyword'] : ''; ?>">
                        </div>
                    <?php endif; ?>

                    <?php if ($filterTaxonomies = $module->getFilterTaxonomies()) : ?>
                        <?php foreach ($filterTaxonomies as $filterTaxonomy) : ?>
                            <div class="cell small-12 medium-4">
                                <div class="select-wrap">
                                    <select name="<?php echo $filterTaxonomy['fieldName']; ?>" id="">
                                        <option value="" <?php echo $filterTaxonomy['defaultTerm']['active'] ? ' selected' : ''; ?>><?php echo $filterTaxonomy['defaultTerm']['label']; ?></option>
                                        <?php foreach ($filterTaxonomy['terms'] as $term) : ?>
                                            <option data-permalink="<?php echo $term['permalink']; ?>"
                                                <?php echo $term['active'] ? ' selected' : ''; ?>
                                                    value="<?php echo $term['value']; ?>"><?php echo $term['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                        <?php endforeach; ?>
                    <?php endif; ?>

                    <?php if ($filterOrder = $module->getFilterOrder()) : ?>
                        <div class="cell small-12 medium-4">
                            <div class="filter-option select-wrap">
                                <select name="order">
                                    <?php foreach ($filterOrder as $filterOrderItem) : ?>
                                        <option value="<?php echo $filterOrderItem['order']; ?>"
                                                data-orderby="<?php echo $filterOrderItem['orderby']; ?>"
                                            <?php echo ($postArguments['order'] === $filterOrderItem['order'] && $postArguments['orderby'] === $filterOrderItem['orderby']) ? 'selected' : ''; ?>>
                                            <?php echo $filterOrderItem['label']; ?>
                                        </option>

                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($module->showFilterPostTypes()) : ?>
                        <?php
                        $postTypes = $module->getPostTypes();

                        if (is_array($postTypes) && count($postTypes) > 1) :
                            ?>

                            <div class="cell small-12 medium-4">
                                <div class="select-wrap">
                                    <select name="filter-post-types" id="">
                                        <option value="all" <?php echo !$module->isPostTypeActive() ? ' selected' : ''; ?>>Show All</option>
                                        <?php foreach ($postTypes as $postType => $options) : ?>
                                            <option value="<?php echo $postType; ?>" <?php echo $options['active'] ? ' selected' : ''; ?>><?php echo $options['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                        <?php endif; ?>
                    <?php endif; ?>
                </div>

            </form>

        </div>
    </div>
<?php endif; ?>
