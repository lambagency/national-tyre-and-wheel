<?php
namespace LambAgency\Modules;

use LambAgency\Components\TitleBlock;
use LambAgency\Components\Icon;

/**
 * Class ModulePostListing
 *
 * @package LambAgency\Modules
 */
class ModulePostListing extends AbstractModule
{
    public static $icon = "admin-page";

    private $titleBlock = null;

    /**
     * Taxonomies to exclude from the filterable taxonomies list
     *
     * @var array
     */
    protected static $excludedFilterTaxonomies = [
        'post_tag',
        'post_format'
    ];

    /**
     * Default post arguments
     *
     * @var array
     */
    protected static $defaultPostArguments = [
        'posts_per_page' => 12,
        'paged'          => 1,
        'order'          => 'DESC',
        'orderby'        => 'date',
        'search-meta'    => 1,    // Search by meta fields (use bool in int format)
        'exclude-hidden' => 1     // Hide posts that aren't indexed (use bool in int format)
    ];

    /**
     * @var array Post arguments generated from defaults and user set
     */
    protected $postArguments = [];

    /**
     * @var string Load more state
     */
    protected $loadMore = 'none';

    /**
     * Array of post types and associated name and template
     *
     * @var array
     *
     * @example
     *
     * 'post' = [
     *      'name' => 'Posts'
     *      'template' => 'templates/post/three-column.php'
     * ]
     */
    protected $postTypes = [];

    /**
     * @var array Available post ordering options
     */
    protected static $availableOrderFilters = [
        [
            'label'   => 'Newest First',
            'orderby' => 'date',
            'order'   => 'DESC',
        ],
        [
            'label'   => 'Oldest First',
            'orderby' => 'date',
            'order'   => 'ASC',
        ],
        [
            'label'   => 'Title',
            'orderby' => 'title',
            'order'   => 'ASC',
        ],
        [
            'label'   => 'Menu Order',
            'orderby' => 'menu_order',
            'order'   => 'ASC',
        ]
    ];
    /**
     * @var array The filterable taxonomies
     */
    protected $filterTaxonomies = [];

    /**
     * @var array The filterable order properties
     */
    protected $filterOrder = [];

    /**
     * @var array First page of posts data array
     */
    protected $posts = [];

    /**
     * @var bool Show the filter
     */
    protected $showFilter = false;


    public function __construct($block)
    {
        parent::__construct($block);

        $this->setTitleBlock();
        $this->setPostTypes();
        $this->setLoadMore();
        $this->setShowFilter();
        $this->setFilterTaxonomies();
        $this->setFilterOrder();

        $this->setPostArguments();
        $this->setFirstPagePosts();

        $this->addModuleClasses();
    }


    /**
     * Add to the module classes
     */
    public function addModuleClasses()
    {
        if ($this->loadMore != 'none') {
            $this->moduleClass[] = $this->loadMore;
        }
    }


    private function setTitleBlock()
    {
        $this->titleBlock = new TitleBlock($this->fields['title_block'] ??  []);
    }

    /**
     * @return TitleBlock|null
     */
    public function getTitleBlock()
    {
        return $this->titleBlock;
    }

    /**
     * Return an array of post types that are available to use in the post listing
     *
     * @return array
     */
    public static function getAvailablePostTypes()
    {
        $postTypes = [];

        foreach (get_declared_classes() as $class) {
            if (is_subclass_of($class, '\LambAgency\Template\Template')) {
                /**
 * @var $class \LambAgency\Template\Template
*/

                if ($class::$availablePostTypes) {
                    foreach ($class::$availablePostTypes as $availablePostType) {
                        if (!in_array($postTypes, $postTypes)) {
                            $postTypes[] = $availablePostType;
                        }
                    }
                }
            }
        }

        return $postTypes;
    }


    /**
     * Get the load more state
     *
     * @return string
     */
    public function getLoadMore()
    {
        return $this->loadMore;
    }


    /**
     * Set the load more option
     */
    public function setLoadMore()
    {
        if (!empty($this->fields['load_more_option'])) {
            $this->loadMore = $this->fields['load_more_option'];
        } else {
          $this->loadMore = 'pagination';
        }
    }


    /**
     * Get the show filter
     *
     * @return bool
     */
    public function isShowFilter()
    {
        return $this->showFilter;
    }


    /**
     * Set the show filter state
     */
    public function setShowFilter()
    {
        $this->showFilter = !empty($this->fields['show_filter']) ? $this->fields['show_filter'] : $this->showFilter;
    }


    /**
     * Get the post type and associated template array
     *
     * @return array
     */
    public function getPostTypes()
    {
        return $this->postTypes;
    }


    /**
     * Set the post type and associated template array to be used
     */
    public function setPostTypes()
    {
        if (!empty($this->fields['post_types'])) {
            $activePostTypes = !empty($_REQUEST['type']) ? explode(',', $_REQUEST['type']) : [];

            foreach ($this->fields['post_types'] as $postType) {
                $postTypeObj = get_post_type_object($postType['module_ptl_post_type']);

                if ($postTypeObj instanceof \WP_Post_Type) {
                    $active = $activePostTypes && in_array($postTypeObj->name, $activePostTypes);

                    $this->postTypes[$postType['module_ptl_post_type']] = [
                        'name'     => $postTypeObj->label,
                        'template' => $postType['module_ptl_template'],
                        'active'   => $active ? 1 : 0
                    ];
                }
            }
        } else {
          $postTypeObj = get_post_type_object('post');
          $this->postTypes['post'] = [
              'name'     => $postTypeObj->label,
              'template' => 'LambAgency\Template\Post',
              'active'   => 1
          ];
        }
    }


    /**
     * Check if any post types are active
     *
     * @return bool
     */
    public function isPostTypeActive()
    {
        $activePostType = false;

        foreach ($this->postTypes as $postType => $options) {
            if ($options['active']) {
                $activePostType = true;
                break;
            }
        }

        return $activePostType;
    }


    /**
     * Get the default post arguments
     *
     * @return array
     */
    public static function getDefaultPostArguments()
    {
        return self::$defaultPostArguments;
    }


    /**
     * Get the post arguments in the desired format
     *
     * @param string $format
     *
     * @return array|string
     */
    public function getPostArguments($format = 'return')
    {
        if ($format == 'json') {
            return htmlspecialchars(json_encode($this->postArguments));
        } else {
            return $this->postArguments;
        }
    }


    /**
     * Set the post arguments
     */
    private function setPostArguments()
    {
        // Extend from default post arguments
        $this->postArguments = self::$defaultPostArguments;

        // Set the post types
        $this->postArguments['post_types'] = $this->getPostTypes();

        // Set the posts per page
        if (!empty($this->fields['posts_per_page'])) {
            $this->postArguments['posts_per_page'] = (int)$this->fields['posts_per_page'];
        }

        if (!empty($this->fields['module_ptl_order'])) {
            $order = self::$availableOrderFilters[$this->fields['module_ptl_order']];

            $this->postArguments['order'] = $order['order'];
            $this->postArguments['orderby'] = $order['orderby'];
        }


        // Keyword
        if (!empty($_REQUEST['keyword'])) {
            $this->postArguments['keyword'] = $_REQUEST['keyword'];
        }
        // Paged
        $this->postArguments['load_more'] = $this->loadMore;

        if ($this->loadMore == 'load-more' && !empty($_REQUEST['pn'])) {
            $this->postArguments['mod_paged'] = $this->postArguments['paged'];
            $this->postArguments['paged'] = (int)$_REQUEST['pn'];
        } elseif ($this->loadMore == 'pagination' && !empty($_REQUEST['pn'])) {
            $this->postArguments['paged'] = (int)$_REQUEST['pn'];
        }
        // Set the order by
        if (!empty($_REQUEST['orderby'])) {
            $this->postArguments['orderby'] = $_REQUEST['orderby'];
        }

        // Set the order (ascending, descending, random)
        if (!empty($_REQUEST['order'])) {
            $this->postArguments['order'] = $_REQUEST['order'];
        }

        // Set Taxonomy filters
        $taxonomies = [];

        if ($filterTaxonomies = $this->getFilterTaxonomies()) {
            foreach ($filterTaxonomies as $filterTaxonomy) {
                foreach ($filterTaxonomy['terms'] as $term) {
                    if ($term['active']) {
                        $taxonomies[$filterTaxonomy['name']][] = $term['value'];
                    }
                }
            }
        }

        if ($defaultFilterTaxonomies = $this->getDefaultFilterTaxonomies()) {
            foreach ($defaultFilterTaxonomies as $defaultFilterTaxonomy => $terms) {
                foreach ($terms as $term) {
                    $taxonomies[$defaultFilterTaxonomy][] = $term;
                }
            }
        }

        if ($taxonomies) {
            $this->postArguments['taxonomies'] = $taxonomies;
        }
    }


    public function getDefaultFilterTaxonomies()
    {
        $defaultTaxonomies = [];

        if (!empty($this->fields['module_ptl_default_taxonomy'])) {
            $termList = $this->fields['module_ptl_default_taxonomy'];

            foreach ($termList as $termListItem) {
                $termListItemArray = explode('-', $termListItem);
                $termName = array_pop($termListItemArray);
                $taxonomy = implode('-', $termListItemArray);
                $defaultTaxonomies[$taxonomy][] = $termName;
            }
        }

        return $defaultTaxonomies;
    }

    /**
     * Get the filterable taxonomies
     *
     * @return array
     */
    public function getFilterTaxonomies()
    {
        return $this->filterTaxonomies;
    }


    /**
     * Set the filterable taxonomies
     * Also check for POST or GET variables that will assign the active taxonomy filter
     * Must pre-append 'filter-' to taxonomy to avoid conflicts with WP routing
     *
     * Example:
     * Taxonomy: category
     * ?filter-category=news
     */
    public function setFilterTaxonomies()
    {
        if (!empty($this->fields['module_ptl_filter_taxonomies'])) {
            $taxonomyList = $this->fields['module_ptl_filter_taxonomies'];

            $defaultTaxonomies = $this->getDefaultFilterTaxonomies();

            if ($taxonomyList) {
                foreach ($taxonomyList as $taxonomyItem) {
                    $taxonomy = get_taxonomy($taxonomyItem);

                    if ($taxonomy) {
                        $terms = get_terms(
                            [
                            'taxonomy'   => $taxonomy->name,
                            'hide_empty' => true
                            ]
                        );

                        if ($terms) {
                            $filterTaxonomy = [
                                'name'        => $taxonomy->name,
                                'label'       => $taxonomy->label,
                                'fieldName'   => 'taxonomy-' . $taxonomy->name,
                                'defaultTerm' => [
                                    'id'     => $taxonomy->name . '-default',
                                    'label'  => $taxonomy->labels->all_items,
                                    'active' => true,
                                ],
                                'terms'         => [],
                                'hierarchical'  => $taxonomy->hierarchical,
                                'hasArchive'    => !empty($taxonomy->has_front_page) || $taxonomy->name == 'category' ? true : false
                            ];

                            $activeTerms = [];

                            $taxonomyParameter = 'filter-' . $taxonomy->name;

                            if (!empty($_REQUEST[$taxonomyParameter])) {
                                $activeTerms = explode(',', $_REQUEST[$taxonomyParameter]);
                            }

                            $queriedObject = get_queried_object();

                            if ($queriedObject instanceof \WP_Term) {
                                if ($queriedObject->taxonomy == $filterTaxonomy['name']) {
                                    $activeTerms[] = $queriedObject->slug;
                                }
                            }

                            $defaultTaxonomyTerms = [];

                            if (array_key_exists($taxonomy->name, $defaultTaxonomies)) {
                                $defaultTaxonomyTerms = $defaultTaxonomies[$taxonomy->name];
                            }

                            foreach ($terms as $index => $term) {
                                /**
 * @var $term \WP_Term
*/
                                $active = false;

                                if ($activeTerms && !array_diff($activeTerms, [$term->name, $term->ID, $term->slug])) {
                                    $active = true;
                                }

                                if ($defaultTaxonomyTerms && in_array($term->name, $defaultTaxonomyTerms)) {
                                    $active = true;
                                }

                                if ($active) {
                                    $filterTaxonomy['defaultActive'] = false;
                                }

                                $filterTaxonomy['terms'][] = [
                                    'id'        => $term->name . $index,
                                    'termID'    => $term->term_id,
                                    'name'      => $term->name,
                                    'value'     => $term->slug,
                                    'parent'    => $term->parent,
                                    'permalink' => get_term_link($term),
                                    'active'    => $active
                                ];
                            }

                            $this->filterTaxonomies[] = $filterTaxonomy;
                        }
                    }
                }
            }
        }
    }


    /**
     * Get the filterable order properties
     *
     * @return array
     */
    public function getFilterOrder()
    {
        return $this->filterOrder;
    }


    /**
     * Set the filterable order properties
     */
    public function setFilterOrder()
    {
        if (!empty($this->fields['module_ptl_filter_order'])) {
            $orderList = $this->fields['module_ptl_filter_order'];

            $orderFilter = [];

            foreach ($orderList as $orderItem) {
                $orderFilter[] = self::$availableOrderFilters[$orderItem];
            }

            $this->filterOrder = $orderFilter;
        }
    }


    /**
     * Check if the keyword filter should display
     *
     * @return bool
     */
    public function showFilterKeyword()
    {
        return !empty($this->fields['show_keyword_search']) ? $this->fields['show_keyword_search'] : false;
    }


    /**
     * Check if the keyword filter should display
     *
     * @return bool
     */
    public function showFilterPostTypes()
    {
        return !empty($this->fields['show_post_type_filter']) ? $this->fields['show_post_type_filter'] : false;
    }


    /**
     * Clean and setup the post data ready for the wp query
     *
     * @param array $postArgs
     * @param array $postTypes
     */
    public static function setupPostData(&$postArgs = [], &$postTypes = [])
    {

        // Setup Post Types
        // -------------------------
        $postTypes = array_key_exists('post_types', $postArgs) ? $postArgs['post_types'] : [];
        $filterPostTypes = array_key_exists('filter-post-types', $postArgs) ? $postArgs['filter-post-types'] : [];

        $availablePostTypes = self::getAvailablePostTypes();

        $queryPostTypes = [];
        $exclusiveQueryPostTypes = [];

        foreach ($postTypes as $postType => $attributes) {

            // Only allow available post types
            if (in_array($postType, $availablePostTypes) && (count($filterPostTypes) == 0 || in_array($postType, $filterPostTypes))) {
                $queryPostTypes[] = $postType;

                if ($attributes['active']) {
                    $exclusiveQueryPostTypes[] = $postType;
                    $postArgs['filter-post-types'][] = $postType;
                }
            }
        }

        $postArgs['post_type'] = count($exclusiveQueryPostTypes) ? $exclusiveQueryPostTypes : $queryPostTypes;


        // Set Search
        // -------------------------
        if (array_key_exists('keyword', $postArgs) && !empty($postArgs['keyword'])) {
            $postArgs['s'] = $postArgs['keyword'];
        }


        // Set Taxonomies
        // -------------------------
        if (array_key_exists('taxonomies', $postArgs) && count($postArgs['taxonomies'])) {
            $taxArgs = [
                'relation' => 'AND'
            ];

            foreach ($postArgs['taxonomies'] as $taxonomyName => $terms) {
                if ($terms) {

                    // Array of searched term_ids
                    $termIDs = [];

                    // Array of searched term names (name|slug)
                    $termNames = [];

                    // Split terms into array of term IDs and term names
                    if (is_array($terms)) {
                        foreach ($terms as $term) {
                            if (is_numeric($term)) {
                                $termIDs[] = (int)$term;
                            } elseif (is_string($term)) {
                                $termNames[] = $term;
                            }
                        }
                    } else {
                      if (is_numeric($terms)) {
                          $termIDs[] = (int)$terms;
                      } elseif (is_string($terms)) {
                          $termNames[] = $terms;
                      }
                    }

                    // Generate our tax query
                    $generatedTaxArgs = [
                        'relation' => 'OR'
                    ];

                    if (count($termIDs)) {
                        $generatedTaxArgs[] = [
                            'taxonomy' => $taxonomyName,
                            'field'    => 'term_id',
                            'terms'    => $termIDs,
                        ];
                    }

                    if (count($termNames)) {
                        $generatedTaxArgs[] = [
                            'taxonomy' => $taxonomyName,
                            'field'    => 'name',
                            'terms'    => $termNames,
                        ];

                        $generatedTaxArgs[] = [
                            'taxonomy' => $taxonomyName,
                            'field'    => 'slug',
                            'terms'    => $termNames,
                        ];
                    }

                    $taxArgs[] = $generatedTaxArgs;
                }
            }

            $postArgs['tax_query'] = $taxArgs;
        }
    }


    /**
     * Main function for returning posts for the post query
     *
     * @param array $postArgs   Array of post arguments
     * @param bool  $renderHTML If true return the data as rendered html,
     *                          otherwise return as data for use in Handlebars js
     *
     * @return array|string Returns array containing posts html, max pages, next_page
     */
    public static function getPosts($postArgs = [], $renderHTML = false)
    {
        global $post;
        $currentPost = $post;

        $postTypes = [];



        // Clean and setup the post arguments
        self::setupPostData($postArgs, $postTypes);


        $postsPerPage = $postArgs['posts_per_page'];

        if (!empty($postArgs['mod_posts_per_page'])) {
            $modPostsPerPage = $postArgs['mod_posts_per_page'];
            $postArgs['posts_per_page'] = $modPostsPerPage;
            unset($postArgs['mod_posts_per_page']);
        }

        // Posts Query
        //---------------------------------

        $postData = [];
        $renderedHTML = '';

        $post_query = new \WP_Query($postArgs);

        if ($post_query->have_posts()) {
            // Start index bases on page number, index will match post index
            $index = !empty($postArgs['paged']) ? ($postArgs['paged'] - 1) * $postArgs['posts_per_page'] : 0;

            while ($post_query->have_posts()) {
                $post_query->the_post();

                global $post;

                // Get template
                if (array_key_exists($post->post_type, $postTypes)) {
                    $templateClass = $postTypes[$post->post_type]['template'];

                    // Ensure the class exists
                    if (!class_exists($templateClass)) {
                        continue;
                    }

                    /**
                     * @var $templateObj \LambAgency\Template\Template
                    */
                    $templateObj = new $templateClass($post);

                    $templateObj->setIndex($index);

                    $postData[] = $templateObj->getPostData();

                    if ($renderHTML) {
                        $renderedHTML .= $templateObj->getTemplateHTML();
                    }
                }

                $index++;
            }
            wp_reset_postdata();
        } else {
            global $post;
            $templateObj = new \LambAgency\Template\EmptyResults($post);
            $postData[] = $templateObj->getPostData();
        }

        $post = $currentPost;


        if (!empty($modPostsPerPage)) {
            $currentPage = ($modPostsPerPage / $postsPerPage);
            $postCount = intval($post_query->found_posts);
            $maxPages = ceil($postCount / $postsPerPage);
        } else {
            $currentPage = intval($postArgs['paged']);
            $maxPages = $post_query->max_num_pages;
        }


        $response = array(
            'currentPage' => $currentPage,
            'maxPages'    => $maxPages,
            'count'       => $post_query->post_count,
            'data'        => $postData
        );

        if ($renderHTML) {
            $response['html'] = $renderedHTML;
        }

        // Pagination
        if ($postArgs['load_more'] == 'pagination') {
            $response['pagination'] = paginate_links(
                [
                'base'      => '%_%',
                'format'    => '?pn=%#%',
                'current'   => max(1, $postArgs['paged']),
                'prev_text' => Icon::renderSVG('angle-left', true),
                'next_text' => Icon::renderSVG('angle-right', true),
                'total'     => $post_query->max_num_pages
                ]
            );
        }

        return $response;
    }


    /**
     * Set the first page of posts
     */
    public function setFirstPagePosts()
    {
        $postArguments = $this->getPostArguments();

        if ($this->loadMore == 'load-more' && !empty($_REQUEST['pn'])) {
            $postArguments['mod_posts_per_page'] = $this->postArguments['posts_per_page'] * (int)$_REQUEST['pn'];
        } elseif ($this->loadMore == 'pagination' && !empty($_REQUEST['pn'])) {
            $postArguments['paged'] = (int)$_REQUEST['pn'];
        }

        if (!empty($postArguments['mod_paged'])) {
            $postArguments['paged'] = $postArguments['mod_paged'];
            unset($postArguments['mod_paged']);
        }

        $this->posts = self::getPosts($postArguments, true);

        $this->postArguments['max-pages'] = $this->posts['maxPages'];
    }


    /**
     * Get the html of the first page of posts
     *
     * @return mixed
     */
    public function getFirstPagePosts()
    {
        return $this->posts;
    }



    /**
     * ===================================================
     * IMPORTANT
     * The below functions are all used in the admin section
     * ===================================================
     */


    /**
     * Return array of values for all post types
     *
     * @param string $type
     *
     * @return array
     */
    public static function loadSelectOptions($type)
    {
        $options = [];

        $postTypes = self::getAvailablePostTypes();

        // Stores values as keys to be used to ensure no duplicates are added
        $usedValues = [];

        if ($postTypes) {
            foreach ($postTypes as $postType) {
                switch ($type) {

                case 'templates':
                    $values = self::getPostTypeTemplates($postType);
                    break;

                case 'taxonomies':
                    $values = self::getPostTypeTaxonomies($postType);
                    break;

                case 'terms':
                    $values = self::getPostTypeTerms($postType);
                    break;

                default:
                    $values = [];
                }

                if ($values) {
                    foreach ($values as $value) {
                        if (in_array($value['value'], $usedValues)) {
                            continue;
                        } else {
                            $usedValues[] = $value['value'];
                        }

                        $options[] = [
                            'value' => $value['value'],
                            'label' => $value['label']
                        ];
                    }
                }
            }
        }

        return $options;
    }


    /**
     * Returns field array containing loaded options
     *
     * @param array $field
     * @param array $options
     *
     * @return array
     */
    private static function loadOptions(array $field, array $options)
    {
        $field['choices'] = [];

        if ($options) {
            foreach ($options as $option) {
                $field['choices'][$option['value']] = $option['label'];
            }
        }

        return $field;
    }


    /**
     * Dynamically populates post type options
     *
     * @param array $field
     *
     * @return array
     */
    public static function loadPostTypeOptions(array $field)
    {
        if (is_admin()) {
            $options = [];

            foreach (self::getAvailablePostTypes() as $postName) {
                $postType = get_post_type_object($postName);

                if ($postType) {
                    $options[] = [
                        'value' => $postType->name,
                        'label' => $postType->label
                    ];
                }
            }

            $field = self::loadOptions($field, $options);
        }

        return $field;
    }


    /**
     * Dynamically populates post type options
     *
     * @param array $field
     *
     * @return array
     */
    public static function loadPostTemplateOptions(array $field)
    {
        if (is_admin()) {
            $options = self::loadSelectOptions('templates');

            $field = self::loadOptions($field, $options);
        }

        return $field;
    }


    /**
     * Dynamically populates post's taxonomy options
     *
     * @param array $field
     *
     * @return array
     */
    public static function loadPostFilterTaxonomyOptions(array $field)
    {
        if (is_admin()) {
            $options = self::loadSelectOptions('taxonomies');

            $field = self::loadOptions($field, $options);
        }

        return $field;
    }

    /**
     * Dynamically populates post's default taxonomy term options
     *
     * @param array $field
     *
     * @return array
     */
    public static function loadPostDefaultFilterTaxonomyOptions(array $field)
    {
        if (is_admin()) {
            $options = self::loadSelectOptions('terms');

            $field = self::loadOptions($field, $options);
        }

        return $field;
    }


    /**
     * Dynamically populates filter order options
     *
     * @param array $field
     *
     * @return array
     */
    public static function loadFilterOrderOptions(array $field)
    {
        if (is_admin()) {
            $options = [];

            foreach (self::$availableOrderFilters as $index => $availableOrderFilter) {
                $options[] = [
                    'value' => $index,
                    'label' => $availableOrderFilter['label'],
                ];
            }

            $field = self::loadOptions($field, $options);
        }

        return $field;
    }


    /**
     * Return array of templates for a given post type
     * Templates will be searched for in the /templates/{post-type} folder
     *
     * @param string $postType
     *
     * @return array
     */
    private static function getPostTypeTemplates($postType)
    {
        $postTypeTemplates = [];

        foreach (get_declared_classes() as $class) {
            if (is_subclass_of($class, '\LambAgency\Template\Template')) {
                /**
                 * @var $class \LambAgency\Template\Template
                 */

                if (in_array($postType, $class::$availablePostTypes)) {
                    $postTypeTemplates[] = [
                        'value' => $class,
                        'label' => $class::$name
                    ];
                }
            }
        }

        return $postTypeTemplates;
    }


    /**
     * Return array of taxonomies for a given post type
     *
     * @param string $postType
     *
     * @return array
     */
    private static function getPostTypeTaxonomies($postType)
    {
        $postTypeTaxonomies = [];

        $taxonomies = get_object_taxonomies($postType, 'objects');

        if ($taxonomies) {
            foreach ($taxonomies as $taxonomy) {

                // Skip excluded taxonomies
                if ($postType == 'post' && in_array($taxonomy->name, self::$excludedFilterTaxonomies)) {
                    continue;
                }

                $postTypeTaxonomies[] = [
                    'value' => $taxonomy->name,
                    'label' => $taxonomy->label
                ];
            }
        }

        return $postTypeTaxonomies;
    }

    /**
     * Return array of terms for a given post type
     *
     * @param string $postType
     *
     * @return array
     */
    private static function getPostTypeTerms($postType)
    {
        $postTypeTerms = [];

        $taxonomies = get_object_taxonomies($postType, 'objects');

        if ($taxonomies) {
            foreach ($taxonomies as $taxonomy) {
                /**
                 * @var $taxonomy \WP_Taxonomy
                */
                // Skip excluded taxonomies
                if ($postType == 'post' && in_array($taxonomy->name, self::$excludedFilterTaxonomies)) {
                    continue;
                }

                $terms = get_terms(
                    [
                    'hide_empty'    => true,
                    'taxonomy'      => $taxonomy->name
                    ]
                );

                foreach ($terms as $term) {
                    /**
                     * @var $term \WP_Term
                    */
                    $postTypeTerms[] = [
                        'value' => $term->taxonomy . '-' . $term->name,
                        'label' => $taxonomy->label . ' > ' . $term->name
                    ];
                }
            }
        }

        return $postTypeTerms;
    }

    /**
     * Returns an associative array for outputting a json array containing post types and their
     * posts, taxonomies and templates
     *
     * @return array
     */
    private static function getPostListingJS()
    {
        $PostListingJS = [];

        $postTypes = self::getAvailablePostTypes();

        if ($postTypes) {
            foreach ($postTypes as $postType) {
                $PostListingJS[$postType]['templates']  = self::getPostTypeTemplates($postType);
                $PostListingJS[$postType]['taxonomies'] = self::getPostTypeTaxonomies($postType);
                $PostListingJS[$postType]['terms']      = self::getPostTypeTerms($postType);
            }
        }

        return $PostListingJS;
    }


    /**
     * Enqueue module's admin javascript files
     *
     * @param string $hook
     */
    public static function enqueueAdminScripts($hook)
    {
        global $post_type;


        $adminPages = [
            'post.php',
            'post-new.php'
        ];

        if (!in_array($hook, $adminPages)) {
            return;
        }

        wp_enqueue_script(
            'post-type-listing',
            THEME_URI . '/' . MODULES . '/post-listing/admin/module-post-listing.js',
            'jquery'
        );

        wp_localize_script(
            'post-type-listing',
            'PostListingJS',
            [
                'currPostType' => $post_type,
                'postTypes'    => self::getPostListingJS()
            ]
        );
    }
}


/**
 * Actions and filters
 */
add_action('admin_enqueue_scripts', __NAMESPACE__ . '\ModulePostListing::enqueueAdminScripts');

add_filter('acf/load_field/name=module_ptl_post_type', __NAMESPACE__ . '\ModulePostListing::loadPostTypeOptions');
add_filter('acf/load_field/name=module_ptl_template', __NAMESPACE__ . '\ModulePostListing::loadPostTemplateOptions');
add_filter('acf/load_field/name=module_ptl_filter_taxonomies', __NAMESPACE__ . '\ModulePostListing::loadPostFilterTaxonomyOptions');
add_filter('acf/load_field/name=module_ptl_default_taxonomy', __NAMESPACE__ . '\ModulePostListing::loadPostDefaultFilterTaxonomyOptions');
add_filter('acf/load_field/name=module_ptl_filter_order', __NAMESPACE__ . '\ModulePostListing::loadFilterOrderOptions');
add_filter('acf/load_field/name=module_ptl_order', __NAMESPACE__ . '\ModulePostListing::loadFilterOrderOptions');


/**
 * Class PostListingAPI
 *
 * @package LambAgency\Modules
 */
class PostListingAPI
{
    private static $instance = null;


    /**
     * WPAPI constructor.
     */
    public function __construct()
    {
        add_action('rest_api_init', [&$this, 'addEndPoints']);
    }


    /**
     * Add custom REST end points
     */
    public function addEndPoints()
    {
        register_rest_route(
            API_NAMESPACE, '/getposts/', [
            [
                'methods'  => ['GET', 'POST'],
                'callback' => [&$this, 'getPosts'],
                'args'     => []
            ]
            ]
        );
    }


    /**
     * Get the posts
     *
     * @param \WP_REST_Request $request
     *
     * @return array
     */
    public function getPosts(\WP_REST_Request $request)
    {
        $response = [];

        $parameters = $request->get_json_params();

        if (!(array_key_exists('postArguments', $parameters))) {
            //Fail
            return $response;
        }

        // Get the default post arguments
        $postArguments = \LambAgency\Modules\ModulePostListing::getDefaultPostArguments();

        // Merge the posted arguments with the defaults
        $postArguments = array_merge($postArguments, $parameters['postArguments']);

        // Get the posts
        $response = \LambAgency\Modules\ModulePostListing::getPosts($postArguments);

        return $response;
    }


    /**
     * Creates or returns an instance of this class.
     *
     * @return PostListingAPI single instance of this class.
     */
    public static function getInstance()
    {
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }
}

PostListingAPI::getInstance();
