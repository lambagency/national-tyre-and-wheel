import AbstractModule from "class/AbstractModule";

import createEvent from "plugins/createEvent";

import TemplateLoader from "../../templates/templateLoader";

export default class ModulePostListing extends AbstractModule {
  constructor(module) {
    super(module);

    this.tabs = this.module.querySelectorAll(".announcement-tabs-box");
    this.tabcontent = this.module.querySelectorAll(".template-announcement");
    this.initAnnouncement();

    this.list = this.module.querySelector(".post-list");
    this.container = this.list.querySelector(".post-list-container");
    this.filterForm = this.module.querySelector("form.filter");

    this.action = "getposts";
    this.ajaxXHR = null;
    this.baseURL = `${window.location.protocol}//${window.location.host}${window.location.pathname}`;
    this.data = {};
    this.defaultData = JSON.parse(this.list.getAttribute("data-post-data"));
    this.loadMore = null;
    this.pagination = null;
    this.loading = false;
    this.loader = null;
    this.setLoadMoreType();

    this.initList();
    this.initFilter();

    window.WordpressApp.refreshLazyImages();

    let loadPostsListener;
    // This event fires every time posts are loaded via ajax
    this.list.addEventListener(
      "loadPosts",
      (loadPostsListener = () => {
        setTimeout(() => {
          window.WordpressApp.refreshLazyImages();
        }, 20);
      })
    );

    if (module.hot) {
      // Listeners have to be added manually so that they can be unloaded during hot-reloading
      this.listeners.push({
        type: "loadPosts",
        element: this.list,
        listener: loadPostsListener,
      });
    }
  }

  setLoadMoreType() {
    this.loadMoreType = "none";
    if (this.module.classList.contains("pagination")) {
      this.loadMoreType = "pagination";
    }
    if (this.module.classList.contains("load-more")) {
      this.loadMoreType = "load-more";
    }
  }

  initAnnouncement() {
    let yearsWithContent = [], counter = 0

    // Get years with content
    this.tabcontent.forEach((tabcontent) => {
      if (!yearsWithContent.includes(tabcontent.dataset.tab)) {
        yearsWithContent.push(tabcontent.dataset.tab);
      }
    });

    // Tab/tabcontent Activation
    this.tabs.forEach((tab) => {
      // Remove tab if it doesn't have any content
      if(!yearsWithContent.includes(tab.innerHTML)) {
        tab.remove()
      } else {
        // Activate first tab and it's content
        if(counter == 0) {
          tab.classList.add("active")

          for (var i = 0; i < this.tabcontent.length; i++) {
            if (tab.innerText == this.tabcontent[i].dataset.tab) {
              this.tabcontent[i].classList.add("active");
            }
          }

          counter ++;
        }

        // Activate tab on click
        tab.addEventListener("click", () => {
          // Remove .active from all tabs and tab content
          for (var j = 0; j < this.tabs.length; j++) {
            this.tabs[j].classList.remove("active");
          }
  
          // Remove .active from all tabs
          for (var i = 0; i < this.tabcontent.length; i++) {
            this.tabcontent[i].classList.remove("active");
          }
  
          // Activate tab and it's content
          for (var i = 0; i < this.tabcontent.length; i++) {
            tab.classList.add("active");
            if (tab.innerText == this.tabcontent[i].dataset.tab) {
              this.tabcontent[i].classList.add("active");
            }
          }
        });
      }
    });
  }

  /**
   * Initialise the post listing
   */
  initList() {
    // Refreshes data from DOM on hot reloads, which may cause some weirdness
    // The module doesn't know you've already changed filters / pagination after hot reload
    if (this.defaultData) {
      this.data = {
        ...this.data,
        ...this.defaultData,
      };

      this.loader = document.createElement("div");
      this.loader.classList.add("loader");
      this.module.appendChild(this.loader);

      if (this.loadMoreType === "load-more") {
        this.initLoadMore();
      } else if (this.loadMoreType === "pagination") {
        this.initPagination();
      }

      if (module.hot) {
        this.mountedElements.push(this.loader);
      }
    }
  }

  /**
   * Initialise the load more component
   */
  initLoadMore() {
    this.loadMore = document.createElement("button");
    this.loadMore.type = "button";
    this.loadMore.classList.add("button", "theme-primary", "load-more");
    this.loadMore.innerHTML = "Load More";

    this.updateLoadMoreState();

    this.module.appendChild(this.loadMore);

    let loadMoreClickListener;
    // Load posts on load more component click
    this.loadMore.addEventListener(
      "click",
      (loadMoreClickListener = () => {
        if (this.loading) {
          return;
        }

        this.loadPosts();
        this.updateQueryStrings();
      })
    );

    if (module.hot) {
      this.listeners.push({
        type: "click",
        element: this.loadMore,
        listener: loadMoreClickListener,
      });
      this.mountedElements.push(this.loadMore);
    }
  }

  /**
   * Update the load more state to show or hide if more posts are available to load
   */
  updateLoadMoreState() {
    if (this.data.paged >= this.data["max-pages"]) {
      this.loadMore.classList.add("hide");
      this.list.classList.remove("load-more-visible");
    } else {
      this.loadMore.classList.remove("hide");
      this.list.classList.add("load-more-visible");
    }
  }

  /**
   * Get URL GET parameters from href string
   *
   * @param href
   * @returns {Array}
   */
  getUrlVars(href) {
    const vars = [];

    let hash;

    const hashes = href.slice(href.indexOf("?") + 1).split("&");

    for (let i = 0; i < hashes.length; i += 1) {
      hash = hashes[i].split("=");
      // ????
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }

    return vars;
  }

  /**
   * Update the pagination state
   */
  initPagination() {
    if (this.module.classList.contains("pagination")) {
      this.pagination = this.module.querySelector(".pagination-container");
      const paginationLinks = this.pagination.querySelectorAll("a");

      for (let i = paginationLinks.length - 1; i >= 0; i -= 1) {
        const paginationLink = paginationLinks[i];

        let paginationLinkListener;
        paginationLink.addEventListener(
          "click",
          (paginationLinkListener = (e) => {
            e.preventDefault();

            const href = paginationLink.getAttribute("href");

            const pageNumber = this.getUrlVars(href).pn;

            this.data.paged = pageNumber;

            this.updateQueryStrings();
            this.loadPosts();
          })
        );

        if (module.hot) {
          this.listeners.push({
            type: "click",
            element: paginationLink,
            listener: paginationLinkListener,
          });
        }
      }
    }
  }

  /**
   * Initialise the post type listing filter
   */
  initFilter() {
    if (!this.filterForm.length) {
      return;
    }

    // Auto submit form on select change
    const filterInputs = this.filterForm.querySelectorAll("input,select");

    let formSubmitListener;

    this.filterForm.addEventListener(
      "submit",
      (formSubmitListener = (e) => e.preventDefault())
    );

    if (module.hot) {
      this.listeners.push({
        type: "submit",
        element: this.filterForm,
        listener: formSubmitListener,
      });
    }

    for (let i = filterInputs.length - 1; i >= 0; i -= 1) {
      const filterInput = filterInputs[i];

      // Dispatch a custom event
      let filterInputListener;
      filterInput.addEventListener(
        "change",
        (filterInputListener = () => {
          this.submitFilterForm();
        })
      );
      if (module.hot) {
        this.listeners.push({
          type: "change",
          element: filterInput,
          listener: filterInputListener,
        });
      }
    }
  }

  setTaxonomies() {
    // Set Taxonomies
    // -------------------------
    const taxonomies = {};

    const selectedPermalinks = [];

    // Loop through each form input and update the data to match the inputs
    const taxonomyInputs = this.filterForm.querySelectorAll(
      "select[name*='taxonomy-']"
    );

    for (let i = taxonomyInputs.length - 1; i >= 0; i -= 1) {
      const taxonomyInput = taxonomyInputs[i];

      const inputValue = taxonomyInput.value;

      const inputOption =
        taxonomyInput.selectedIndex > -1
          ? taxonomyInput.options[taxonomyInput.selectedIndex]
          : null;

      const taxonomyName = taxonomyInput
        .getAttribute("name")
        .replace("taxonomy-", "");

      let inputPermalink = null;

      if (inputValue) {
        if (taxonomies[taxonomyName] === undefined) {
          taxonomies[taxonomyName] = [];
          inputPermalink = inputOption.getAttribute("data-permalink");
        }
        taxonomies[taxonomyName].push(inputValue);
      }

      if (inputPermalink) {
        selectedPermalinks.push(inputPermalink);
      }
    }

    // Only update slug if single post type listing
    if (this.data["update-slug"]) {
      // If only one taxonomy with a permalink has been defined then update the url
      if (selectedPermalinks.length === 1) {
        window.history.replaceState({}, null, selectedPermalinks[0]);
      }
      // If the user has multiple taxonomies with permalinks selected,
      // update url to the taxonomy base URL
      else if (this.baseURL) {
        window.history.replaceState({}, null, this.baseURL);
      }
    }

    this.data.taxonomies = taxonomies;

    return { taxonomies, selectedPermalinks };
  }

  setPostType() {
    // Set Post Type Filter
    // -------------------------

    const filterPostTypes = [];

    const postTypeFilter = this.filterForm.querySelector(
      "select[name='filter-post-types']"
    );

    if (postTypeFilter !== null) {
      filterPostTypes.push(postTypeFilter.value);

      this.data["filter-post-types"] =
        postTypeFilter.value === "all"
          ? this.defaultData["filter-post-types"]
          : filterPostTypes;

      Object.keys(this.data.post_types).forEach((name) => {
        const postType = this.data.post_types[name];
        postType.active = 0;
        if (
          postTypeFilter.value === "all" ||
          filterPostTypes.indexOf(name) > -1
        ) {
          postType.active = 1;
        }
      });
    }
  }

  setOrderBy() {
    // Set Order
    // -------------------------

    const orderFilter = this.filterForm.querySelector("select[name='order']");

    const inputOption =
      orderFilter && orderFilter.selectedIndex > -1
        ? orderFilter.options[orderFilter.selectedIndex]
        : null;

    this.data.order = (orderFilter && orderFilter.value) || "";
    this.data.orderby =
      (inputOption && inputOption.getAttribute("data-orderby")) || "";
  }

  /**
   * Submit the post type listing filters
   */
  submitFilterForm() {
    // Set Search
    // -------------------------
    const searchInput = this.filterForm.querySelector("input[name='keyword']");

    if (searchInput !== null) {
      this.data.keyword = searchInput.value;
    }

    this.setTaxonomies();

    this.setPostType();

    this.setOrderBy();

    // Load a new set of posts
    // -------------------------
    this.newLoadPosts();
    this.updateQueryStrings();
  }

  updateQueryStrings() {
    // Populate query parameters
    // -------------------------
    const parameters = [];

    if (this.data.paged > 1) {
      parameters.push(`pn=${this.data.paged}`);
    }

    if (this.data.order && this.data.order.length) {
      parameters.push(`order=${this.data.order}`);
    }
    if (this.data.orderby && this.data.orderby.length) {
      parameters.push(`orderby=${this.data.orderby}`);
    }

    if (this.data.keyword && this.data.keyword.length) {
      parameters.push(`keyword=${this.data.keyword}`);
    }

    if (this.data.taxonomies) {
      Object.keys(this.data.taxonomies).forEach((key) => {
        parameters.push(`filter-${key}=${this.data.taxonomies[key]}`);
      });
    }

    const url = parameters.length ? `?${parameters.join("&")}` : this.baseURL;

    window.history.replaceState({}, null, url);
  }

  /**
   * AJAX load posts and reset page to 1 to ensure first set of results loaded
   */
  async newLoadPosts() {
    this.data.paged = 0;

    return this.loadPosts();
  }

  /**
   * Toggle the loading state of the component
   *
   * @param state
   */
  toggleLoadingState(state) {
    this.loading = state === true;
    if (this.loading) {
      this.module.classList.add("loading");
    } else {
      this.module.classList.remove("loading");
    }
  }

  /**
   * AJAX load the next set of posts
   */
  loadPosts() {
    // AJAX load posts
    if (this.loading) {
      return;
    }
    this.toggleLoadingState(true);

    // Increment the page
    if (this.loadMoreType === "load-more") {
      this.data.paged += 1;
    }
    this.fetch = new Promise((resolve, reject) => {
      const request = new XMLHttpRequest();

      request.open(
        "POST",
        `${JSON_URL + this.action}?_=${new Date().getTime()}`,
        true
      );

      request.setRequestHeader("Content-Type", "application/json");
      request.setRequestHeader("accept", "application/json");

      request.onload = () => {
        if (request.status !== 200) {
          return reject({
            status: request.status,
            statusText: request.statusText,
          });
        }

        const response = JSON.parse(request.responseText);

        if (this.data.paged === 1 || this.loadMoreType !== "load-more") {
          this.container.innerHTML = "";
        }
        this.loadPostTemplates(response.data);

        this.data["max-pages"] = response.maxPages;
        this.data.paged = response.currentPage;

        if (this.loadMoreType === "load-more") {
          this.updateLoadMoreState();
        } else if (this.loadMoreType === "pagination") {
          this.pagination.innerHTML = response.pagination;
          this.initPagination();
        }

        this.toggleLoadingState(false);

        const postEvent = createEvent("loadPosts");
        this.list.dispatchEvent(postEvent);

        return resolve(response);
      };

      request.onerror = () => {
        reject({
          status: this.request.status,
          statusText: this.request.statusText || "Couldn't retrieve posts",
        });
      };

      request.send(
        JSON.stringify({
          postArguments: this.data,
        })
      );
    }).catch((error) => {
      this.toggleLoadingState(false);
      console.error("Error with post listing load:", error);
    });
  }

  /**
   * Load the post templates via Handlebars
   *
   * @param postData
   */
  loadPostTemplates(postData) {
    // Add ajax item class to each item
    for (let i = 0; i < postData.length; i += 1) {
      const data = postData[i];

      const template = new TemplateLoader({
        template: data.template,
        data: data.templateData,
      });

      template.getCompiled().then((t) => {
        this.container.insertAdjacentHTML("beforeend", t);
        window.WordpressApp.autoloader.loadTemplates();
      });
    }
  }
}
