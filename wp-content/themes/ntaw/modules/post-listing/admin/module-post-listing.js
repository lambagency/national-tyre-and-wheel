/* eslint-disable */
(function($) {
  "use strict";

  var layoutIDs = ["post-listing"];

  var layoutInstances = [];

  var Layout = {
    layout: null,
    postID: null,
    postTypesRepeater: null,
    postTypes: [],
    taxonomySelect: null,
    postsSelect: null,
    defaultTaxonomySelect: null,

    init: function(layoutElement) {
      var instance = this;

      // Init layout
      instance.layout = layoutElement;
      instance.layout.addClass("initialised");

      instance.postID = $("#post_ID").val();

      instance.postTypesRepeater = instance.layout.find(
        '[data-name="post_types"]'
      );
      instance.taxonomySelect = instance.layout.find(
        '[data-name="module_ptl_filter_taxonomies"] select'
      );
      instance.postsSelect = instance.layout.find(
        '[data-name="module_ptl_manual_posts"] select'
      );
      instance.defaultTaxonomySelect = instance.layout.find(
        '[data-name="module_ptl_default_taxonomy"] select'
      );

      instance.initPostTypeRows();
    },

    /**
     * Triggered on add action append
     * @param $el
     */
    actionAppend: function($el) {
      var instance = this;

      instance.initPostTypeRows();
    },

    /**
     * Triggered on add action remove
     * @param $el
     */
    actionRemove: function($el) {
      var instance = this;

      if ($el.hasClass("post-type-row")) {
        setTimeout(function() {
          instance.updatePostTypes();
        }, 600);
      }
    },

    /**
     * Initialise the post types rows
     */
    initPostTypeRows: function() {
      var instance = this;

      var rows = instance.layout.find(".acf-row:not(.acf-clone)");

      // Loop through each post types row and populate the posttypes array
      $.each(rows, function() {
        instance.initPostTypeRow($(this));
      });
    },

    /**
     * Initialise individual post types ro
     */
    initPostTypeRow: function(row) {
      var instance = this;

      // Only initialise row once
      if (row.hasClass("initialised")) {
        return;
      }

      row.addClass("post-type-row initialised");

      // Get post type and template fields from row
      var postTypeSelect = row.find(
          '.acf-field[data-name="module_ptl_post_type"] select'
        ),
        templateSelect = row.find(
          '.acf-field[data-name="module_ptl_template"] select'
        ),
        postType = postTypeSelect.val(),
        template = templateSelect.val();

      // Init
      instance.updateRowTemplates(postType, templateSelect);
      instance.updatePostTypes();

      // Bind change events to select fields to ensure values updated
      postTypeSelect.unbind().bind("change", function() {
        instance.updateRowTemplates($(this).val(), templateSelect);
        instance.updatePostTypes();
      });

      templateSelect.unbind().bind("change", function() {
        instance.updatePostTypes();
      });
    },

    /**
     * Update the available row templates based on selected post types
     *
     * @param postType
     * @param templateSelect
     */
    updateRowTemplates: function(postType, templateSelect) {
      // Selected Template
      var selectedTemplate = templateSelect.val();

      // Remove template options
      templateSelect.find("option").remove();

      // Update templates select to only show templates for matching post type
      var data = PostListingJS.postTypes[postType]["templates"];

      // If templates found for selected post type, populate dropdown
      if (data.length) {
        $.each(data, function(i, option) {
          var value = option.value,
            label = option.label,
            selected = selectedTemplate == value ? " selected" : "";

          templateSelect.append(
            '<option value="' +
              value +
              '"' +
              selected +
              ">" +
              label +
              "</option>"
          );
        });
      }
    },

    /**
     * Update the post types value array
     */
    updatePostTypes: function() {
      var instance = this;

      instance.postTypes = [];

      var rows = instance.layout.find(".acf-row:not(.acf-clone)");

      // Loop through each post types row and populate the posttypes array
      $.each(rows, function() {
        // Get post type and template fields from row
        var postTypeSelect = $(this).find(
            '.acf-field[data-name="module_ptl_post_type"] select'
          ),
          templateSelect = $(this).find(
            '.acf-field[data-name="module_ptl_template"] select'
          ),
          postType = postTypeSelect.val(),
          template = templateSelect.val();

        // Add row values to array
        instance.postTypes[postType] = {
          template: template
        };
      });

      instance.updateTaxonomies();
      instance.updateDefaultTaxonomies();
    },

    /**
     * Update taxonomies list
     */
    updateTaxonomies: function() {
      var instance = this;

      if (!instance.taxonomySelect) {
        return;
      }

      // Selected Taxonomy
      var selectedTaxonomies = instance.taxonomySelect.val();

      // Update templates select to only show templates for matching post type
      var taxonomySelectValues = [];

      for (var postType in instance.postTypes) {
        var data = PostListingJS.postTypes[postType]["taxonomies"];

        // If templates found for selected post type, populate dropdown
        if (data.length) {
          $.each(data, function(i, option) {
            var selected = false;

            if (selectedTaxonomies) {
              selected = selectedTaxonomies.indexOf(option.value) >= 0;
            }

            taxonomySelectValues.push({
              value: option.value,
              label: option.label,
              selected: selected ? " selected" : ""
            });
          });
        }
      }

      // Remove taxonomy options
      instance.taxonomySelect.find("option").remove();

      var usedTaxonomies = [];

      for (var i in taxonomySelectValues) {
        // Ensure no duplicates are added
        if (usedTaxonomies.indexOf(taxonomySelectValues[i].value) >= 0) {
          continue;
        }

        instance.taxonomySelect.append(
          '<option value="' +
            taxonomySelectValues[i].value +
            '"' +
            taxonomySelectValues[i].selected +
            ">" +
            taxonomySelectValues[i].label +
            "</option>"
        );
        usedTaxonomies.push(taxonomySelectValues[i].value); // Add to used array
      }
    },

    updateDefaultTaxonomies: function() {
      var instance = this;

      if (!instance.defaultTaxonomySelect) {
        return;
      }

      console.log(PostListingJS.postTypes);

      // Selected Default Taxonomy
      var selectedTaxonomies = instance.defaultTaxonomySelect.val();

      // Update templates select to only show templates for matching post type
      var taxonomySelectValues = [];

      for (var postType in instance.postTypes) {
        var data = PostListingJS.postTypes[postType]["terms"];

        // If templates found for selected post type, populate dropdown
        if (data.length) {
          $.each(data, function(i, option) {
            var selected = false;

            if (selectedTaxonomies) {
              selected = selectedTaxonomies.indexOf(option.value) >= 0;
            }

            taxonomySelectValues.push({
              value: option.value,
              label: option.label,
              selected: selected ? " selected" : ""
            });
          });
        }
      }

      // Remove taxonomy options
      instance.defaultTaxonomySelect.find("option").remove();

      var usedTaxonomies = [];

      for (var i in taxonomySelectValues) {
        // Ensure no duplicates are added
        if (usedTaxonomies.indexOf(taxonomySelectValues[i].value) >= 0) {
          continue;
        }

        instance.defaultTaxonomySelect.append(
          '<option value="' +
            taxonomySelectValues[i].value +
            '"' +
            taxonomySelectValues[i].selected +
            ">" +
            taxonomySelectValues[i].label +
            "</option>"
        );
        usedTaxonomies.push(taxonomySelectValues[i].value); // Add to used array
      }
    }
  };

  /**
   * Initialise layouts
   * The below code should remain unchanged
   */

  $(document).ready(function() {
    // Check if ACF defined
    if (typeof acf === "undefined") {
      return;
    }

    // Initialise layouts on load
    acf.add_action("load", function($el) {
      initialiseLayouts();
    });

    // Initialise additional layouts on append
    acf.add_action("append", function($el) {
      initialiseLayouts();

      for (var i = 0; i < layoutInstances.length; i++) {
        layoutInstances[i].actionAppend($el);
      }
    });

    // Initialise layout functions on layout remove
    acf.add_action("remove", function($el) {
      for (var i = 0; i < layoutInstances.length; i++) {
        layoutInstances[i].actionRemove($el);
      }
    });
  });

  /**
   * Initialise flexible layouts that match layoutID
   */
  function initialiseLayouts() {
    $.each(layoutIDs, function(i, layoutID) {
      // Match layouts that are not already initialised
      var matchedLayouts = $(
        '.acf-field-flexible-content .acf-flexible-content > .values [data-layout="' +
          layoutID +
          '"]:not(.initialised)'
      );

      if (matchedLayouts.length) {
        matchedLayouts.each(function() {
          var layoutInstance = Object.create(Layout);
          layoutInstance.init($(this));
          layoutInstances.push(layoutInstance);
        });
      }
    });
  }
})(jQuery);
