<?php
namespace LambAgency\Modules;

use LambAgency\Components\TitleBlock;
use LambAgency\Components\Link;
use LambAgency\Components\Button;

/**
 * @package LambAgency\Modules
 */
class ModuleLogoList extends AbstractModule
{
    public static $icon = "excerpt-view";

    private $titleBlock = null;
    private $logos = [];
    private $buttons = [];

    public function __construct($block)
    {
        parent::__construct($block);

        $this->setTitleBlock();
        $this->setLogos();
        $this->setButtons();
    }

    private function setTitleBlock()
    {
        $this->titleBlock = new TitleBlock($this->fields['title_block']);
    }

    /**
     * @return TitleBlock|null
     */
    public function getTitleBlock()
    {
        return $this->titleBlock;
    }

    private function setLogos()
    {
        if (is_array($this->fields['logos']) && !empty($this->fields['logos'])) {
            foreach($this->fields['logos'] as $logoGridItem) {
                $logoGridItem['link'] = new Link($logoGridItem['link']);

                $this->logos[] = $logoGridItem;
            }
        }
    }

    /**
     * @return array
     */
    public function getLogos()
    {
        return $this->logos;
    }


    private function setButtons()
    {
        if (!empty($this->fields['buttons'])) {
            foreach($this->fields['buttons'] as $button) {
                $this->buttons[] = new Button($button);
            }
        }
    }

    /**
     * @return Button[]|array
     */
    public function getButtons()
    {
        return $this->buttons;
    }
}
