import Glide from "@glidejs/glide";
import "@glidejs/glide/src/assets/sass/glide.core.scss";
import "@glidejs/glide/src/assets/sass/glide.theme.scss";

import AbstractModule from "class/AbstractModule";

/**
 * ModuleTestimonials Class
 */
export default class ModuleLogoList extends AbstractModule {
  constructor(module) {
    super(module);

    this.slider = this.module.querySelector(".slider");
    this.slide = this.slider.querySelectorAll(".slide");

    [this.currentSlide] = this.slides;

    this.initLogoListSlider();
  }

  initLogoListSlider() {
    this.glide = new Glide(this.slide, {
        gap: 0
    });
      
    this.glide.on("build.after", () => {
        this.currentSlide.classList.add("slide-animate");
      });
  
      this.glide.on("run.after", () => {
        this.currentSlide.classList.remove("slide-animate");
        this.currentSlide = this.slides[this.glide.index];

        this.currentSlide.classList.add("slide-animate");
        window.WordpressApp.refreshLazyImages();
    });
  
    this.glide.mount();

  }
}
