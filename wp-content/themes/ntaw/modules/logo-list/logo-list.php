<?php
/**
 * @var LambAgency\Modules\ModuleLogoList $module
 * @var array $fields
 */
$logos = $module->getLogos();
?>

<div class="<?php echo esc_attr($module->getModuleClass()); ?>" data-autoload-module="logo-list">

    <?php
    $titleBlock = new \LambAgency\Components\TitleBlock($fields['title_block']);

    if ($titleBlock->showTitleBlock()) : ?>
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell"><?php $titleBlock->renderTitleBlock(); ?></div>
            </div>
        </div>
    <?php endif; ?>

    <div class="grid-container">
        <?php if (!empty($fields['logos'])) : ?>
            <div class="logo grid-x grid-margin-x grid-margin-y">
                <?php foreach ($fields['logos'] as $logo) :
                    $link = new \LambAgency\Components\Link($logo['link']);
                    ?>
                    <div class="logo-box cell small-12 medium-6 large-4">
                        <?php if ($link->getURL()) : ?>
                            <a class="icon-link"
                               target="<?php echo esc_attr($link->getTarget()); ?>"
                               href="<?php echo esc_url($link->getURL()); ?>">
                        <?php endif; ?>
                        <?php if (!empty($logo['image'])) : ?>
                            <img src="<?php echo $logo['image']['sizes']['square-medium']; ?>"
                                 width="<?php echo $logo['image']['sizes']['square-medium-width']; ?>"
                                 height="<?php echo $logo['image']['sizes']['square-medium-height']; ?>" 
                            />
                            <div class="logo-box-content">
                                <h3><?php echo $link->getTitle(); ?></h3>
                            </div>
                        <?php endif; ?>
                        <?php if ($link->getURL()) : ?>
                            </a>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

    <?php if (!empty($fields['buttons'])) : ?>
        <div class="grid-container">
            <div class="button-group button-group-margin-top align-center">
                <?php
                foreach ($fields['buttons'] as $buttonFields) :
                    $button = new \LambAgency\Components\Button($buttonFields);
                    $button->renderButton();
                endforeach;
                ?>
            </div>
        </div>
    <?php endif; ?>
</div>

