<?php
/**
 * @var LambAgency\Modules\ModuleMediaFeature $module
 * @var array $fields
 */

$position = $module->getPosition();
$gutters = $module->getMediaGutters();
?>

<div class="<?php echo esc_attr($module->getModuleClass()); echo isset($fields['remove_shadow']) && $fields['remove_shadow']  ? ' no-shadow' : ''; ?>" data-autoload-module="media-feature">
    <div class="grid-container">
        <div class="media-feature <?php echo esc_attr(sprintf('gutters-%s media-position-%s', $gutters, $position)); ?> grid-x grid-margin-y align-middle">
            <div class="media-feature-media cell medium-6 small-12<?php echo esc_attr($position === 'right' ? ' medium-order-2' : ''); ?>">
                <div class="media-feature-media-wrapper">
                    <?php
                    $image = !empty($fields['image']) ? $fields['image']['sizes']['large'] : '';
                    ?>
                    <img class="media-feature-image" src="<?php echo esc_url($image); ?>" alt="">
                </div>
            </div>
            <div class="media-feature-text cell medium-6 small-12<?php echo esc_attr($position === 'right' ? ' medium-order-0' : ''); ?>">
                <div class="title-block">
                    <?php if ($subtitle = $fields['subtitle']) : ?>
                        <p class="title-block-subtitle pusher"><?php echo esc_html($subtitle); ?></p>
                    <?php endif; ?>
                    <?php if ($title = $fields['title']) : ?>
                        <h2 class="title-block-title color-dark dash"><?php echo esc_html($title); ?></h2>
                    <?php endif; ?>
                </div>
                <div class="rte">
                    <?php echo wp_kses_post($fields['content']); ?>
                </div>
                <?php if (!empty($fields['buttons'])) : ?>
                    <div class="button-group button-group-margin-top-small align-left">
                        <?php
                        foreach ($module->getButtons() as $button) :
                            $button->renderButton();
                        endforeach;
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
