<?php
namespace LambAgency\Modules;

use LambAgency\Components\Button;

/**
 * Class ModuleMediaFeature
 *
 * @package LambAgency\Modules
 */
class ModuleMediaFeature extends AbstractModule
{
    public static $icon = "align-left";

    private $position = 'left';
    private $mediaGutters = 'off';
    private $buttons = [];

    public function __construct($block)
    {
        parent::__construct($block);

        $this->setPosition();
        $this->setMediaGutters();
        $this->setButtons();
    }


    /**
     * Get the position of the media column
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }


    /**
     * Set the position of the media column
     */
    public function setPosition()
    {
        $this->position = !empty($this->fields['media_position']) ? $this->fields['media_position'] : $this->position;
    }


    /**
     * Get the media gutter
     *
     * @return string
     */
    public function getMediaGutters()
    {
        return $this->mediaGutters;
    }


    /**
     * Set the media gutter
     */
    public function setMediaGutters()
    {
        $this->mediaGutters = !empty($this->fields['media_gutters']) ? $this->fields['media_gutters'] : $this->mediaGutters;
    }

    private function setButtons()
    {
        if (!empty($this->fields['buttons'])) {
            foreach($this->fields['buttons'] as $button) {
                $this->buttons[] = new Button($button);
            }
        }
    }

    /**
     * @return Button[]|array
     */
    public function getButtons()
    {
        return $this->buttons;
    }
}
