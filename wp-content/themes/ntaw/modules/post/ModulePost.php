<?php
namespace LambAgency\Modules;

Use \LambAgency\Components\SocialShare;

/**
 * Class ModulePost
 *
 * @package LambAgency\Modules
 */
class ModulePost extends AbstractModule
{
    public static $icon = "admin-post";

    private $shares;

    public function __construct($block)
    {
        parent::__construct($block);
        $this->setShares();
    }

    private function setShares()
    {
        $this->shares = new SocialShare();
    }

    public function getShares()
    {
        return $this->shares;
    }
}
