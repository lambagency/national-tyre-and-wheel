<?php
/**
 * @var LambAgency\Modules\ModulePost $module
 * @var array $fields
 */

$moduleClasses[] = isset($fields['content']) && $fields['content'] ? '' : ' no-content';
?>

<div class="<?php echo esc_attr($module->getModuleClass()); ?>" data-autoload-module="post">
    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-margin-y">
            <div class="cell small-12 medium-12 rte content">
                <?php echo wp_kses_post($fields['content']); ?>
            </div>
            <?php //if ($fields['socials']) : ?>
                <!-- <div class="cell small-12 medium-4 social">
                    <?php //$shares = $module->getShares();
                        //$shares->renderShareButton();
                    ?>
                </div> -->
            <?php //endif ?>
        </div>
    </div>
</div>
