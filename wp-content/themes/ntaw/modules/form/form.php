<?php
/**
 * @var LambAgency\Modules\ModuleForm $module
 * @var array $fields
 */
?>

<div data-autoload-module="form" class="<?php echo esc_attr($module->getModuleClass()); ?>">

    <?php
    $titleBlock = $module->getTitleBlock();

    if ($titleBlock->showTitleBlock()) : ?>
        <div class="grid-container">
            <div class="grid-x grid-margin-y grid-margin-x">
                <div class="rte cell large-6">
                    <div class="cell"><?php $titleBlock->renderTitleBlock(); ?></div>

                    <?php if (!empty($fields['content'])) : ?>
                        <div class="rte cell"><?php echo wp_kses_post($fields['content']); ?></div>
                    <?php endif; ?>
                </div>

                <div class="form-container rte cell large-6">
                    <?php
                        $formComponent = $module->getForm();
                        $formComponent->renderForm();
                    ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
