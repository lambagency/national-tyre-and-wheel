<?php
namespace LambAgency\Modules;

use LambAgency\Components\TitleBlock;
use LambAgency\Components\Form;

/**
 * Class ModuleForm
 *
 * @package LambAgency\Modules
 */
class ModuleForm extends AbstractModule
{
    public static $icon = "editor-table";

    private $titleBlock;
    private $form;

    public function __construct($block)
    {
        parent::__construct($block);

        $this->setTitleBlock();
        $this->setForm();
    }

    private function setTitleBlock()
    {
        $this->titleBlock = new TitleBlock($this->fields['title_block']);
    }

    /**
     * @return TitleBlock
     */
    public function getTitleBlock()
    {
        return $this->titleBlock;
    }

    private function setForm()
    {
        $this->form = new Form($this->fields['form']);
    }

    /**
     * @return Form
     */
    public function getForm()
    {
        return $this->form;
    }
}
