<?php
namespace LambAgency\Modules;

use LambAgency\Components\Image;
use LambAgency\Components\Button;

/**
 * Class ModulePageHeader
 *
 * @package LambAgency\Modules
 */
class ModulePageHeader extends AbstractModule
{

    /**
     * @var Image
     */
    private $backgroundImage;
    private $buttons = [];


    public function __construct($block)
    {
        parent::__construct($block);

        $this->setBackgroundImage();
        $this->setButtons();
    }


    /**
     * Get the background image
     *
     * @return \LambAgency\Components\Image
     */
    public function getBackgroundImage()
    {
        return $this->backgroundImage;
    }


    /**
     * Set the background image
     * Checks if background image is set to none, featured image or custom
     */
    public function setBackgroundImage()
    {

        switch ($this->fields['background_image']) {
        case 'none':
            break;
        case 'featured-image':
            $postID = get_the_ID();

            // Fallback to feature image if feature image is set
            if (has_post_thumbnail($postID)) {
                $image = \LambAgency\Utility::getFeaturedImageArray($postID);
                if ($image) {
                    $this->backgroundImage = new Image($image);
                }
            }
            break;
        case 'image':
            if ($this->fields['image']) {
                $this->backgroundImage = new Image($this->fields['image']);
            }
            break;
        }
    }


    private function setButtons()
    {
        if (!empty($this->fields['buttons'])) {
            foreach($this->fields['buttons'] as $button) {
                $this->buttons[] = new Button($button);
            }
        }
    }

        /**
         * @return Button[]|array
         */
    public function getButtons()
    {
        return $this->buttons;
    }
}
