<?php
/**
 * @var LambAgency\Modules\ModuleMediaFeature $module
 * @var array $fields
 */

$module->addModuleClass(['theme-dark', 'no-padding', 'no-border']);
?>

<div class="<?php echo esc_attr($module->getModuleClass()); ?>" data-autoload-module="page-header">

    <?php if ($backgroundImage = $module->getBackgroundImage()) : ?>
        <?php $backgroundImage->renderImage(); ?>
    <?php endif; ?>

    <div class="grid-container">
        <div class="page-header-inner grid-x align-center align-middle">
            <div class="cell">
                <?php
                if (function_exists('yoast_breadcrumb')) {
                    yoast_breadcrumb('<p id="breadcrumbs" class="page-header-breadcrumbs small-label">', '</p>');
                }
                ?>
                <h2 class="page-header-title dash"><?php echo esc_html(do_shortcode($fields['title'])); ?></h2>
                <!-- <p class="page-header-content lead"><?php echo wp_kses_post($fields['content']); ?></p> -->

                <!-- <?php if (!empty($fields['buttons'])) : ?>
                    <div class="banner-buttons button-group button-group-margin-top-small align-center">
                        <?php
                        foreach ($module->getButtons() as $button) :
                            $button->renderButton();
                        endforeach;
                        ?>
                    </div>
                <?php endif; ?> -->

            </div>
        </div>
    </div>
</div>
