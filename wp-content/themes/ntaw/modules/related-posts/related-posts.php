<?php
/**
 * @var LambAgency\Modules\ModuleRelatedPosts $module
 * @var array $fields
 */
?>

<?php if ($relatedPosts = $module->getRelatedPosts()) : ?>
    <div class="<?php echo esc_attr($module->getModuleClass()); ?>">

        <?php
        $titleBlock = $module->getTitleBlock();

        if ($titleBlock->showTitleBlock()) : ?>
            <div class="grid-container">
                <div class="grid-x text-center">
                <div class="cell"><?php $titleBlock->renderTitleBlock(); ?></div>
                </div>

            </div>
        <?php endif; ?>

        <div class="grid-container">
            <div class="grid-x grid-margin-x grid-margin-y">
                <?php
                // TODO: escape this output
                foreach ($relatedPosts as $relatedPost) :
                    echo $relatedPost['html'];
                endforeach; ?>
            </div>
        </div>

        <?php if (!empty($fields['buttons'])) : ?>
            <div class="grid-container">
                <div class="button-group button-group-margin-top align-center">
                    <?php
                    foreach ($module->getButtons() as $button) :
                        $button->renderButton();
                    endforeach;
                    ?>
                </div>
            </div>
        <?php endif; ?>

    </div>
<?php endif; ?>
