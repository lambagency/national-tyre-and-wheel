<?php
namespace LambAgency\Modules;

use LambAgency\Components\TitleBlock;
use LambAgency\Components\Button;

/**
 * Class ModuleRelatedPosts
 *
 * @package LambAgency\Modules
 */
class ModuleRelatedPosts extends AbstractModule
{

    /**
     * @var \WP_Post The current post
     */
    private $post;

    /**
     * @var array Related taxonomies to the current post
     */
    private $relatedPostTaxonomies;

    /**
     * @var array Related posts
     */
    public $relatedPosts = [];

    private $titleBlock = null;
    private $buttons = [];

    public function __construct()
    {
        parent::__construct();

        $this->post = get_post();
        $this->relatedPostTaxonomies = get_object_taxonomies($this->post->post_type);

        $this->setRelatedPosts();
        $this->setTitleBlock();
        $this->setButtons();
    }


    /**
     * Get the related posts
     *
     * @return array
     */
    public function getRelatedPosts()
    {
        return $this->relatedPosts;
    }


    /**
     * Set related posts based on sharing of taxonomies
     */
    public function setRelatedPosts()
    {
        $postArgs = [
            'post_type'         => $this->post->post_type,
            'posts_per_page'    => 3,
            'post__not_in'      => [$this->post->ID]
        ];

        if (count($this->relatedPostTaxonomies)) {
            $taxQuery = [
                'relation' => 'OR'
            ];

            foreach ($this->relatedPostTaxonomies as $taxonomy) {
                $termSlugs = [];
                $terms = wp_get_post_terms($this->post->ID, $taxonomy);

                foreach ($terms as $term) {
                    $termSlugs[] = $term->slug;
                }

                $taxQuery[] = [
                    'taxonomy' => $taxonomy,
                    'field'    => 'slug',
                    'terms'     => $termSlugs
                ];
            }

            $postArgs['tax_query'] = $taxQuery;
        }

        $posts = get_posts($postArgs);

        foreach ($posts as $post) {
            $templateObj = new \LambAgency\Template\Post($post);
            $this->relatedPosts[] = [
                'html' => $templateObj->getTemplateHTML()
            ];
        }
    }

    private function setTitleBlock()
    {
        $this->titleBlock = new TitleBlock($this->fields['title_block']);
    }

    /**
     * @return TitleBlock|null
     */
    public function getTitleBlock()
    {
        return $this->titleBlock;
    }

    private function setButtons()
    {
        if (!empty($this->fields['buttons'])) {
            foreach($this->fields['buttons'] as $button) {
                $this->buttons[] = new Button($button);
            }
        }
    }

    /**
     * @return Button[]|array
     */
    public function getButtons()
    {
        return $this->buttons;
    }
}
