<?php
/**
 * @var \LambAgency\Modules\ModuleAccordion $module
 * @var array $fields
 */
?>
<div data-autoload-module="accordion"
     class="<?php echo esc_attr($module->getModuleClass()); ?>">

    <?php
    $titleBlock = $module->getTitleBlock();

    if ($titleBlock->showTitleBlock()) : ?>
        <div class="grid-container">
            <div class="grid-x text-center">
                <div class="cell"><?php $titleBlock->renderTitleBlock(); ?></div>
            </div>
        </div>
    <?php endif; ?>

    <?php if (count($module->getItems()) > 0) : ?>
        <div class="grid-container">
            <div class="accordion">
                <?php
                  foreach ($module->getItems() as $key => $item) :
                    $accordionID = sprintf(
                        'accordion-%d-item-%d',
                        $module->getLayoutIndex(),
                        $key
                      );
                  ?>
                    <div class="accordion-item<?php if ($item['open_on_load']) {
                        echo esc_attr(' active');
} ?>">
                        <button
                          class="accordion-title grid-x grid-margin-x align-middle align-justify"
                          aria-controls="<?php echo esc_attr($accordionID); ?>"
                          <?php if ($item['open_on_load']): ?>
                            aria-expanded="true"
                          <?php endif; ?>
                        >
                            <div class="cell auto">
                                <h3 class="accordion-title-text"><?php echo esc_html($item['title']); ?></h3>
                            </div>
                            <div class="accordion-toggle cell shrink">
                                <span class="accordion-toggle-icon"></span>
                            </div>
                        </button>
                        <div
                          class="accordion-content rte"
                          id="<?php echo esc_attr($accordionID); ?>"
                        >
                            <?php echo wp_kses_post($item['content']); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($fields['buttons'])) : ?>
        <div class="grid-container">
            <div class="button-group button-group-margin-top align-center">
                <?php
                    foreach ($fields['buttons'] as $buttonFields) :
                        $button = new \LambAgency\Components\Button($buttonFields);
                        $button->renderButton();
                    endforeach;
                ?>
            </div>
        </div>
    <?php endif; ?>

</div>
