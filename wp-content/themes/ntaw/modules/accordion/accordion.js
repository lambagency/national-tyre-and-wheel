import AbstractModule from "class/AbstractModule";

export default class ModuleAccordion extends AbstractModule {
  constructor(module) {
    super(module);

    this.initAccordion();
  }

  initAccordion() {
    this.items = this.module.querySelectorAll(".accordion-item");

    for (let i = this.items.length - 1; i >= 0; i -= 1) {
      const item = this.items[i];
      const title = item.querySelector(".accordion-title");
      // Have to name the event listener so it can be unmounted on hot reloads
      let listener;

      title.addEventListener(
        "click",
        (listener = () => {
          item.classList.toggle("active");
          if (item.classList.contains("active")) {
            title.setAttribute("aria-expanded", "true");
          } else {
            title.setAttribute("aria-expanded", "false");
          }
        })
      );

      if (module.hot) {
        this.listeners.push({
          type: "click",
          element: title,
          listener
        });
      }
    }
  }
}
