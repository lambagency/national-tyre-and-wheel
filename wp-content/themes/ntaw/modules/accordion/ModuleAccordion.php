<?php
namespace LambAgency\Modules;

use LambAgency\Components\TitleBlock;
use LambAgency\Components\Button;

/**
 * Class Accordion
 *
 * @package LambAgency\Modules
 */
class ModuleAccordion extends AbstractModule
{
    public static $icon = "sort";

    private $titleBlock = null;
    private $items = [];
    private $buttons = [];

    public function __construct($block)
    {
        parent::__construct($block);

        if (!empty($this->fields['items'])) {
            $this->setItems($this->fields['items']);
        }

        $this->setTitleBlock();
    }

    private function setTitleBlock()
    {
        $this->titleBlock = new TitleBlock($this->fields['title_block']);
    }

    /**
     * @return TitleBlock
     */
    public function getTitleBlock()
    {
        return $this->titleBlock;
    }

    /**
     * Get the accordion items
     *
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }


    /**
     * Set the accordion items
     *
     * @param array $items
     */
    public function setItems(array $items)
    {
        if (!empty($items)) {
            foreach ($items as $item) {
                $this->items[] = $item;
            }
        }
    }



    /**
     * Adds the accordion items into the tab structure
     *
     * @return array
     */
    public function populateTabs()
    {
        $multiExpand = $allowAllClosed = false;
        $accordionClass = '';

        if($this->getAccordionType() === 'expandable') {
            $multiExpand = $allowAllClosed = true;
            $accordionClass = 'auto-expanding';
        }

        $tabsComponentData = [
            'structure'         => 'accordion',
            'multi-expand'      => $multiExpand,
            'allow-all-closed'  => $allowAllClosed,
            'tabs'              => [],
            'accordion-class'   => $accordionClass
        ];

        while (have_rows('items')) {

            the_row();

            $tab = [
                'title'     => get_sub_field('title'),
                'content'   => get_sub_field('content')
            ];

            $tabsComponentData['tabs'][] = $tab;
        }

        return $tabsComponentData;
    }


    /**
     * Get the accordion tabs component
     */
    public function getAccordionTabs()
    {
        return $this->accordionTabsComponent;
    }

    /**
     * Set the accordion type
     */
    public function setAccordionType()
    {
        $this->accordionType = !empty($this->fields['type']) ? $this->fields['type'] : '';
    }

    /**
     * Get the accordion type
     */
    public function getAccordionType()
    {
        return $this->accordionType;
    }

    private function setButtons()
    {
        if (!empty($this->fields['buttons'])) {
            foreach($this->fields['buttons'] as $button) {
                $this->buttons[] = new Button($button);
            }
        }
    }

    /**
     * @return Button[]|array
     */
    public function getButtons()
    {
        return $this->buttons;
    }
}
