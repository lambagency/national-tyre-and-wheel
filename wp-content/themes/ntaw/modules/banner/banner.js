import Glide from "@glidejs/glide";
import "@glidejs/glide/src/assets/sass/glide.core.scss";

import "plugins/closestPolyfill";
import AbstractModule from "class/AbstractModule";

/**
 * Banner Class
 */
export default class ModuleBanner extends AbstractModule {
  constructor(m) {
    super(m);
    this.slider = this.module.querySelector(".slider");

    this.slides = this.slider.querySelectorAll(".slide");

    [this.currentSlide] = this.slides;

    this.videos = this.module.querySelector(
      "[data-autoload-component='video']"
    );

    this.initBannerSlider();
  }

  initBannerSlider() {
    this.glide = new Glide(this.slider, {
      gap: 0
    });

    // TODO: Clear old event listeners after hot reload
    this.glide.on("build.after", () => {
      this.currentSlide.classList.add("slide-animate");
      window.WordpressApp.refreshLazyImages();
    });

    this.glide.on("run.after", () => {
      this.currentSlide.classList.remove("slide-animate");
      this.currentSlide = this.slides[this.glide.index];

      this.currentSlide.classList.add("slide-animate");
      this.refreshVideos();
      window.WordpressApp.refreshLazyImages();
    });

    this.glide.mount();
  }

  /**
   * Play active videos and pause inactive videos
   */
  refreshVideos() {
    if (this.videos && this.videos.length) {
      for (let i = this.videos.length - 1; i >= 0; i -= 1) {
        const video = this.videos[i];
        const videoClass = video.component;
        if (video.closest(".glide__slide--active").length > 0) {
          videoClass.playVideo();
        } else {
          videoClass.pauseVideo();
        }
      }
    }
  }
}
