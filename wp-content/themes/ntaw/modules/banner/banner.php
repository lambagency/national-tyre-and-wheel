<?php
/**
 * @var LambAgency\Modules\ModuleBanner $module
 * @var array $fields
 */

$module->addModuleClass('no-border');
$module->addModuleClass('theme-dark');

$slides = $module->getSlides();
?>

<div data-autoload-module="banner" class="<?php echo esc_attr($module->getModuleClass()); ?>">

    <div class="slider">

      <div class="glide__track" data-glide-el="track">
        <div class="glide__slides">

            <?php foreach ($slides as $key => $slide) : ?>
                <div class="slide glide__slide">
                    <?php
                    $slide['background']->renderImage();
                    ?>

                    <div class="grid-container h-100">
                        <div class="grid-x align-center align-middle h-100">

                            <div class="banner-slide-content cell medium-9 text-center">

                                <?php if (!empty($slide['subtitle'])) : ?>
                                    <p class="banner-subtitle color-white pusher"><?php echo esc_html($slide['subtitle']); ?></p>
                                <?php endif; ?>
                                
                                <h1 class="banner-title large-hero-title dash-center color-white"><?php echo esc_html($slide['title']); ?></h1>

                                <?php if (!empty($slide['buttons'])) : ?>
                                    <div class="banner-buttons button-group align-center">
                                        <?php
                                        foreach ($slide['buttons'] as $buttonFields) :
                                            $button = new \LambAgency\Components\Button($buttonFields);
                                            $button->renderButton();
                                        endforeach;
                                        ?>
                                    </div>
                                <?php endif; ?>

                            </div>

                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
          </div>
        </div>
    </div>

</div>
