<?php
namespace LambAgency\Modules;

use \LambAgency\Components\Image;

/**
 * Class ModuleBanner
 *
 * @package LambAgency\Modules
 */
class ModuleBanner extends AbstractModule
{
    public static $icon = "format-gallery";


    private $slides = [];


    public function __construct($block)
    {
        parent::__construct($block);

        $this->setSlides();
    }

    /**
     * Get the banner slides
     *
     * @return array
     */
    public function getSlides()
    {
        return $this->slides;
    }


    /**
     * Set the banner slides
     */
    public function setSlides()
    {
        // If page has slides added
        if ($this->fields['slides']) {
            foreach ($this->fields['slides'] as $key => $slide) {
                $slide['background'] = new Image($slide['image']);
                $this->slides[] = $slide;
            }
        }
    }
}
