<?php

namespace LambAgency\Modules;

use LambAgency\Components\Image;
use LambAgency\PostType\Post;

/**
 * Class PostHeader
 *
 * @package LambAgency\Modules
 */
class ModulePostHeader extends AbstractModule
{

    /**
     * @var Image
     */
    private $backgroundImage;
    /**
     * @var Post
     */
    private $postClass;


    public function __construct($block)
    {
        parent::__construct($block);

        $this->setBackgroundImage();
        $this->setPostClass();
    }


    /**
     * Get the background image
     *
     * @return \LambAgency\Components\Image
     */
    public function getBackgroundImage()
    {
        return $this->backgroundImage;
    }


    /**
     * Set the background image
     * Checks if background image is set to none, featured image or custom
     */
    public function setBackgroundImage()
    {

        switch ($this->fields['background_image']) {
        case 'none':
            break;
        case 'featured-image':
            $postID = get_the_ID();

            // Fallback to feature image if feature image is set
            if (has_post_thumbnail($postID)) {
                $image = \LambAgency\Utility::getFeaturedImageArray($postID);
                if ($image) {
                    $this->backgroundImage = new Image($image);
                }
            }
            break;
        case 'image':
            if ($this->fields['image']) {
                $this->backgroundImage = new Image($this->fields['image']);
            }
            break;
        }
    }

    private function setPostClass()
    {
        $this->postClass = new Post();
    }

    public function getPostClass()
    {
        return $this->postClass;
    }
}
