<?php
/**
 * @var LambAgency\Modules\ModuleMediaFeature $module
 * @var array $fields
 */

$module->addModuleClass(['theme-dark', 'no-padding', 'no-border']);
$postClass = $module->getPostClass();
?>

<div class="<?php echo esc_attr($module->getModuleClass()); ?>" data-autoload-module="post-header">

    <?php if ($backgroundImage = $module->getBackgroundImage()) : ?>
        <?php $backgroundImage->renderImage(); ?>
    <?php endif; ?>

    <div class="grid-container">
        <div class="post-header-inner grid-x align-center align-middle">
            <div class="cell text-center">
                <time pubdate class="small-label">
                  <?php echo esc_html($postClass->getDate()); ?>
                </time>
                <h1 class="post-header-title">
                  <?php echo esc_html(do_shortcode($fields['title'])); ?>
                </h1>
                <div class="post-header-subtitle">
                  <?php echo esc_html($fields['subtitle']); ?>
                </div>
                <div class="post-header-author">
                  <?php echo esc_html(__(sprintf("Posted by %s", $postClass->getAuthor()))); ?>
                </div>
            </div>
        </div>
    </div>
</div>
