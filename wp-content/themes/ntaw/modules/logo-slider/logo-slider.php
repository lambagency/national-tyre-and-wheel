<?php
/**
 * @var \LambAgency\Modules\ModuleAnnouncement $module
 * @var array $fields
 */
?>
<div data-autoload-module="logo-slider" class="<?php echo esc_attr($module->getModuleClass()); ?>">
    <?php if (count($module->getLogos()) > 0) : ?>
        <div class="slider">
            <div class="glide">
                <?php $titleBlock = $module->getTitleBlock();

                if ($titleBlock->showTitleBlock()) : ?>
                    <div class="slider-title  grid-container">
                        <div class="grid-x text-center">
                            <div class="cell"><?php $titleBlock->renderTitleBlock(); ?></div>
                        </div>

                        <div class="slider-buttons" data-glide-el="controls">
                            <button type="button" data-glide-dir="&lt;" class="slide-prev slide-arrow">
                                <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8.30497 1.37502L2.67997 7.00002L8.30497 12.625L6.97915 13.9508L0.0283203 7.00002L6.97915 0.0491943L8.30497 1.37502Z" fill="#272628"/>
                                </svg>
                            </button>
                            <button type="button" data-glide-dir="&gt;" class="slide-arrow slide-next">
                                <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M2.02077 0.0491943L8.9716 7.00002L2.02077 13.9509L0.694946 12.625L6.31995 7.00002L0.694946 1.37502L2.02077 0.0491943Z" fill="#272628"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
                        <?php foreach ($fields['logos'] as $logo) :
                            $link = new \LambAgency\Components\Link($logo['link']);
                        ?>
                            <li class="glide__slide">
                                <div class="slider-logo-box">
                                    <?php if ($link->getURL()) : ?>
                                        <a class="icon-link"
                                        target="<?php echo esc_attr($link->getTarget()); ?>"
                                        href="<?php echo esc_url($link->getURL()); ?>">
                                    <?php endif; ?>
                                    
                                    <?php if (!empty($logo['image'])) : ?>
                                        <img src="<?php echo $logo['image']['sizes']['square-medium']; ?>"
                                            width="<?php echo $logo['image']['sizes']['square-medium-width']; ?>"
                                            height="<?php echo $logo['image']['sizes']['square-medium-height']; ?>" 
                                            alt="Company Logo"
                                        />
                                    <?php endif; ?>

                                    <?php if ($link->getURL()) : ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($fields['buttons'])) : ?>
        <div class="grid-container">
            <div class="button-group button-group-margin-top align-center">
                <?php
                foreach ($fields['buttons'] as $buttonFields) :
                    $button = new \LambAgency\Components\Button($buttonFields);
                    $button->renderButton();
                endforeach;
                ?>
            </div>
        </div>
    <?php endif; ?>
</div>