import Glide from "@glidejs/glide";
import "@glidejs/glide/src/assets/sass/glide.core.scss";
import "@glidejs/glide/src/assets/sass/glide.theme.scss";

import AbstractModule from "class/AbstractModule";

/**
 * ModuleTestimonials Class
 */
export default class ModuleLogoSlider extends AbstractModule {
  constructor(module) {
    super(module);

    this.initDefaultMargin();
    this.initLogoSlider();
  }

  initDefaultMargin() {
    if (screen.width > 1200) {
      this.module.querySelector('.glide__slides').style.marginLeft = ((screen.width - 1190) / 2).toString() + 'px'
    }
  }

  initLogoSlider() {
    new Glide('.glide', {
      type: 'slider',
      perView: 9.7,
      focusAt: 0.05,
      gap: 20,
      breakpoints: {
        2560: {
          perView: 13.15
        },
        1920: {
          perView: 9.8
        },
        1680: {
          perView: 8.65
        },
        1440: {
          perView: 7.4
        },
        1366: {
          perView: 7
        },
        1280: {
          perView: 6.65
        },
        1024: {
          perView: 5.25
        },
        768: {
          perView: 4.25
        },
        540: {
          perView: 3.3
        },
        415: {
          perView: 2.25,
          gap: 30
        },
        376: {
          perView: 2.3,
          gap: 10
        }
      }
    }).mount()
  }
}
