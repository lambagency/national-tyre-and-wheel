<div class="menu-top-wrap">

    <div class="menu-container">
        <?php
        if (has_nav_menu('header-main')) :
            wp_nav_menu(array(
                'theme_location'    => 'header-main',
                'menu_class'        => 'menu menu-top',
                'container'         => ''
            ));
        endif;
        ?>
    </div>

</div>
