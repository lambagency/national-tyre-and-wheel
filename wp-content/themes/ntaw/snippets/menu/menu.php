<div data-autoload-snippet="menu" class="menu-wrap menu">

	<nav id="header-menu-wrapper">
        <?php
        if (has_nav_menu('header-main')) :
            wp_nav_menu(array(
                'walker'            => new \LambAgency\Menus\DefaultNavWalker(),
                'theme_location'    => 'header-main',
                'menu_class'        => 'menu menu-main',
                'container'         => ''
            ));
        endif;
        ?>
	</nav>

</div>
