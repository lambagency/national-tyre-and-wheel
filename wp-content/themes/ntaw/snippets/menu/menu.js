import AbstractSnippet from "class/AbstractSnippet";

function menuActivate(el) {
  el.setAttribute("tabindex", 0);
  el.focus();
}

function menuDeactivate(el) {
  el.setAttribute("tabindex", -1);
}

function menuNext(el) {
  if (el.next) {
    menuDeactivate(el);
    menuActivate(el.next);
  }
}

function menuPrev(el) {
  if (el.prev) {
    menuDeactivate(el);
    menuActivate(el.prev);
  }
}

export default class SnippetMenu extends AbstractSnippet {
  constructor(snippet) {
    super(snippet);

    this.body = document.body;
    this.navigation = this.snippet.querySelector("nav");
    this.menu = this.navigation.querySelector(".menu");
    this.menuToggle = document.querySelector(".mobile-menu-toggle");
    this.mobileMenuAnimationTime = 400;
    this.menuFocused = false;

    this.initMobileMenu();
    this.initMobileSubMenu();
    this.initKeyboardMenu();
  }

  /**
   * Initialise the mobile menu
   */
  initMobileMenu() {
    let listener;
    this.menuToggle.addEventListener("click", e => {
      e.preventDefault();
      console.log(e, this);

      this.menuToggle.classList.toggle("active");
      this.body.classList.toggle("menu-active");
      this.navigation.classList.toggle("is-opened");
      this.navigation.classList.add("animating");

      clearTimeout(this.animationTimer);

      this.animationTimer = setTimeout(() => {
        this.navigation.classList.remove("animating");
      }, this.mobileMenuAnimationTime);
    });

    if (module.hot) {
      this.listeners.push({
        type: "click",
        element: this.menuToggle,
        listener
      });
    }
  }

  /**
   * Initialise the mobile sub menu
   */
  initMobileSubMenu() {
    const subMenuToggles = this.snippet.querySelectorAll(".sub-menu-toggle");

    for (let i = subMenuToggles.length - 1; i >= 0; i -= 1) {
      const subMenuToggle = subMenuToggles[i];
      let listener;
      subMenuToggle.addEventListener(
        "click",
        (listener = () => {
          subMenuToggle.parentNode.classList.toggle("open");
        })
      );
      this.listeners.push({
        type: "click",
        element: subMenuToggle,
        listener
      });
    }
  }

  closeMenu() {
    for (let i = this.toggles.length - 1; i >= 0; i -= 1) {
      const toggle = this.toggles[i];

      toggle.setAttribute("aria-expanded", "false");
    }
  }

  setupFocusListeners(element) {
    let focusListener;
    let blurListener;
    element.addEventListener(
      "focus",
      (focusListener = e => {
        if (e.target === element) {
          this.menuFocused = true;
        }
      })
    );

    element.addEventListener(
      "blur",
      (blurListener = () => {
        this.menuFocused = false;
        setTimeout(() => {
          if (this.menuFocused) {
            return;
          }
          requestAnimationFrame(() => {
            this.closeMenu();
          });
        }, 30);
      })
    );

    if (module.hot) {
      this.listeners.push(
        {
          type: "focus",
          element,
          listener: focusListener
        },
        {
          type: "blur",
          element,
          listener: blurListener
        }
      );
    }
  }

  initKeyboardMenu() {
    this.toggles = this.snippet.querySelectorAll(".menu-main .depth-0 > a");
    this.snippet
      .querySelector(".menu-main")
      .setAttribute(
        "aria-label",
        "Main Menu - use the arrow keys to navigate or the tab key to move on to the rest of the page"
      );

    for (let i = this.toggles.length - 1; i >= 0; i -= 1) {
      const toggle = this.toggles[i];
      this.setupFocusListeners(toggle);

      toggle.setAttribute("tabindex", i === 0 ? 0 : -1);
      toggle.subMenu = toggle.parentNode.querySelector(".sub-menu") || false;

      toggle.next = this.toggles[i + 1] || null;
      toggle.prev = this.toggles[i - 1] || null;

      if (toggle.subMenu) {
        toggle.setAttribute("aria-haspopup", "true");
        toggle.setAttribute("aria-expanded", "false");
        this.initKeyboardSubmenu(toggle.subMenu, toggle);
      }

      let kdlistener;
      toggle.addEventListener(
        "keydown",
        (kdlistener = e => {
          if (e.target !== toggle) {
            return;
          }

          switch (e.keyCode) {
            case 40: // Arrow Down
              if (!toggle.subMenu) {
                return;
              }
              e.preventDefault();
              toggle.setAttribute("aria-expanded", "true");
              setTimeout(
                () =>
                  requestAnimationFrame(() => {
                    toggle.subMenu.querySelector("a").focus();
                  }),
                50
              );
              break;
            case 39: // Arrow Right
              if (toggle.next) {
                e.preventDefault();
                menuNext(toggle);
              }
              break;
            case 37: // Arrow Left
              if (toggle.prev) {
                e.preventDefault();
                menuPrev(toggle);
              }
              break;
          }
        })
      );

      if (module.hot) {
        this.listeners.push({
          type: "keydown",
          element: toggle,
          listener: kdlistener
        });
      }
    }
  }

  initKeyboardSubmenu(menu, parent) {
    const links = menu.querySelectorAll("a");

    for (let i = links.length - 1; i >= 0; i -= 1) {
      const link = links[i];
      this.setupFocusListeners(link);

      link.setAttribute("tabindex", -1);

      link.next = links[i + 1] || null;
      link.prev = links[i - 1] || null;

      let kdlistener;
      link.addEventListener(
        "keydown",
        (kdlistener = e => {
          if (e.target !== link) {
            return;
          }

          switch (e.keyCode) {
            case 38: // Arrow up
              e.preventDefault();
              if (link.prev) {
                menuPrev(link);
              } else {
                menuDeactivate(link);
                menuActivate(parent);
                parent.setAttribute("aria-expanded", "false");
              }
              break;
            case 40: // Arrow down
              if (link.next) {
                e.preventDefault();
                menuNext(link);
              }
              break;
            case 39: // Arrow right
              if (parent.next) {
                e.preventDefault();
                menuDeactivate(link);
                menuNext(parent);
              }
              break;
            case 37: // Arrow left
              if (parent.prev) {
                e.preventDefault();
                menuDeactivate(link);
                menuPrev(parent);
              }
          }
        })
      );

      this.listeners.push({
        type: "keydown",
        element: link,
        listener: kdlistener
      });
    }
  }
}
