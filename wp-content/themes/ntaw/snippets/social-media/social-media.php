<?php
$platforms = function_exists('get_field') ? get_field('social_media', 'option') : null;
$availablePlatforms = [];

if (!empty($platforms) && is_array($platforms)) {
    foreach ($platforms as $platform => $url) {
        if (!empty($url)) {
            $availablePlatforms[] = [
                'platform' => $platform,
                'url' => $url,
            ];
        }
    }
}
?>

<?php if ($availablePlatforms) : ?>
    <div class="component-social-media" data-autoload-snippet="social-media">
        <?php foreach ($availablePlatforms as $platform) : ?>
            <a href="<?php echo esc_url($platform['url']); ?>" target="_blank" rel="nofollow noopener" class="<?php echo esc_attr(sprintf('social-%s', $platform['platform'])); ?>">
                <span class="show-for-sr"><?php echo esc_html($platform['platform']); ?></span>
                <?php \LambAgency\Components\Icon::renderSVG($platform['platform']); ?>
            </a>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
