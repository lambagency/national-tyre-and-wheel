import AbstractSnippet from "class/AbstractSnippet";
import { throttle } from "plugins/throttle";

export default class SnippetHeader extends AbstractSnippet {
  constructor(snippet) {
    super(snippet);

    this.snippet = this.snippet;

    this.body = document.querySelector("body");
    this.header = this.snippet;
    this.minBaseScroll = 0;
    this.minScroll = 50;
    this.scrollTop = 0;

    this.initStickyHeader();
  }

  /**
   * Initialise the sticky header scroll behaviour
   */
  initStickyHeader() {
    let listener;
    window.addEventListener(
      "scroll",
      (listener = throttle(() => {
        const scrollTopNew = window.pageYOffset;
        if (scrollTopNew > this.minBaseScroll + this.minScroll) {
          this.header.classList.add("compact");
          this.body.classList.add("compact-header");
        } else {
          this.header.classList.remove("compact");
          this.body.classList.remove("compact-header");
        }

        if (scrollTopNew > this.scrollTop) {
          if (scrollTopNew > this.minScroll) {
            this.header.classList.add("compact-alt");
            this.body.classList.add("compact-alt-header");
          }
        } else if (scrollTopNew < this.scrollTop) {
          this.header.classList.remove("compact-alt");
          this.body.classList.remove("compact-alt-header");
        }

        this.scrollTop = scrollTopNew;
      }, 20))
    );

    if (module.hot) {
      this.listeners.push({
        type: "scroll",
        element: window,
        listener
      });
    }
  }

  /**
   * Get the header height in pixels
   *
   * @returns {number}
   */
  getHeaderHeight() {
    let headerHeight = this.header.offsetHeight;

    // Add admin bar height offset
    headerHeight += this.body.classList.contains("admin-bar")
      ? document.querySelector("#wpadminbar").offsetHeight
      : 0;

    return headerHeight;
  }

  /**
   * Check if fixed header
   *
   * @returns bool
   */
  isFixedHeader() {
    return this.body.classList.contains("header-fixed");
  }

  /**
   * Get the header offset height
   *
   * @returns {number}
   */
  getHeaderOffsetHeight() {
    let offsetHeight = 0;

    if (this.isFixedHeader()) {
      offsetHeight = this.getHeaderHeight();
    } else {
      offsetHeight = this.body.classList.contains("admin-bar")
        ? document.querySelector("#wpadminbar").offsetHeight
        : 0;
    }

    return offsetHeight;
  }
}
