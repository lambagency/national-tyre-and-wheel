<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@400;500;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php wp_head(); ?>

    <?php \LambAgency\Components\CodeSnippet::getInstance()
        ->renderSnippets('header'); ?>

</head>

<body <?php body_class(['header-fixed', 'no-js']); ?>>

<?php \LambAgency\Components\CodeSnippet::getInstance()
    ->renderSnippets('body'); ?>

  <header data-autoload-snippet="header">

      <div class="header-top">
        <div class="grid-container">
          <div class="grid-x align-middle align-justify header-top-grid">

            <?php if (function_exists('get_field') && $headerLogo = get_field('header_logo', 'option')) : ?>
                <div class="header-logo">
                    <a href="<?php echo esc_url(home_url()); ?>" title="<?php bloginfo('name'); ?>">
                        <div class="logo"
                             style="background-image: url(<?php echo esc_url($headerLogo['sizes']['square-small']); ?>)"></div>
                    </a>
                </div>
            <?php else: ?>
              <div class="header-logo header-logo-no-logo">
                <a href="<?php echo esc_url(home_url()); ?>"><?php esc_html_e('LOGO'); ?></a>
              </div>
            <?php endif; ?>

            <div class="header-top-content grid-x align-middle">

              <div class="hide-for-medium-only hide-for-small-only">
                <?php LambAgency\Framework\ModuleFramework::getSnippet('menu', 'top'); ?>
              </div>

              <?php
                $buttons = function_exists('get_field') ? get_field('header_buttons', 'option') : null;
                if (!empty($buttons) && count($buttons) >= 1) : ?>
                  <div class="header-top-button-wrapper grid-x">
                    <?php
                      foreach ($buttons as $buttonFields) {
                          $button = new \LambAgency\Components\Button($buttonFields);
                          $button->renderButton();
                      }
                    ?>
                  </div>
                <?php endif;?>

                <div class="hide-for-large">
                  <button
                    class="mobile-menu-toggle"
                    type="button"
                    aria-label="<?php echo esc_attr(__('Open Mobile Menu')); ?>"
                    aria-controls="header-menu-wrapper"
                  >
                    <span></span>
                  </button>
                </div>

            </div>

          </div>
        </div>
      </div>

      <div class="header-bottom">
          <div class="grid-container">
              <div class="grid-x align-center text-center">
                  <?php LambAgency\Framework\ModuleFramework::getSnippet('menu'); ?>
              </div>
          </div>
      </div>

  </header>
  <main>
