</main>
<footer id="footer" class="mod-footer" data-autoload-snippet="footer">

    <div class="footer-top grid-container">
        <div class="cell grid-x grid-margin-y align-middle">
            <div class="footer-nav-logo grid-margin-y large-6 medium-6 small-12 ">
                <svg width="110" height="73" viewBox="0 0 110 73" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <rect width="110" height="73" fill="url(#pattern0)"/>
                    <defs>
                    <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                    <use xlink:href="#image0" transform="scale(0.00909091 0.0136986)"/>
                    </pattern>
                    <image id="image0" width="110" height="73" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG4AAABJCAYAAADc8hN1AAAgAElEQVR4Ae09B1icx5XKXc6Xc3yXXOw0O/XOycW5S+I4PY4TYtlyTrZkWRKWLMkyp4JlhOgg2qKlsx220GGpAi1lgaV3ECAQnRUdwdKraCu68Nz3fpjRv8siEEXl+/J/3zL/P/PmzZv3prx5b2bYs+cpe/z8/L6Rk5N5JCcz0xhINzIyenVqYqKktbm5pLPzdlV/X18zQmi+6/btm5AeLpUyEELqtpbmsuqbN+PLSkrE+bm5ToWFeZfML1x46Smr/tNDroDFeuV2e7tz061bCYMDAx1LS0ufo9UnVaHwVdbXV+Bvejg/N3dP1dnZvri4sEiPp7+nKhQu/v7+PxoeHGxta26OS01O/iQ6OPj7Tw93niBKAwICvtJ065ZroL//JX19/eeGBgaG6MzeyfelpaVF+GnjHBwcbK+tqQnKTEt7f8+ePV94gtjz5JGSmJj4l0alMhEhRPUotVo9Mzw0pNJm6qP+9hUK3wVuxcTEvPHkce3xUfSF6spK+7tq9a71qu0KerC/t2p6eppqQIMDA41JCQlHHx+7HnPJsRERrwAJEh8f0+0y9nHknxgfH8nJyjJPT0//58fMykdTPJPJfOZGaakEmN3Z0XGj83Z77eNg/A6WOXPBwOAHj4Z7j6mUtJSUj9Vq9dwOMu2JQJWVlWWYn59/Qh4f/+FjYu3uFMtydX0FIaR8Iri8y0TUVlUl8fn8f9kdTj5arF/o7Gjv3mV+PVHo5+fm5qOjw2EZ8fQ9oYGBf6ytro5Kio93mJ+bW36iOPuIiJm4c0fGZDL/4amRXnx8/P6lpaVHxJ4nuxhFUlLUUyG4zPR0myeblY+WuuamxpyosKjXw8LC/vzECnBwYCDs0bLl6SqtqKDA8YkTXklRUcrjYuPY6EhPR2trfm1VVXjp9WJWQlyc/fXCQouWxkaT4uJi88T4eNuS4mKPyoqK4EalMhPskItLS/ceB73K+nqfJ0Z4XC73hcnx8UeyPltcXLrb1NiYmpuVZSeXy/fKZLJvbYURAoHgq5GRkX/Mz881aayvjx0fGxt5VIJsVCojtkLzjuXR09N7TllXF1pVUXF1empy1wSnnp6ebm9tDc7Lzt4XEBDwTztWAS1E0eHhvy0uLGTP3L3bt9tC7Oxoz9Qq/tF93igpSdvNCna0t1fmZWfrI4TWuFUMDQ2fNTEx2WtlY2V9xYWZ5uzmkn3hwoVNmZ5ARXdiMsV2jg6Bly5d+oUujqWkpPyltaVlV+vnwmA8em9DSIC/eLeE1t/fW5QcH/9LbYYaGBh8y8TcxNTV3bWQw+MuCHy8kbfQh/qJfCWIy+cje3v7U9r5dH1fvHjxbaFYhMS+vsiLzZpgOjvHG5sa6xsYGHyJDh8iEr14o6wseqfruri4gOQJCWblxcU/p5e3q+8iH59dUflHhoeVEaGhv9Mm3tTCVJ/p4lLM9xYgYLbAxwfBO5fPQ1zeyo/D4yIQJAjQxc0lRVsA2jjh28zM7NveQp9Bib/fSl6JGHF53HlbR/tIw4uGr9HzhIaGfrepoSFrpwUI+NheXk70snbl3cbS8vBOE69WqxeLiorO0gnW19d/xtLa2obN5Y6LJGLkLRSuCGtVUFhga0I+jxIeT8CfuXz58v/Scep6f++9917iewtGJH5+iM1dET40Dh+RCDFdXOqMTU3foefLUGTo3Rkb3XHfoYWp6TF6OTv+3tHWVrOTguvr7S0MCQn5VzqhFlYWJhwed07sK6F6whrhbCA8eu9jMpnhdNy63t95553/lvj53YPeCsKD/NCbQXjQaNw8PBqNjIx+Q89bXVUVuJN8QAgtHzt2jPJR0svZsfeaqqobO0VwVkaGHZ0wfX39l4QS8ZjYz3dzvesBAsTMF0kkiMXhDNja2mownl4uvOsf1//ANyCAmi+pvDzeGgHaMxgyKyurL+O8GYqMD3aKF4Cno63tlr6+/j9i/DsSRoaHcwD5+J07EztA7FLCtYS/ahMmk8n+0dbWxgvmMB+xiMxfD9vjtOG9RSJqbmRcucLRLpP+feHCBQYIjz53Ai4QJI/PXx2CBQvmVlYf4Hyy8PDvLS8v75j3IzkpKRHj3nZoaGj4px0QFoXiztjYYExMDFHbP7346dvOLs4pFy0vki1xpqam3/Tw8qqA3sITCFZa/gN6l7agtL8pxgsEFONZHFa7paXlT9ZjCoN5JUPi708Nmdp44BsUINBEnZjMMIwDlhc93d3FO8Ujo/PnX8W4txVW3LhRvkNEtUul0q9iYiysLQywSs4T8JesrKw0fFiOTo6XIR16jC4mbiXOWyRE8LN3tL+M6aCHoKz4iMXTMLfhIZNeDhXH5yGJny9y83BXnjhx4t9x/pGhoW2v+7pVXWNgicI4txy6u7pe2gmhDfT13QYzEybE0tqaC5octGBQCGA9BkJydXO9St/HaGln9xNPllc7KA471ftgKQG92YPFqjSyNlpjMjtz7sxpPx1DJl2A8A49z1soHP/oo49ID66tqdmW8JaXV1yXKUmJW7dpHj58+DtLS0uz2xXc6MjI0MGDB7+JhcbzFsig0nRB4JYMDGXzuOPW1tZ/wfAQMplMNmh58NNm4Fa/YR4FGuwZ9ufoZcE7g8FI9A3wf2BZHC6XGn5FvpJ7R44fJ+vP1paWHRk2Lc3MNlzOaNNNfSclJMRsV2gIoXsGJ0/+DBdga2eX4Re4ogDoGopACLj3OTs7++J8ENo42PyaxWEP7nzvEyMPD48cGxsbsizZt2/flzlcTimUBT10PVphtIBGKPb1XX733Xd/BXTq6el9qbOjo227vGtpaqqn13/T712dt1u3W7jRp5+SzaNMF5cEv8DAdSd+3HOASdATgGksNrvn8uXLGk5IV1fXcGruE/o8sEdgfBuGsGiXSEA4C05OTgfoDHJ0crLGDWk9PCA8GPbFvr5qvb/pUYqXvr7+T6HRbpd/MFXR6dnUe21VVdx2CvaXSIj6bW5uHugfGEip2uu1XjpjeAI+ZS2BNZ3IV7J04cIFDdultbX1fg6PNwPC1Vbf6Xg2+w40wXwLDcLNw0NjnrW2tv5PT5ZX04N6HwgPhlY3T49GvBZzsLU5uR3+Qd7hwYGRt9566yubEthlS8s/TU9NTc7Nzs5steCy4pIyXNiZ82csAoKCNOY0nQzlr6jbMPfA8OTh6VFlb29/xtjG+EXMDIwTQrBHurm7p4AGCEzfTIPQWe7qUoPKf7/3rZlnHRgMZzC9rTfPgvBgGnBgMGSYzpirV0O2ykOcTyGXh2B8DwwVSUnhONNWw1OnTlHukuPHj+uBhgYVXo+xsLjFigebyxllMBgsI3Ojlx9IJC3xsv3l4zyB4HMQ4E70PhAuDI8wfDoxnTTm2UuWl37K4rA7IY2uXOEGAXUB4RleMPwUSDxw4MCzw4ODvVvlI+SLl8WKaNXV/WppaPjC9NSkejsFCTgcD8AOC1MWh9MOkze0Rlw5HK6o5Su9xdnVtcjGxuaQbqo2jrW0tHzBk+W1qkzsTO+j5lkwmXG5XVZWVv9Dp8LZ2VlIrQm15llonCBUb6HPzL73930X8liYmoJPccuPIjnZgl62zndfsdhsyyUghHp7ujvxXkILKwsBtD5tocGwRs1NPN4c4wpDZGlpSawmOol6iEg7BzuTjZQJ3HA2G64ISIgcnRyv0EmxsrP6A4fLGQZBcQV8MqJQQ2ZAALK2to7G8Fmpqdta3zXfutWIcekM62pqdJ7u3KwwPd3dDQCx/okTvwXhABMxgyiBScRg9B22c7AzgwMgOonYZqSZmdkPPFleDVC+ruEM07PZEHrRSu8TIxaXfYtungNSmc7MYKzl4ukARhPw8x0/dfyPAGN0/vxvNsvD9eD4HM5xnaxh2DF+t16mzcQ3KZVk3eHAYKQC4WwuB/G9V3oYi8Mec3Jy+j+dhe9CpKOTowvMe9BjNiukjeBAQNAAHR0dNdR0KyurN7k83jTV+/g8BItzqP9le9v7ikpUdMRm+LgeTFZ6eoxONkWEhbHWy7SZeG8Oh3KGnjp16g3smqEqwuMtMxgMW52F7nKkhYXFL1lsdj81NPPvD2cbCWi9dOhR1NzsK4FFe4GZmRkx4xkaGv6Ti4tLPNQZhAsNFpSyU6dOUYqWubHxrzfDx/VgRoaHRuluJcI6jpfXh+tl2ii+u7OzEyOytLG6BnMbtHQXV9drdIsEhnnUIdOZ6as9nK0nnM3Gg4B4Av49WwdbDe+1o6PjIS6fNw/lwdrO2MSEieubm5OTuhEvH5TOY7P3Y1wkDBCLX5uempx/UMb10gJ9fal9EydPnvwpWBG8hT6LFhYWa3xupLDH8AL2Tw5shVhdtOP5aLOC0oaD/FjRcnV3T7506RI5nQo9w83NLQ0E5+ruXoWra2tltS3na25WluaaTigQbGuVf/H8+R8DcdY2Nled3Vx7YNjAxD5JIdDl7u6eAL0BFA5tYTzsNyX81UU7WHusrKw03EWu7q6m0FAMDe9vPurq7Ly9XgfYKD4nIyNAg5+pimTuRpnWS29vbc0HZHp6el80OHv2gFAoJC1Po5An6MPBweEk39s7l8vnx3G53Gvb+vF4sWweN8pb6FPlLRTecnNz+xO9qobGhj8xMTH5EY4LDQnaMq/FYjHVQTCuPRXl5ZnrCWaj+NjoaOqGH+xHQwg9gxD6N/hpCxHWeAihf0UIPQfpUqlUYx8jIWgLLwIB+zUem23A53BOwFyA15NbQLWtLHi3NXgJXn755X8DvsA7psfVlam3EU8fkD71ySef/DchsKO9fcuegM/OnPkvjAgh9OfVQik7Z0N9LbFZAsyRgwd/DjuaEEILABcfG0sWqRjHRiHHy+vdiLAwh2B/fzuhgHsYwyOE6HbBYWhAOO1xhHExMXyoo6qrC7W3tAB/vwh0gAB7VF1b3qeSEBfHourDtLL6xl21ektKSWtzM1m7ATKE0JvarUUkEBCLw8GDB39JT4+XyZIelql9fX3pNBykYUxPToZMT02i1R/sfXysglMkJ/vS6BzAgoP6piqSt+zrvFFWlkLxjO3h8TqtgId6zUhP15gsdQkOEJ7+6CNqq/VDCG7NeQEs4IG+Prr5qBTHSySS565evfrNiIiIbwQHB38Nxz+CUCetCrmcLrh+uuAEXK7hQzGaBtzW2rrSWcKl0jO0+Id6DQ8N1XD7rye42urq68DA/Xv3/pheQBytx5WUFH3Y292dBftT+np6xgYHBgaGBgdu9ag6uZj58viE4Pn5+SmMY35ubjQ7M7PI8MyZv+bm5gJut7GREYfJyclPEULU+Ws2m/2tihs3+L0qVU1/X19bX2+vsr6mJpp+PsHN2Rm06qypqel0Hz7/BKw9h4eG5HOzs/3j4+PtnR0dLpgGCGtu3tzb3tqaNDAw0Nbf1zc6ODAw2Nfb3dLS3Bzq5uBA3ciXkZpK3eeySquG4Bi2jF/hOjxsODI8NMZkMp/d48JgnH7YzBjezc3tt/QKrSc4gLcyN/8AtgTgvBCGh4ZSJqH0tLRQerz2e3FhYdrF06efn5yYuKOdBt+gkORmZZnQ0oaBLmsTkx/hu8FoaeRVLBR8BnDefJ4Pjmxrbq5RdXXdwt84zM7IkAKs0NvbHsfpCvt7e0cBLiY6mklL1xAcNIw7Y6Nb3qMq5vFWtMvSktJYWiGbeoUzcXZ2dl8HIvGjLbi52dnl+YUF6lI12AphfOHCkW6ViriNxD4+lL8LIQS32KG52VmUlJjoBfhSk5NP0QlJuHbtrZ6env9BCJGJvermzcbOjo7TcDVTfXU1mwbfxOFwvpyZmkpuLurv6+tLlcvNlPX1JTQ4BD4zpoODFT1ucXFhdnJiond5eZlcsTg9NTnl7e39vfaW1pX8n3+O0lJSKOVKKpW+MTpy/2ykp4vLb4ICAi7ScGoIDurXqFRu2aAv8fH56x5fX+Hv7969+9CbW253tLdigeFQW3D1dTUJ1ZWVObgCHa2tXWOjo9P4OzQoKA7yMpnML4K2xXZ3f41paUntK8xKT/8Mw0FofenSPoBFCJXieKG3dy6t7GAcPzs7m5+ZmXkCFBX8sDw8KA2Uw+H8kN4L+RzOWRcm0wjDQRjo5/ce4M3LyaHPU8jOwoKYnARswatMJvM7ABcaGPje5MT9DiQNCno7Qiq9QMO5RnBFhfnXaOmbflXW10uoOpeXlW3JflZXU0OYRmOehlap6ux0EbBYr0xPE1lpEBjgK5FD3iip9GfK+trwHpWqemZ2VuddG2+/+eYawcXLZIQGhBARXH9PT+b1oiI3XNj83Byi76Cuq6khQ2F2ZiYvLDiYHB2bnJiYBEUH6Dp98iONXv/OO29Sx696VCpJt0pVtt72jquRkW/HREU8UHCpKSnUcgHTuNnwZnm5K8Xv5sbG/M1mosOVFhevucNDu8fBNAaFcL286GssgsaHx+OcPHnyZyQCIRgul2sqKxNKrl/3XKIdsj966BAWXBmGD/Lz0y24/v7MspISZwwHoY+PD7FeNDc1knVrckICK0Astsawc7OzY2VlZdS1Tkc/+ICYAu+q1Z+7ubgYd3a0k9FpfmEBNTc1FaUkJTkNDgyMYhyRkZFvR0U8WHBxV6/aYfiHDSmlUKXq2tIW8+tFRWt22+oQHOyYAnPYc/19fWvOlYUEBHBDg4NJzwDmXLly5XuQh+Xu/ld6hY4cOUKdVUMIXcfxjQ0N/YlxcdSwNjAw4I3jl5aWihRJSQfo846vUEzZEdOSk7GRgAJ3tLM7zHBwIL1jZnb2jkKheBZoOHHsGBHc8PDQnZrKSo35MTw09C2Ac3R0/E/6sLwZwYUGBW15SVBcWMjY09fT04Er/DChNCTEHYimP+sJDmAunD9/XBu/NCiIHRYa6ojjQUFJlsvtUhISzMbHRjWMAkfef5+aX1qaGpMwPA6vXb16SKlU0rXKDigzPDRUgWHmZmfv9XZ319xVq4lmOjkxMQZwrk5OcOE29awvuOHx64WF2RgOwhtlZSJQomC3Nj0+NDT0rY3muLQU+Uf0PA/zniyXu+5Rq9Wwqn/oJ02RTCwiWHgIob1aiDS8tsmJiRoTssTHRwKnTxFCd7XyoerKysIelYrcgMDy8DCBcrgs1lFtWIVcfqilqYl+aekQpmlkZOSmNvzqd7+NmRl1mFDk7S3CMHfV6nHc444dPUp6HKQbffrpAViaYFgc3u5oL+3v62vC30EBAZ+FBAXR9+8MIoQ0PCYRUumWT/e2NDWx96jHxuBawlcRQqBqb/YH3txvYObgEOIQQp8ghKB3nUMI/R6nQaivr/8tenpYcPDrEC8UCr9Ter2ImZGmCLpeUMCVRUdT68OZmZn3EEIfI4QMaysr9TCuIInklYS4uHMMW1tDYJK3t/s3R0ZGYM12Hgw1CKGDeAEOeeLiYvYq5HKWPDExLO7aNWF6uuIonMfD+NyZzNdW83587969Qzjt8P7930cIgYECescn1NKByfzi6OioZWpycmDp9es+iTLZ3wBPfnb2XxBC/we/1sbGN+Bg/mpeoOeA9u0RTCYTDPFQLszxm+U7wP3a3d3925j2v4dPGwdys7IOgWEDWvUGP9ugoCCNHvS46pqTk/O9TIXCplGp9CsvKxPLExIMWSwWObShTdfU+Ph+hJAtQghMYUYednbPAwy4WybvTJ5FCMH8aGN07hw5h50UHw+9BLRNUFxOnzUwOHD61CljwzNnzAzPnv2koKCAsvZrl7Vb34cPHPj98Q8/tD929LDZmU8+Ob2nuLCQqNd4jF4vVMjlxG64WwRuhFeekGCwDn0zZ0+f1jhjgHGFh4VRR6FxPgGXSx1hEolEGht4YqOiPHGevu7uFgxfUlxcEiGVkgP7k+PjRPPE8LsdIoRIHe6Mjg4/lOAkIhE5yLHbhOrCb2Vs/MOlpXvEDFV5oyK4orSUWDdam5ubdeVjubu/jYUAISgOABcaFARzEnngMjmI9/Dw+Pr83BwxBIQGBbnRD3lOT02OyWSyR3qFL1yJggnFgoP9fqAig/ZFWhkAzczOLszPzoK9rwr+X02aQkG1SPBi40UqVBTMVaue7Wenp6dfqKioeB4h9C/g5cYVjI6O/j4MP1hjg3yy4OCvxcXG7osKD/8gNjZWY5s3pGs/IQEB+zDxqtu323F6Q10t0Rzd3ZlrzlDb2dk9PzkxTjTX9NTUQMibkZamYb0YHhrqhngWi/VbXA6EFiYm+y3NzUHpoh4QHMDJZLJnkhMTD8fFxOhhzzemCYcpMtkPE2Syg1Hh4e/BRTc4XlcYGRr6WlxsLODbKxQKwXtOnjWCIykrdkDQWsiTlZFBrYcwTLxMFoAT21tbye4lNptNGAruk+vFxTx8PiwqMjLS9OJF0A6px9fTkzo3fUupDMJxOJycGO9Nio/XuBQGlw3h+dOnf4xvn52fn0cSH5+/BQYG/lGtJnZrZHT2rM4DI5Xl5QW4nJqqygLAV1NVqbEug3SI9xWJNPb7w7+KsbtsTWyn/T09t3Kzsy3n5+fpl8+NWtDuBnN1dPzhwtzcmutFpqemrkMavV4VZWXvjQwP3bdSQy9ZWEC3GhrIgY+HElzVzZsagnOwtj6GKw8h096ecpAmy+VkA0x2VlZwbHQ0cRXBZdrdKhU+sTINHoVrV68SD/DgwMBkbU0NGAHIEMiwt9e93RrsmpobdyEPyRceFkZZauhMwe/xsbFkqOlsb29mMplf7VZ1USPM4uICOYRob239WkRYmCWtnr2Aw9HW1gHHLS0tLcMPf+Owv7eX2uN/QE/vBTWtNbU0NQ00KpU9GA7WrccPHaIOQTLs7QmvIL2hrq6zp7ubGAlkMTHZUD5C6ArOTw2VuGKriRo9rqmxUUNwYMXv7u4iBAT6+xtBvqZbShhKqUfs47P3akSEhrEZp0HFFMn3d5T1qLr7zc3NqblCKpWC5kc93aqufl33cWWnpf16dHiQ2BkxPIRFBQVU6+Tz+V8rLi7+dnV5+c/PnTtHzp4L+XzQEqlnaHBgKCIkxGB6aooy6VeUlWX1qFRdkBjo72+hSEkho0FleTllCHdhMj1Xs1NBfW1tSUZamsng4CBY/slz8eLF5+OuXSOKBNhdMY8L8vIIjqsREdRwPTI8RIwMKUlJ1PGsFZ7eIm6fsLCw19uam4lStqHglA0NGoIDhClJScTpmJqsCPz444/BOUq1emhkAJORmqphFehRqTqys7P3hfv5vVRZXlGEa9nR1taWmZZm2tDQcDk7M9N9emqS9J7Y6GiyEQhwwv0fOB+E8O/F6P9iLFwqtXBhMP6AYSYnJtRw4x7khQfcRZMTE9Qmprtq9VxtVQ0Mk1SvCfAVn7tZUZEHeZV1daUd7e3EexAeEkIdc3JjMokwAM7Dw4NyZoYGB5vjMiFMTU4+2NLURJheU3UzszAv77OykhLj0uJicn6graW1LjosTMNeWZiXJ6ypqrIEl5ayvp4Ms1WVlUJlXR0ZBbYkuCsODuTCGvjfbdGRkcQjrEhJTgcm1VVXw3qJPBxPT3Ibz8jwIP0+MCIoArz6IouNdVhh+crfsuJicufzyPDQjIODw3e9eTwNtwu+bgJQeLq6amxOBSy11dXElzc/P0+GR7tLl76emZ5KqfurwyChC67ih7xMJycyHczcvTuTmppKzdUebm5g2SFPblaWYWtzUzuJoA3ltDjY1DQC1xHT43AH0IpDyvp6RV1tLTnbsSXBQSUaamqqAfnc7Ozc+NjIGC7Im8ejurqyrk7jfpQT+vpE0+toa8vF8CD4goIC2GP5gwy5/AciPv9nYM7Ky8t702zVjrgitj176mtryXozLzv7Go5PT00lDMV4FXK5N06nh/Fx94cwDNvf19cPMFEREWvcLMNDQ0P4OJgrk0nKmZ+bI4LzcnfXmKMKcnOPNiqVFH+gjPIbNygvyvz8/MupCQn/IREIXpVIJK+EhIS8mJ+bSwzj4DPMUCg+aGxsfKatre0/4B8S+vv7/0Iikbwqk8m+cletJt6PLQsO9jTiiuNwZnZ20djY+EVggrKuRkNwb7zxBrlQM9jfn2wT6O/rW4gIDqaWAT483v6o8PDk+Lg4RXJ8fLqBgQE5CQM4K27cIBd2zy8sTEZHRprKoqM/6+vpWfPvX1RdXaF0geF3kUh0BNOLw/KyshxID/TzW+O9KCosTMN52Z6eGoLLzMykdpKxtQSXnJz8X3ExMWRY7WhvJ9p3QlysWXRYmDwrPf2Gh4sLdSv6KE2bzM/L40N5TCbzSxFhUnFkWFhMkjxRCTsD2lpaiOtJl+A0nJq65jhAbG5u/jK0EPqTl52djCtZU1VFWhLAHDx4kAgOYEpLSohPDdTeocEBhNV8gJfHx6/RDh0uX36fXp72++zs7PTS0hK10RbSSq8XV+Ddw5gu2GoA7h16XrzHhcGw1djzCTAhQUHWOC+bxdqU4JRKJWU261Z1kXXj5Pg4Gh7S8PzABaPURiUPDzeyPoQyAW58jAxiqKWpaRloWFxcJN4PXYKDLW7ggoBSZpUNDYWYcO2wu6tL4wYd2P6NYSoqKjAxsKN46MiBA+TaJAyj6lRBq9TwuY0MD/cWFxSsuxQozMv7WPtfY8Jw3dzcSA1HAb6+7qC34A1F3V1da5y9t+rrYUMtwMA/81uWBgdTZ89hy9vo8DAYG2D7H2h6kwmxseS6RK6XF57Lx+fn5prkcjk1IsBeltW5CbRLUH7+AHW0MTZ+caCvb822kP7e3vLS0lIydQBsekrKm700E9tqw1puqKuVMo2MqG0Uq7ZWSJq+Mzpaifm46fD8mTNHivPzQTsirvqhwYExmzNn1jXyroccLA+xsbG/qLl5888ikUhjUbpeHphzcnLSf9+nUr0bFhT0KzwHacODGwUh9B3t8wvacLv9HeDl9ZXr+fm/K87Pf10oFGrsitMu28/P76XG+vo38rOyfhUQEEB54bVhtvwdIZVGrbYIEkSFhbltGeHfMz4aDoQEBEKB/m8AAAAdSURBVAhAYnfVatj7NliQl0ftg3w0pf+9FMyB/wfioFQILwwnAAAAAABJRU5ErkJggg=="/>
                    </defs>
                </svg>
                <div class="footer-nav-logo-content">
                    <h4>Contact Us</h4>
                    <a href="tel:<?php echo wp_kses_post(do_shortcode(get_field('footer_contact_no', 'options'))); ?>">
                        <h2><?php echo wp_kses_post(do_shortcode(get_field('footer_contact_no', 'options'))); ?></h2>
                    </a>
                </div>
            </div>

            <div class="footer-nav-main large-6 medium-6 small-12">
                <?php
                if (has_nav_menu('footer-main')) :
                    wp_nav_menu(array(
                        'theme_location' => 'footer-main'
                    ));
                endif;
                ?>
            </div>
        </div>

        <div class="footer-nav-legals hide-for-large hide-for-medium">
            <?php
            if (has_nav_menu('footer-legals')) :
                wp_nav_menu(array(
                    'theme_location' => 'footer-legals'
                ));
            endif;
            ?>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="grid-container">
            <div class="footer-bottom-content grid-x align-middle">
                <div class="cell medium-auto">
                    <?php LambAgency\Framework\ModuleFramework::getSnippet('social-media'); ?>
                </div>

                <div class="cell medium-shrink text-center medium-text-left  hide-for-small-only">
                    <div class="footer-bottom-legals">
                        <div class="footer-nav-legals">
                            <?php
                            if (has_nav_menu('footer-legals')) :
                                wp_nav_menu(array(
                                    'theme_location' => 'footer-legals'
                                ));
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-bottom-rights grid-x align-middle">
                <div class="cell medium-auto text-center medium-text-left">
                    <p class="small-label">© <?php echo date('Y'); ?> All rights reserved.</p>  
                </div>

                <div class="cell medium-shrink text-center">
                    <a href="https://www.lambagency.com.au/"><p class="small-label">Made by Lamb Agency</p></a>  
                </div>
            </div>
        </div>
    </div>

</footer>

<?php wp_footer(); ?>

<?php \LambAgency\Components\CodeSnippet::getInstance()->renderSnippets('footer'); ?>

</body>
</html>
