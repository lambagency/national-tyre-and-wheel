<?php

namespace LambAgency\PostType;

class Post extends PostType
{

    public static $postType = 'post';
    public static $singularName = 'Post';
    public static $pluralName = 'Posts';

    private $content;
    private $date;
    private $categories;


    public function __construct($postID = 0)
    {
        parent::__construct($postID);

        $this->setContent();
        $this->setDate('j F Y');
    }

    /**
     * Get the content
     *
     * @param bool $excerpt Return excerpt instead of full text
     *
     * @return string
     */
    public function getContent($excerpt = false)
    {
        $content = $this->content;

        if ($excerpt) {
            $content = wp_trim_words($content, 50, null);
        }

        return $content;
    }


    /**
     * Set the content
     */
    public function setContent()
    {
        $this->content = !empty($this->fields['description']) ? $this->fields['description'] : $this->content;
    }


    /**
     * Get the date
     *
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }


    /**
     * Set the date
     *
     * @param string $format Optional format override
     */
    public function setDate($format = '')
    {
        $this->date = get_the_date($format, $this->getPostID());
    }

    /**
     * Get the categories
     *
     * @param bool $string
     *
     * @return array|string
     */
    public function getCategories($string = true)
    {
        $categories = [];

        if ($this->categories) {
            foreach ($this->categories as $category) {
                $categories[$category->term_id] = $category->name;
            }
        }

        return $string ? implode(' | ', $categories) : $categories;
    }


    /**
     * Set the categories
     */
    public function setCategories()
    {
        $term_list = wp_get_post_terms($this->postID, 'category');
        $this->categories = !empty($term_list) ? $term_list : $this->categories;
    }
}
