<?php

namespace LambAgency\PostType;

class TeamMemberNew extends PostType
{
    public static $postType = 'team-member';
    public static $singularName = 'Team Member';
    public static $pluralName = 'Team Members';

    public function __construct($postID = 0)
    {
        parent::__construct($postID);

        $this->setPosition();
        $this->setDescription();
        $this->setMobileNo();
        $this->setOfficeNo();
        $this->setEmail();
        $this->setTeams();
        $this->setLinkedIn();

        $this->setStreet();
        $this->setSuburb();
        $this->setCity();
        $this->setState();
        $this->setCountry();
        $this->setPostcode();

        $this->setPostalStreet();
        $this->setPostalSuburb();
        $this->setPostalCity();
        $this->setPostalState();
        $this->setPostalCountry();
        $this->setPostalPostcode();

        $this->setHeaderImage();
        $this->setFooterImage();

        $this->setHeaderColorScheme();
        $this->setFooterColorScheme();
    }


    /**
     * Get the position
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set the position
     */
    public function setPosition()
    {
        $this->position = !empty($this->fields['position']) ? $this->fields['position'] : $this->position;
    }


    /**
     * Set the description
     */
    public function setDescription()
    {
        $this->description = !empty($this->fields['description']) ? $this->fields['description'] : $this->description;
    }

    /**
     * Get the description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set the mobile_no
     */
    public function setMobileNo()
    {
        $this->mobile_no = !empty($this->fields['mobile_no']) ? $this->fields['mobile_no'] : $this->mobile_no;
    }

    /**
     * Get the mobile_no
     * @return string
     */
    public function getMobileNo()
    {
        return $this->mobile_no;
    }


    /**
     * Set the office_no
     */
    public function setOfficeNo()
    {
        $this->office_no = !empty($this->fields['office_no']) ? $this->fields['office_no'] : $this->office_no;
    }

    /**
     * Get the office_no
     * @return string
     */
    public function getOfficeNo()
    {
        return $this->office_no;
    }


    /**
     * Set the email
     */
    public function setEmail()
    {
        $this->email = !empty($this->fields['email']) ? $this->fields['email'] : $this->email;
    }


    /**
     * Get the email
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * Set the teams
     */
    public function setTeams()
    {
        $this->teams = !empty($this->fields['teams']) ? $this->fields['teams'] : $this->teams;
    }

    /**
     * Get the teams
     * @return string
     */
    public function getTeams()
    {
        return $this->teams;
    }


    /**
     * Set the linkedin
     */
    public function setLinkedIn()
    {
        $this->linkedin = !empty($this->fields['linkedin']) ? $this->fields['linkedin'] : $this->linkedin;
    }

    /**
     * Get the linkedin
     * @return string
     */
    public function getLinkedIn()
    {
        return $this->linkedin;
    }


    /**
     * Set the street
     */
    public function setStreet()
    {
        $this->street = !empty($this->fields['address_type']['street']) ? $this->fields['address_type']['street'] : $this->street;
    }

    /**
     * Get the street
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }


    /**
     * Set the suburb
     */
    public function setSuburb()
    {
        $this->suburb = !empty($this->fields['address_type']['suburb']) ? $this->fields['address_type']['suburb'] : $this->suburb;
    }

    /**
     * Get the suburb
     * @return string
     */
    public function getSuburb()
    {
        return $this->suburb;
    }


    /**
     * Set the city
     */
    public function setCity()
    {
        $this->city = !empty($this->fields['address_type']['city']) ? $this->fields['address_type']['city'] : $this->city;
    }

    /**
     * Get the city
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }


    /**
     * Set the state
     */
    public function setState()
    {
        $this->state = !empty($this->fields['address_type']['state']) ? $this->fields['address_type']['state'] : $this->state;
    }

    /**
     * Get the state
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }


    /**
     * Set the country
     */
    public function setCountry()
    {
        $this->country = !empty($this->fields['address_type']['country']) ? $this->fields['address_type']['country'] : $this->country;
    }

    /**
     * Get the country
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }


    /**
     * Set the postcode
     */
    public function setPostcode()
    {
        $this->postcode = !empty($this->fields['address_type']['postcode']) ? $this->fields['address_type']['postcode'] : $this->postcode;
    }

    /**
     * Get the postcode
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }


    /**
     * Set the postal_street
     */
    public function setPostalStreet()
    {
        $this->postal_street = !empty($this->fields['address_type']['postal_street']) ? $this->fields['address_type']['postal_street'] : $this->postal_street;
    }

    /**
     * Get the street
     * @return string
     */
    public function getPostalStreet()
    {
        return $this->postal_street;
    }


    /**
     * Set the postal_suburb
     */
    public function setPostalSuburb()
    {
        $this->postal_suburb = !empty($this->fields['address_type']['postal_suburb']) ? $this->fields['address_type']['postal_suburb'] : $this->postal_suburb;
    }

    /**
     * Get the postal_suburb
     * @return string
     */
    public function getPostalSuburb()
    {
        return $this->postal_suburb;
    }


    /**
     * Set the postal_city
     */
    public function setPostalCity()
    {
        $this->postal_city = !empty($this->fields['address_type']['postal_city']) ? $this->fields['address_type']['postal_city'] : $this->postal_city;
    }

    /**
     * Get the postal_city
     * @return string
     */
    public function getPostalCity()
    {
        return $this->postal_city;
    }


    /**
     * Set the postal_state
     */
    public function setPostalState()
    {
        $this->postal_state = !empty($this->fields['address_type']['postal_state']) ? $this->fields['address_type']['postal_state'] : $this->postal_state;
    }

    /**
     * Get the postal_state
     * @return string
     */
    public function getPostalState()
    {
        return $this->postal_state;
    }


    /**
     * Set the postal_country
     */
    public function setPostalCountry()
    {
        $this->postal_country = !empty($this->fields['address_type']['postal_country']) ? $this->fields['address_type']['postal_country'] : $this->postal_country;
    }

    /**
     * Get the postal_country
     * @return string
     */
    public function getPostalCountry()
    {
        return $this->postal_country;
    }


    /**
     * Set the postal_postcode
     */
    public function setPostalPostcode()
    {
        $this->postal_postcode = !empty($this->fields['address_type']['postal_postcode']) ? $this->fields['address_type']['postal_postcode'] : $this->postal_postcode;
    }

    /**
     * Get the postal_postcode
     * @return string
     */
    public function getPostalPostcode()
    {
        return $this->postal_postcode;
    }



    // *************************
    
    /**
     * Set the header_color_scheme
     */
    public function setHeaderColorScheme()
    {
        $this->header_color_scheme = !empty($this->fields['header_color_scheme']) ? $this->fields['header_color_scheme'] : $this->header_color_scheme;
    }

    /**
     * Get the header_color_scheme
     * @return string
     */
    public function getHeaderColorScheme()
    {
        return $this->header_color_scheme;
    }


    /**
     * Set the footer_color_scheme
     */
    public function setFooterColorScheme()
    {
        $this->footer_color_scheme = !empty($this->fields['footer_color_scheme']) ? $this->fields['footer_color_scheme'] : $this->footer_color_scheme;
    }

    /**
     * Get the footer_color_scheme
     * @return string
     */
    public function getFooterColorScheme()
    {
        return $this->footer_color_scheme;
    }


    /**
     * Set the header_image
     */
    public function setHeaderImage()
    {
        $this->header_image = !empty($this->fields['header_image']) ? $this->fields['header_image'] : $this->header_image;
    }

    /**
     * Get the header_image
     * @return string
     */
    public function getHeaderImage()
    {
        return $this->header_image;
    }


    /**
     * Set the footer_image
     */
    public function setFooterImage()
    {
        $this->footer_image = !empty($this->fields['footer_image']) ? $this->fields['footer_image'] : $this->footer_image;
    }

    /**
     * Get the footer_image
     * @return string
     */
    public function getFooterImage()
    {
        return $this->footer_image;
    }


    //  ************************


    /**
     * Register the post type
     */
    public static function registerPostType()
    {
        if (empty(self::$singularName) || empty(self::$pluralName)) {
            return;
        }

        register_post_type(self::$postType, [
            'labels'             => [
                'name'               => __(self::$pluralName),
                'singular_name'      => __(self::$singularName),
                'add_new'            => __('Add New'),
                'add_new_item'       => __('Add New ' . self::$singularName),
                'edit_item'          => __('Edit ' . self::$singularName),
                'new_item'           => __('New ' . self::$singularName),
                'all_items'          => __('All ' . self::$pluralName),
                'view_item'          => __('View ' . self::$singularName),
                'search_items'       => __('Search ' . self::$pluralName),
                'not_found'          => __('No ' . self::$pluralName . ' found'),
                'not_found_in_trash' => __('No ' . self::$pluralName . ' found in Trash'),
                'parent_item_colon'  => '',
                'menu_name'          => self::$pluralName
            ],
            'publicly_queryable' => true,
            'public'             => true,
            'show_ui'            => true,
            'hierarchical'       => true,
            'menu_position'      => null,
            'query_var'          => true,
            'rewrite'            => [
                'slug' => 'team',
                'with_front' => false
            ],
            'supports'           => ['title', 'thumbnail'],
            // 'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', ),
            'taxonomies'         => array('type', 'category' ),
            'has_archive'        => true,
            'menu_icon'          => 'dashicons-groups'
        ]);
    }
}

add_action('init', __NAMESPACE__ . '\TeamMember::registerPostType', 1);
