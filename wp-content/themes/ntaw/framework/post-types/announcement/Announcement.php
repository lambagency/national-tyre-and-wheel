<?php

namespace LambAgency\PostType;

class Announcement extends PostType
{

    public static $postType = 'announcement';
    public static $singularName = 'Announcement';
    public static $pluralName = 'Announcements';

    private $firstName = '';
    private $surname = '';
    private $position = '';

    public function __construct($postID = 0)
    {
        parent::__construct($postID);

        $this->setFirstName();
        $this->setSurname();
        $this->setPosition();
    }


    /**
     * Get the first name
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }


    /**
     * Set the first name
     */
    public function setFirstName()
    {
        $this->firstName = !empty($this->fields['first_name']) ? $this->fields['first_name'] : $this->firstName;
    }


    /**
     * Get the surname
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }


    /**
     * Set the surname
     */
    public function setSurname()
    {
        $this->surname = !empty($this->fields['surname']) ? $this->fields['surname'] : $this->surname;
    }


    /**
     * Get the position
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }


    /**
     * Set the position
     */
    public function setPosition()
    {
        $this->position = !empty($this->fields['position']) ? $this->fields['position'] : $this->position;
    }


    /**
     * Register the post type
     */
    public static function registerPostType()
    {
        if (empty(self::$singularName) || empty(self::$pluralName)) {
            return;
        }

        register_post_type(self::$postType, [
            'labels'             => [
                'name'               => __(self::$pluralName),
                'singular_name'      => __(self::$singularName),
                'add_new'            => __('Add New'),
                'add_new_item'       => __('Add New ' . self::$singularName),
                'edit_item'          => __('Edit ' . self::$singularName),
                'new_item'           => __('New ' . self::$singularName),
                'all_items'          => __('All ' . self::$pluralName),
                'view_item'          => __('View ' . self::$singularName),
                'search_items'       => __('Search ' . self::$pluralName),
                'not_found'          => __('No ' . self::$pluralName . ' found'),
                'not_found_in_trash' => __('No ' . self::$pluralName . ' found in Trash'),
                'parent_item_colon'  => '',
                'menu_name'          => self::$pluralName
            ],
            'publicly_queryable' => true,
            'public'             => true,
            'show_ui'            => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'query_var'          => true,
            'rewrite'            => [
                'slug' => 'announcement',
                'with_front' => false
            ],
            'supports'           => ['title', 'thumbnail'],
            'taxonomies'         => [],
            'has_archive'        => false,
            'menu_icon'          => 'dashicons-media-default'
        ]);
    }
}

add_action('init', __NAMESPACE__ . '\Announcement::registerPostType', 1);
