<?php

namespace LambAgency\Admin;

function editor_enqueue() {
    wp_enqueue_script(
        'admin-editor',
        \get_stylesheet_directory_uri() . '/framework/admin/editor.js',
        ['acf-blocks'],
        '2.1.0',
        true
    );

    $currentScreen = \get_current_screen();
    if ($currentScreen->post_type) {
        $fields = get_fields('option')["blocks_posttype_{$currentScreen->post_type}"] ?? [];

        if (!empty($fields) && is_array($fields)) {
            $blockNames = array_map(function ($field) use ($currentScreen) {
                return $field["field_blocks_posttype_{$currentScreen->post_type}_block"] ?? '';
            }, $fields);
        }
        if (!empty($blockNames)) {
            wp_localize_script(
                'admin-editor',
                '__lamb',
                [
                    'blocks' => $blockNames
                ]
            );
        }

    }
}

add_action('enqueue_block_editor_assets', __NAMESPACE__ . '\editor_enqueue');

add_action('admin_head', __NAMESPACE__ . '\editor_full_width_gutenberg');
function editor_full_width_gutenberg() {
  echo '<style>
    body.gutenberg-editor-page .editor-post-title__block, body.gutenberg-editor-page .editor-default-block-appender, body.gutenberg-editor-page .editor-block-list__block {
                max-width: 80% !important;
        }
    .block-editor__container .wp-block {
        max-width: 80% !important;
    }
    /*code editor*/
    .edit-post-text-editor__body {
        max-width: none !important;
        margin-left: 2%;
        margin-right: 2%;
    }
    
    /* Remove Advanced Settings */
    .block-editor-block-inspector__advanced {
        display: none !important;
    }
    
    /* Remove Text settings */
    .block-editor-block-inspector>div:not([class]) {
        display: none !important;
    }
    
    /* Remove Reusable block tab */
    .components-tab-panel__tabs-item:nth-child(2) {
        display: none !important;
    }
  </style>';
}
