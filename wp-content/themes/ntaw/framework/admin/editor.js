// __lamb added by wp_localize_script
function addAllDefaultBlocks() {
  if (
    wp.data.select("core/editor").getEditedPostContent() === "" &&
    wp.data.select("core/editor").getCurrentPost().content === ""
  ) {
    if (window.__lamb && window.__lamb.blocks) {
      wp.data.dispatch("core/editor").resetBlocks([]);
      for (let i = 0; i < window.__lamb.blocks.length; i++) {
        const block = wp.blocks.createBlock(window.__lamb.blocks[i], {
          mode: "edit"
        });
        wp.data.dispatch("core/editor").insertBlocks(block);
      }
    }
  }
}

// Post content doesn't seem to be available immediately
jQuery(document).ready(function() {
  if (window.requestIdleCallback) {
    requestIdleCallback(addAllDefaultBlocks, {
      timeout: 500
    });
  } else {
    setTimeout(addAllDefaultBlocks, 500);
  }
});
