<?php

// URLs
//---------------------------------
define('COMPONENTS', 'components');
define('MODULES', 'modules');
define('SNIPPETS', 'snippets');
define('TEMPLATES', 'templates');


define('THEME_URI', get_stylesheet_directory_uri());
define('INTERFACE_BUILD_URI', THEME_URI . '/interface/build/');
define('CORE_URI', THEME_URI . '/functions/core/');
define('THEME_JS', INTERFACE_BUILD_URI . 'js');
define('THEME_IMAGES', THEME_URI . '/interface/img/');


define('THEME_PATH', get_stylesheet_directory());
define('BUILD_PATH', THEME_PATH . '/build/');
define('FRAMEWORK_PATH', THEME_PATH . '/framework/');
define('INTERFACE_BUILD_PATH', THEME_PATH . '/interface/build/');
define('COMPONENTS_PATH', THEME_PATH . '/' . COMPONENTS . '/');
define('MODULES_PATH', THEME_PATH . '/' . MODULES . '/');
define('SNIPPETS_PATH', THEME_PATH . '/' . SNIPPETS . '/');
define('TEMPLATES_PATH', THEME_PATH . '/' . TEMPLATES . '/');


define('BASE_URL', esc_url(home_url('/')));
define('API_NAMESPACE', 'la/v1');
define('JSON_URL', BASE_URL . 'wp-json/' . API_NAMESPACE . '/');
define('AJAX_URL', BASE_URL . 'wp-admin/admin-ajax.php');

if (!defined('APP_ENV')) {
  define('APP_ENV', 'production');
}
