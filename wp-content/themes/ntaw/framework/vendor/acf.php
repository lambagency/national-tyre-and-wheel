<?php

namespace LambAgency\Vendor;

/**
 * ACF Settings
 */
function initialiseACF()
{

    if (!in_array('lambagency', wp_get_current_user()->roles)) {
        acf_update_setting('show_admin', false); // Don’t show ACF in the WP admin.
    }

    if (\LambAgency\Utility::getGoogleMapKey()) {
        acf_update_setting('google_api_key', \LambAgency\Utility::getGoogleMapKey());
    }
}

add_action('acf/init', __NAMESPACE__ . '\initialiseACF');


/**
 * For a matching ACF Field Group, set the local JSON directory to save the file to
 *
 * @param $path
 *
 * @return string
 */
function acfJsonSaveDir($path)
{
    if (!is_admin()) {
        return $path;
    }

    if (!user_can(wp_get_current_user(), 'manage_options')) {
        return $path;
    }

    // make sure we're only dealing with ACF field groups
    if (isset($_POST['post_type']) && $_POST['post_type'] == 'acf-field-group') {
        $postTitle = strtolower($_POST['post_title']);
        $customPath = getPath($postTitle, true);

        if ($customPath != '') {
            return $customPath;
        }
    }

    return $path;
}
add_filter('acf/settings/save_json', __NAMESPACE__ . '\acfJsonSaveDir');


/**
 * Add additional ACF local JSON directories
 *
 * @param array $paths
 *
 * @return array
 */
function acfJsonLoadDirs(array $paths)
{
    if (!is_admin()) {
        return $paths;
    }

    if (!user_can(\wp_get_current_user(), 'manage_options')) {
        return $paths;
    }

    $acfFieldGroups = isset($_GET['post_type']) && $_GET['post_type'] == 'acf-field-group';
    $postID = isset($_GET['post']) ? $_GET['post'] : '';

    $acfDirectories = [
        THEME_PATH . '/' . MODULES . '/',
        THEME_PATH . '/' . COMPONENTS . '/',
        FRAMEWORK_PATH . 'post-types/'
    ];

    $paths[] = FRAMEWORK_PATH . 'acf-field-groups/';

    // ACF Field Groups page
    if ($acfFieldGroups) {
        // Loop through each acf directory to look for field groups
        foreach ($acfDirectories as $acfDirectory) {
            // loop through each first level directory and add to paths array
            foreach (new \DirectoryIterator($acfDirectory) as $fileInfo) {
                if ($fileInfo->isDot()) {
                    continue;
                }

                if (is_dir($fileInfo->getPathname())) {
                    $paths[] = $fileInfo->getPathname();
                }
            }
        }
    } elseif ($postID) {
        // Edit specific ACF Field Group
        $post = get_post($postID);

        if ($post && $post->post_type == 'acf-field-group') {
            $postTitle = strtolower($post->post_title);

            $path = getPath($postTitle, false);

            if ($path) {
                unset($paths[0]);
                $paths[] = $path;
            }
        }
    }

    return $paths;
}
add_filter('acf/settings/load_json', __NAMESPACE__ . '\acfJsonLoadDirs');


/**
 * Returns path of matching field group
 *
 * @param string $postTitle
 * @param bool   $mkDir
 *
 * @return string
 */
function getPath($postTitle, $mkDir = false)
{
    $path   = '';
    $types  = ['module', 'component', 'posttype', 'block'];

    // Specify the field groups to ignore
    $ignore = ['module settings'];

    $titleSplit = explode(' ', $postTitle);

    // check if field group is a matching type
    if (is_array($titleSplit) && in_array($titleSplit[0], $types) && !in_array($postTitle, $ignore)) {
        // module/component name
        $fieldTitle = trim(str_replace($titleSplit[0], '', $postTitle));
        $fieldTitle = trim(str_replace(' ', '-', $fieldTitle));


        // add components dir
        if ($titleSplit[0] == 'component') {
            $path = THEME_PATH . '/' . COMPONENTS . '/';
        } elseif ($titleSplit[0] == 'module' || $titleSplit[0] == 'block') {
            $path = THEME_PATH . '/' . MODULES . '/';
        } elseif ($titleSplit[0] == 'posttype') {
            $path = FRAMEWORK_PATH . 'post-types/';
        }

        $path .= "{$fieldTitle}";

        if ($mkDir && !is_dir($path)) {
            mkdir($path);
        }
    } else {
        $path = FRAMEWORK_PATH . 'acf-field-groups/';

        if ($mkDir && !is_dir($path)) {
            mkdir($path);
        }
    }

    return $path;
}
