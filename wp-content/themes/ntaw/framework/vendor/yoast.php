<?php

namespace LambAgency\Vendor;

/**
 * Move the yoast metabox to the bottom
 *
 * @return string
 */
function yoastMetaboxToBottom()
{
    return 'low';
}
add_filter('wpseo_metabox_prio', __NAMESPACE__ . '\yoastMetaboxToBottom');
