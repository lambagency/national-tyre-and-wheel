<?php

namespace LambAgency\Vendor;

/**
 * Remove the add form button from WYSIWYG
 */
add_filter('gform_display_add_form_button', '__return_false');


/**
 * Add australian states to the GForms address
 *
 * @param array $addressTypes
 * @param int   $formID
 *
 * @return array
 */
function addGFAustralianStates(array $addressTypes, $formID)
{
    $addressTypes['au'] = [

        'label'       => esc_html__('Australia', 'gravityforms'),
        'zip_label'   => gf_apply_filters('gform_address_zip', $formID, esc_html__('Postcode', 'gravityforms'), $formID),
        'state_label' => gf_apply_filters('gform_address_state', $formID, esc_html__('State', 'gravityforms'), $formID),
        'country'     => 'Australia',
        'states'      => array_merge([''], getAUStates())
    ];

    return $addressTypes;
}

add_filter('gform_address_types', __NAMESPACE__ . '\addGFAustralianStates', 15, 2);


/**
 * Create array to Australia states
 *
 * @return mixed
 */
function getAUStates()
{
    return apply_filters(
        'gform_au_states',
        [
            esc_html__('Australian Capital Territory', 'gravityforms'),
            esc_html__('New South Wales', 'gravityforms'),
            esc_html__('Northern Territory', 'gravityforms'),
            esc_html__('Queensland', 'gravityforms'),
            esc_html__('Southern Australia', 'gravityforms'),
            esc_html__('Victoria', 'gravityforms'),
            esc_html__('Tasmania', 'gravityforms'),
            esc_html__('Western Australia', 'gravityforms'),
        ]
    );
}


/**
 * Update all notification emails before sending.
 * If live site, add backup@lambagency.com.au as BCC recipient
 *
 * @param $notification
 * @param $form
 * @param $entry
 *
 * @return mixed
 */
function updateNotificationEmails($notification, $form, $entry)
{
    $notification['subject'] = (isset($notification['fromName']) ? $notification['fromName'] : get_bloginfo('name'))  . ' - ' . $notification['subject'];

    if (defined('APP_ENV') && APP_ENV === 'production') {
        $notification['bcc'] = 'backup@lambagency.com.au';
    }

    return $notification;
}

add_filter('gform_notification', __NAMESPACE__ . '\updateNotificationEmails', 10, 3);


/**
 * If field type is 'select', add 'select-wrap' CSS class
 *
 * @param $fieldContent
 * @param $field
 *
 * @return mixed
 */
function modifyFormInputContent($fieldContent, $field)
{
    if ($field->type == 'select') {
        $fieldContent = str_replace('ginput_container_select', 'ginput_container_select select-wrap', $fieldContent);
    }

    $fieldContent = str_replace('aria-required="true"', 'required="true" aria-required="true"', $fieldContent);

    return $fieldContent;
}

add_filter('gform_field_content', __NAMESPACE__ . '\modifyFormInputContent', 10, 2);


/**
 * Applies a custom CSS class for any matching fields
 * @param array $form
 * @return mixed
 */
function addCustomCSSClass(array $form)
{
    foreach ($form['fields'] as &$field) {
        switch ($field->type) {
            case 'select':
                $field->cssClass .= ' select-field';
                break;

            case 'checkbox':
                $field->cssClass .= ' checkbox-field';
                break;

            case 'radio':
                $field->cssClass .= ' radio-field';
                break;
            default:
        }
    }

    return $form;
}

add_filter('gform_pre_render', __NAMESPACE__ . '\addCustomCSSClass');



/**
 * Fix Gravity Form Tabindex Conflicts
 * http://gravitywiz.com/fix-gravity-form-tabindex-conflicts/
 *
 * @param      $tabIndex
 * @param bool $form
 * @return int
 */
function gformTabIndexer($tabIndex, $form = false)
{
    $startingIndex = 1000;

    if ($form) {
        add_filter('gform_tabindex_' . $form['id'], __NAMESPACE__ . '\gformTabIndexer');
    }

    return \GFCommon::$tab_index >= $startingIndex ? \GFCommon::$tab_index : $startingIndex;
}

add_filter('gform_tabindex', __NAMESPACE__ . '\gformTabIndexer', 10, 2);


/**
 * Sanitizes text and textarea fields
 * @param array $form
 */
function preSubmission(array $form)
{
    $inputFields = [
        'text',
        'textarea'
    ];

    $fields = $form['fields'];

    if ($fields) {
        foreach ($fields as $field) {
            if (in_array($field->type, $inputFields)) {
                $val = rgpost('input_' . $field->id);

                if ($field->type == 'text') {
                    $val = sanitize_text_field($val);
                } elseif ($field->type == 'textarea') {
                    $val = str_replace(array("\r\n"), array("\n"), $val);
                    $val = implode("\n", array_map('sanitize_text_field', explode("\n", $val)));
                }

                $_POST['input_' . $field->id] = $val;
            }
        }
    }
}
add_action('gform_pre_submission', __NAMESPACE__ . '\preSubmission');



/**
 * ===================================================
 * Custom columns for form fields
 * ===================================================
 */


/**
 * Add custom options to gravity form fields
 * Columns - dropdown of column sizes for field (1,2,3)
 *
 * @param $position
 * @param $formID
 */
function formFieldCustomOptions($position)
{
    // create settings on position 25 (right after Description)
    if ($position == 25) {
        ?>
        <li class="column_setting field_setting">
            <label for="field_column" class="section_label"><?php _e("Columns", "gravityforms"); ?></label>
            <select id="field_column" onchange="SetFieldProperty('columnValue', this.value);">
                <option value="cell small-12">Full-Width</option>
                <option value="cell small-12 medium-6">Half</option>
                <option value="cell small-12 medium-4">One Third</option>
            </select>
        </li>
        <?php
    }
}
add_action('gform_field_standard_settings', __NAMESPACE__ . '\formFieldCustomOptions', 10);


/**
 * Add support script for field custom options
 * Initialises fields and loading values
 */
function editorScript()
{
    ?>
    <script type="text/javascript">

        // Add custom field setting to all fields
        jQuery.each(fieldSettings, function(index, value) {
            fieldSettings[index] += ', .column_setting';
        });

        jQuery(document).bind('gform_load_field_settings', function(event, field, form) {

            if (typeof field['columnValue'] !== 'undefined') {
                jQuery('#field_column').attr('value', field['columnValue']);
            } else {
                jQuery('#field_column').val(jQuery('#field_column option:first').val());
                SetFieldProperty('columnValue', jQuery('#field_column').val());
            }
        });
    </script>
    <?php
}
add_action('gform_editor_js', __NAMESPACE__ . '\editorScript');


/**
 * Applies a custom CSS class for any matching fields
 * @param array $form
 * @return mixed
 */
function addCSSColumns(array $form)
{
    foreach ($form['fields'] as &$field) {
        if (isset($field['columnValue'])) {
            $field->cssClass .= ' ' . $field['columnValue'];
        }
    }

    return $form;
}

add_filter('gform_pre_render', __NAMESPACE__ . '\addCSSColumns');


/**
 * Add the wrapping class for the CSS columns
 * @param $form
 * @return mixed
 */
function addCSSColumnsWrap($form)
{
    $form = str_replace('gform_fields', 'gform_fields grid-x grid-margin-x grid-margin-y align-top', $form);
    $form = str_replace('gform_footer', 'gform_footer grid-x align-center', $form);

    return $form;
}
add_filter('gform_get_form_filter', __NAMESPACE__ . '\addCSSColumnsWrap', 10);
