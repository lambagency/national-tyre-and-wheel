<?php
namespace LambAgency\Vendor;

function update_qm_capabilities() {
  $role = get_role( 'administrator' );

  if ($role) {
    $role->add_cap('view_query_monitor', false);
  }
}
add_action( 'admin_init', __NAMESPACE__ . '\update_qm_capabilities');
