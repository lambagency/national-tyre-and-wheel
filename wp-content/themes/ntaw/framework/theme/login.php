<?php

namespace LambAgency\Authentication;

/**
 * Replace WP logo on login screen with site's logo
 */
function customLoginLogo()
{
    if (function_exists('get_field')) {
        $logo           = get_field('header_logo', 'option');
        $logoUrl        = $logo ? $logo['sizes']['square-small'] : '';
        $logoWidth      = $logo ? $logo['sizes']['square-small-width'] : '';
        $logoHeight     = $logo ? $logo['sizes']['square-small-height'] : '';

        $primaryColor  = '#477dca';

        echo '<style type="text/css">
           ' . ($logo ? '.login h1 a { width: ' . $logoWidth . 'px; height: ' . $logoHeight . 'px; background-image: url(' . $logoUrl . '); background-size: cover; }' : '') .
        '</style>';
    }
}
add_action('login_head', __NAMESPACE__ . '\customLoginLogo');


/**
 * Change WP URL on login page to home page
 *
 * @return string
 */
function customLoginLogoURL()
{
    return esc_url(home_url('/'));
}
add_filter('login_headerurl', __NAMESPACE__ . '\customLoginLogoURL');


/**
 * Hides username field
 */
function registrationStyling()
{
    echo '
        <style>
            #registerform > p:first-child{
                display: none;
            }
        </style>
    ';
}
add_action('login_head', __NAMESPACE__ . '\registrationStyling');
