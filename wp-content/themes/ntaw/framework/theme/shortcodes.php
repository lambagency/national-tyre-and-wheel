<?php

namespace LambAgency\Theme\Shortcode;

/**
 * Register shortcodes for the theme
 */
function registerShortcodes()
{
    add_shortcode('date', __NAMESPACE__ . '\displayDate');
    add_shortcode('page_title', __NAMESPACE__ . '\pageTitle');
    add_shortcode('button', __NAMESPACE__ . '\displayButton');
}

add_action('init', __NAMESPACE__ . '\registerShortcodes');


/**
 * This is used to get the current date in any supported PHP date format
 *
 * @example shortcode use: [data format='Y']
 *
 * @param $atts
 *
 * @return false|string
 */
function displayDate($atts)
{

    $atts = array_change_key_case((array)$atts, CASE_LOWER);

    // override default attributes with user attributes
    $atts = shortcode_atts([
        'format' => 'd/m/Y'
    ], $atts);


    return date($atts['format']);
}


/**
 * This is used to get the current page title
 *
 * @example shortcode use: [page_title]
 *
 * @param $atts
 *
 * @return string
 */
function pageTitle($atts)
{

    $title = get_the_title();

    // Blog listing Page
    if (is_home()) {
        $title = get_the_title(get_option('page_for_posts'));
    // Category Page
    } elseif (is_category()) {
        $title = single_cat_title('', false);
    // Custom Post Type Archive Page
    } elseif (is_post_type_archive()) {
        $title = post_type_archive_title('', false);
    // Search Results page
    } elseif (is_search()) {
        $title = 'Search Results';
    // 404 Page
    } elseif (is_404()) {
        $title = '404';
    } elseif (is_tax()) {
        $title = single_term_title('', false);
    }

    return $title;
}


// Register custom shortcode for button
function displayButton($attr) {   
    shortcode_atts(
        array(
            'text' => 1,
            'link' => '/about',
            'theme' => 'primary'
        ), $attr
    );
   
    return '<a href="'.$attr['link'].'" class="button  theme-'.$attr['theme'].'">' . $attr['text'] . '</a>';
}