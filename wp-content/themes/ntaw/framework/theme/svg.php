<?php

namespace LambAgency\Theme;

/**
 * Add SVG mime types to allowed list
 *
 * @param $mimes
 *
 * @return mixed
 */
function addSVGMimeType($mimes)
{
    $mimes['svg']  = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';

    return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__ . '\addSVGMimeType');


/**
 * Returns the full URL of a media attachment (SVG)
 */
function getAttachmentURLMediaLibrary()
{
    $url = '';

    $attachmentID = isset($_REQUEST['attachmentID']) ? $_REQUEST['attachmentID'] : '';
    if ($attachmentID) {
        $url = wp_get_attachment_url($attachmentID);
    }

    echo $url;

    exit;
}
add_action('wp_ajax_getAttachmentURLMediaLibrary', __NAMESPACE__ . '\getAttachmentURLMediaLibrary');


/**
 * Fixes displaying uploaded SVG images in admin
 */
function svgSize()
{
    echo '
        <style>
            .media-icon svg, .media-icon img[src*=".svg"] { width: 100% !important; height: auto; }
            img.icon[src*=".svg"] { width: 50%; }
            .edit-attachment-frame .attachment-media-view .details-image { width: 50%; height: auto; max-width: inherit; max-height: inherit; margin: 0 auto; }
        </style>
    ';
}
add_action('admin_head', __NAMESPACE__ . '\svgSize');
