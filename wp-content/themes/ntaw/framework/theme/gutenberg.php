<?php
namespace LambAgency\Editor;

/**
 * Add a grid container around core modules
 *
 * @param   string $block_content
 * @param   array $block
 * @return  string
 */
function wrap_blocks($block_content, $block) {
  if (empty($block['blockName'])) {
    return $block_content;
  }

  // Special handling for core column blocks
  switch($block['blockName']) {
    case 'core/columns':
      return sprintf(
        '<div class="mod-core core-columns"><div class="grid-container"><div class="grid-x grid-margin-x grid-margin-y">%s</div></div></div>',
        implode(
          array_map(
            function($block){
              return \render_block($block);
            },
            $block['innerBlocks']
          )
        )
      );
      break;
    case 'core/column':
      return sprintf(
        '<div class="cell large-auto core-column">%s</div>',
        $block_content
      );
      break;
  }

  $container_exclusions = [];


  if (
    (!isset($block['attrs']['has_parent']) ||  $block['attrs']['has_parent'] === false) &&
    false === strpos($block['blockName'], \LambAgency\Framework\ModuleFramework::$blockKeyPrefix) &&
    false === in_array($block['blockName'], $container_exclusions)
  ) {
    $block_content = sprintf(
      '<div class="%s %s"><div class="grid-container">%s</div></div>',
      'mod-core',
      str_replace('/', '-', $block['blockName']),
      $block_content
    );
  }
  return $block_content;
}
add_filter('render_block', __NAMESPACE__ . '\wrap_blocks', 10, 2);

/**
 * Add data to identify modules that are within a parent element
 *
 * @param     array $block
 * @return    string
 */
function pre_wrap_blocks($block) {
  if (!empty($block['innerBlocks'])) {
    foreach($block['innerBlocks'] as $key => $innerBlock) {
      $innerBlock['attrs'] = array_merge(
        $innerBlock['attrs'],
        [
          'inner'      => true,
          'has_parent' => true
        ]
      );
      $block['innerBlocks'][$key] = $innerBlock;
    }
  }

  return $block;
}
add_filter('render_block_data', __NAMESPACE__ . '\pre_wrap_blocks', 11, 3);
