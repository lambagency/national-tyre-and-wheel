<?php

namespace LambAgency\Authentication;

/**
 * Create 'Lamb Agency' role based off the 'Administrator' role
 */
function addLambAgencyRole()
{
    global $wp_roles;

    // https://docs.gravityforms.com/role-management-guide/
    $gf_capabilities = [
      'gravityforms_edit_forms' => true,
      'gravityforms_delete_forms' => true,
      'gravityforms_create_form' => true,
      'gravityforms_view_entries' => true,
      'gravityforms_edit_entries' => true,
      'gravityforms_delete_entries' => true,
      'gravityforms_view_settings' => true,
      'gravityforms_edit_settings' => true,
      'gravityforms_export_entries' => true,
      'gravityforms_uninstall' => true,
      'gravityforms_view_entry_notes' => true,
      'gravityforms_edit_entry_notes' => true,
      'gravityforms_view_updates' => true,
      'gravityforms_view_addons' => true,
      'gravityforms_preview_forms' => true,
      'gravityforms_system_status' => true,
      'gravityforms_logging' => true
    ];
    $yoast_capabilities = [
      'wpseo_manage_options' => true
    ];
    $qm_capabilities = [
      'view_query_monitor'
    ];

    if (!isset($wp_roles)) {
        $wp_roles = new \WP_Roles();
    }

    $lambAgencyRole = 'lambagency';

    if (!$wp_roles->is_role($lambAgencyRole)) {
        $administrator = $wp_roles->get_role('administrator');

        if ($administrator) {
            $wp_roles->add_role(
                $lambAgencyRole,
                'Lamb Agency',
                array_merge(
                    $administrator->capabilities,
                    $gf_capabilities,
                    $yoast_capabilities,
                    $qm_capabilities
                )
            );
        }
    }
}
add_action('init', __NAMESPACE__ . '\addLambAgencyRole');
