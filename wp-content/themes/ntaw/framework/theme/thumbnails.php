<?php

namespace LambAgency\Theme;

/**
 * Remove default Wordpress thumbnail sizes
 *
 * @param array $sizes
 *
 * @return mixed
 */
function removeThumbnailSizes(array $sizes)
{
    unset($sizes['large']);
    unset($sizes['medium_large']);
    unset($sizes['medium']);

    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', __NAMESPACE__ . '\removeThumbnailSizes');
add_filter('image_size_names_choose', __NAMESPACE__ . '\removeThumbnailSizes');


/**
 * Enable support for Post Thumbnails
 */
function initThumbnails()
{
    add_theme_support('post-thumbnails');

    add_image_size('module-bg', 1800, 800, false);
    add_image_size('module-bg-mobile', 1000, 600, false);

    add_image_size('square-small', 300, 300, false);
    add_image_size('square-medium', 600, 600, false);

    add_image_size('thumbnail', 150, 150, false);
}
add_action('after_setup_theme', __NAMESPACE__ . '\initThumbnails');


/** change WP default jpeg compression from 90 to 80 **/
function jpegQuality()
{
    return 90;
}
add_filter('jpeg_quality', __NAMESPACE__ . '\jpegQuality');


/**
 * @param $sizes
 *
 * @return array
 */
function initImageSizes($sizes)
{
    $addsizes = [
        'square-small'  => __( 'Small'),
        'square-medium' => __( 'Medium'),
        'module-bg'     => __( 'Large')
    ];

    $newsizes = array_merge($sizes, $addsizes);

    return $newsizes;
}
add_filter('image_size_names_choose', __NAMESPACE__ . '\initImageSizes');
