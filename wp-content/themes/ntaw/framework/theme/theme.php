<?php
namespace LambAgency\Custom;

use LambAgency\Helpers\Asset;

/**
 * Creates preload tags for common snippets, components and the blocks included on the page
 */
function preloadAssets() {
  $preloadSnippets = [
    'footer'
  ];
  $preloadComponents = [];

  Asset::preload_block('core');

  Asset::preload_page_blocks();

  foreach($preloadSnippets as $snippet) {
    Asset::preload_block($snippet, 'snippet');
  }
  foreach($preloadComponents as $component) {
    Asset::preload_block($component, 'component');
  }
}

add_action('wp_head', __NAMESPACE__ . '\preloadAssets');


/**
 * Registers custom scripts used throughout the site's templates
 */
function registerAssets()
{
    wp_dequeue_style('wp-block-library');
    wp_dequeue_style('wp-block-library-theme');

    wp_enqueue_style(
        'google-fonts',
        '//fonts.googleapis.com/css?family=Montserrat:400,400i,500,600',
        [],
        null
    );

    wp_deregister_script('jquery');
    wp_enqueue_script(
        'jquery',
        '//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js',
        [],
        null,
        false
    );

    wp_enqueue_script(
      'production-js',
      Asset::get_asset( 'production.js' ),
      [],
      null,
      true
    );

    if ( Asset::has_asset( 'style.css' ) ) {
      wp_enqueue_style(
        'theme-styles',
        Asset::get_asset( 'style.css' ),
        [],
        null
      );
    }

  if ( Asset::has_asset( 'style.js' ) ) {
    // TODO: Remove extra chunk once this bug is fixed: see https://github.com/webpack-contrib/mini-css-extract-plugin/issues/85
    wp_enqueue_script(
      'theme-manifest',
      Asset::get_asset( 'style.js' ),
      [ 'production-js' ],
      null,
      true
    );
  }
}

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\registerAssets');

/**
 * Returns whether current environment is dev or live
 *
 * @return bool
 */
function isDevEnv()
{
    return defined('APP_ENV') && (APP_ENV == "dev" || APP_ENV == "local");
}
