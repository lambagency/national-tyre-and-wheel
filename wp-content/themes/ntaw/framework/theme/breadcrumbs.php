<?php

namespace LambAgency\Breadcrumbs;

/**
 * Custom breadcrumb implementations for specific pages/archives/post types
 *
 * @param array $links
 *
 * @return mixed
 */
function updateBreadcrumbs(array $links)
{
    $links = breadcrumbs404($links);
    $links = breadcrumbsCustomTaxonomy($links);
    $links = breadcrumbsCustomPostType($links);

    return $links;
}
add_filter('wpseo_breadcrumb_links', __NAMESPACE__ . '\updateBreadcrumbs');


/**
 * If custom permalink is defined for blog page, remove Blog from appearing in 404 page
 *
 * @param array $links
 *
 * @return array
 */
function breadcrumbs404(array $links)
{
    if (is_404()) {
        unset($links[1]);
        $links = array_values($links);
    }

    return $links;
}


/**
 * Define the taxonomy breadcrumbs structure
 *
 * @param array $links
 *
 * @return array
 */
function breadcrumbsCustomTaxonomy(array $links)
{
    if (is_tax()) {

    }

    return $links;
}


/**
 * Define the custom post type's breadcrumbs structure
 *
 * @param array $links
 *
 * @return array
 */
function breadcrumbsCustomPostType(array $links)
{
    // example
//    if (is_singular('event')) {
//
//        $breadcrumb[] = array(
//            'url'   => \LambAgency\Utility::getLink('event');,
//            'text'  => 'Events',
//        );
//
//        array_splice($links, 1, -count($links), $breadcrumb);
//    }

    return $links;
}