<?php

namespace LambAgency\Theme;


/**
 * Join posts and postmeta tables
 *
 * @param $join
 * @param \WP_Query $query
 *
 * @return string
 */
function searchJoin($join, \WP_Query $query)
{
    if (!empty($query->query_vars['s']) && !empty($query->query_vars['search-meta'])) {
        global $wpdb;
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' AS meta_table ON '. $wpdb->posts . '.ID = meta_table.post_id ';
    }

    return $join;
}
add_filter('posts_join', __NAMESPACE__ . '\searchJoin', 10, 2);


/**
 * Allows multiple keywords to be search independently and also searches custom fields
 *
 * @param $where
 * @param \WP_Query $query
 *
 * @return string
 */
function searchWhere($where, \WP_Query $query)
{

    if (!empty($query->query_vars['s']) && !empty($query->query_vars['search-meta'])) {
        global $wpdb;

        $search = $query->query_vars['s'];

        if (strpos($search, ' ') !== false) {
            $searchArray = explode(' ', $search);

            $startWhere = strpos($where, ')))');

            if ($startWhere > 0) {
                $startWhere += 3;
                $newWhere = substr($where, $startWhere);

                $updatedWhere = ' AND (';

                foreach ($searchArray as $index => $keyword) {
                    if ($index > 0) {
                        $updatedWhere .= ' OR ';
                    }

                    $updatedWhere .= "(({$wpdb->posts}.post_title LIKE '%{$keyword}%') OR ({$wpdb->posts}.post_content LIKE '%{$keyword}%') OR (meta_table.meta_value LIKE '%{$keyword}%' AND meta_table.meta_key NOT LIKE '\_%'))";
                }

                $updatedWhere .= ')' . $newWhere;

                $where = $updatedWhere;
            }
        } else {
            $where = preg_replace(
                "/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
                "(" . $wpdb->posts . ".post_title LIKE $1) OR (meta_table.meta_value LIKE $1 AND meta_table.meta_key NOT LIKE '\_%')", $where);
        }
    }

    return $where;
}
add_filter('posts_where', __NAMESPACE__ . '\searchWhere', 10, 2);


/**
 * Prevents duplicates from appearing in search results
 *
 * @param $where
 * @param \WP_Query $query
 *
 * @return string
 */
function searchDistinct($where, \WP_Query $query)
{

    if (!empty($query->query_vars['s'])
        && !empty($query->query_vars['search-meta'])
        && $query->query_vars['search-meta']) {
        return 'DISTINCT';
    }

    return $where;
}
add_filter('posts_distinct', __NAMESPACE__ . '\searchDistinct', 10, 2);



/**
 * If a query has the variable 'exclude-hidden' the query will hide any pages/posts with the Yoast
 * no-index option set to true. Useful for preventing hidden pages from displaying in search
 *
 * @param \WP_Query $query
 */
function hideNoIndexedResults(\WP_Query $query)
{

    if (!empty($query->query_vars['exclude-hidden']) && $query->query_vars['exclude-hidden']) {
        $meta_query = $query->get('meta_query');

        // Force array
        $meta_query = is_array($meta_query) ? $meta_query : [];

        //Add our meta query to the original meta queries
        $meta_query[] = [
            'relation' => 'OR',
            [
                'key'     => '_yoast_wpseo_meta-robots-noindex',
                'compare' => 'NOT EXISTS',
            ],
            [
                'key'     => '_yoast_wpseo_meta-robots-noindex',
                'value'   => '1',
                'compare' => '!=',
            ]
        ];

        $query->set('meta_query', $meta_query);
    }
}
add_action('pre_get_posts', __NAMESPACE__ . '\hideNoIndexedResults', 10, 2);
