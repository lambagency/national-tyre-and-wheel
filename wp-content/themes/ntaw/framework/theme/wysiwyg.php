<?php

namespace LambAgency\WYSIWYG;

/**
 * @TODO: Gutenberg
 * @param $settings
 * @return mixed
 */
function populateFormatSelect($settings)
{
    $styles = [
        [
            'title'     => 'Lead Paragraph',
            'classes'   => 'lead',
            'selector'  => 'p',
            'wrapper'   => false,
        ],
        [
            'title'     => 'Tick List',
            'classes'   => 'tick-list',
            'selector'  => 'ul',
            'wrapper'   => false
        ]
    ];

    // Insert the array, JSON ENCODED, into 'style_formats'
    $settings['style_formats'] = json_encode($styles);

    return $settings;
}
add_filter('tiny_mce_before_init', __NAMESPACE__ . '\populateFormatSelect');


/**
 * Add format select option to WYSIWYG
 * @param $buttons
 * @return mixed
 */
function addFormatSelectToWYSIWYG($buttons)
{
    // Add it as the first item
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons', __NAMESPACE__ . '\addFormatSelectToWYSIWYG');
