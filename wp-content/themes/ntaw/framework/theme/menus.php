<?php

namespace LambAgency\Menus;

/**
 * Register menu locations for the them
 */
function registerMenus()
{
    register_nav_menus(array(
        'header-main'   => __('Header - Main', 'header-main'),
        'footer-main'   => __('Footer - Main', 'footer-main'),
        'footer-legals' => __('Footer - Legals', 'footer-legals'),
    ));
}
add_action('after_setup_theme', __NAMESPACE__ . '\registerMenus');


/**
 * Add Lamb Agency link to footer legals menu
 *
 * @param $items
 * @param $args
 *
 * @return string
 */
function addLambLinkToLegalsMenu($items, $args)
{
    // if (in_array($args->theme_location, ['footer-legals'])) {
    //     $items .= '<li><a href="https://www.lambagency.com.au" target="_blank" rel="nofollow noopener">Website by Lamb Agency</a></li>';
    // }

    return $items;
}
add_filter('wp_nav_menu_items', __NAMESPACE__ . '\addLambLinkToLegalsMenu', 10, 2);


/**
 * An example extension of a walker, used by default for themes.
 *
 * Class DefaultNavWalker
 *
 * @package LambAgency\Menus
 */
class DefaultNavWalker extends \Walker_Nav_Menu
{
    public function start_lvl(&$output, $depth = 0, $args = [])
    {
        $output .= '<span class="sub-menu-toggle"></span><ul class="sub-menu">';
    }

    public function end_lvl(&$output, $depth = 0, $args = [])
    {
        $output .= '</ul>';
    }

    public function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $item->classes[] = 'depth-' . $depth;
        parent::start_el($output, $item, $depth, $args, $id);
    }

    public function end_el(&$output, $item, $depth = 0, $args = [])
    {
        parent::end_el($output, $item, $depth, $args);
    }
}
