<?php
namespace LambAgency\Framework;

/**
 * Class ModuleFramework
 *
 * This class handles the core of the framework. It performs the following:
 *  - Autoloads modules and constructs the appropriate flexible content field groups.
 *  - Loads default modules into the flexible content field per post type/taxonomy
 *
 * @package LambAgency\Modules
 */
class ModuleFramework
{

    /**
     * @var ModuleFramework Holds an instance of the Class
     */
    private static $instance = null;

    private $moduleIndex = 0;

    /**
     * @var \LambAgency\Framework\AutoloadedModule[]
     */
    private $modules = [];

    private static $blockCategorySlug = 'lamb';

    public static $blockKeyPrefix = 'acf/';

    public static $includedDefaultBlocks = [
      // 'core/block',
      // 'core/paragraph',
      // 'core/image',
      // 'core/heading',
      // 'core/gallery',
      // 'core/list',
      // 'core/quote',
      // 'core/shortcode',
      // 'core/archives',
      // 'core/audio',
      // 'core/button',
      // 'core/calendar',
      // 'core/categories',
      // 'core/code',
      // 'core/columns',
      'core/column',
      // 'core/cover',
      // 'core/embed',
      // 'core/file',
      // 'core/freeform',
      // 'core/html',
      // 'core/media-text',
      // 'core/latest-comments',
      // 'core/latest-posts',
      // 'core/more',
      // 'core/nextpage',
      // 'core/preformatted',
      // 'core/pullquote',
      // 'core/rss',
      // 'core/search',
      // 'core/separator',
      // 'core/spacer',
      // 'core/subhead',
      // 'core/table',
      // 'core/tag-cloud',
      // 'core/template',
      // 'core/text-columns',
      // 'core/verse',
      // 'core/video',
    ];

    /**
     * @var array Array of locations (post types/taxonomies) where flexible modules are for
     */
    private $moduleLocations = [
        'post-types' => [],
        'taxonomies' => []
    ];


    public function __construct()
    {

        // Client and Admin functions
        $this->setModules();


        if (function_exists('acf_register_block_type')) {
            add_action('acf/init', [$this, 'registerBlocks']);
            add_filter('allowed_block_types', [$this, 'initBlockScoping'], 10, 2);
            add_filter('block_categories', [$this, 'addBlockCategories'], 10, 2);

            // disable custom colors
            add_theme_support( 'disable-custom-colors' );
            add_theme_support( 'disable-custom-colors' );
            add_theme_support( 'editor-color-palette' );
            add_theme_support( 'editor-gradient-presets', [] );
            add_theme_support( 'disable-custom-gradients' );

            // disable text settings
            add_theme_support('editor-font-sizes', []);

            // Remove Pattern tab
            add_filter('block_editor_settings', [$this, 'removeCorePatterns']);

            // Remove Block Patterns
            remove_theme_support( 'core-block-patterns' );

            // Remove Block Directory
            add_action( 'admin_init', function() {
                remove_action( 'enqueue_block_editor_assets', 'wp_enqueue_editor_block_directory_assets' );
            } );
        }

        // Admin only functions
        if (is_admin()) {
            add_action('acf/init', [$this, 'registerOptionPages'], 6);
            add_action('acf/init', [$this, 'setModuleLocations'], 6);
            add_filter('acf/init', [$this, 'registerACFGroupModuleScoping']);
            add_action('acf/init', [$this, 'registerACFGroupModuleLocations']);
        }
    }



    /**
     * Creates or returns an instance of this class.
     *
     * @return ModuleFramework single instance of this class.
     */
    public static function getInstance()
    {
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    /**
     * Get array of autoloaded modules
     *
     * @return \LambAgency\Framework\AutoloadedModule[]
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * Statically find all modules (based on glob loading)
     *
     * @return AutoloadedModule[]
     */
    public static function getAllModules()
    {
        $modules = [];
        foreach (glob(MODULES_PATH . '*') as $modulePath) {
            $modules[] = new AutoloadedModule($modulePath);
        }
        return $modules;
    }


    /**
     * Get a class instance for a module, defaulting to a generic module if no specific module exists
     *
     * @param string $name Name of module to find
     * @param array $block ACF block data array
     *
     * @returns LambAgency\Modules\Module
     */
    public static function getModule($name, $block = null)
    {
        $filtered_modules = array_filter(
            self::getAllModules(), function ($m) use ($name) {
                return $m->moduleName === $name;
            }
        );
        $autoloadedModule = array_values($filtered_modules)[0];

        $moduleClassName = '\\LambAgency\\Modules\\' . $autoloadedModule->moduleClassName;

        $module = null;
        if (class_exists($moduleClassName)) {
            $module = new $moduleClassName($block);
        } else {
            $module = new \LambAgency\Modules\Module($block);
        }

        return $module;
    }

    /**
     * Autoload all the modules
     */
    private function setModules()
    {
        foreach (glob(MODULES_PATH . '*') as $modulePath) {
            $this->modules[]= new AutoloadedModule($modulePath);
        }
    }

    /**
     * Add Custom block categories to Gutenberg editor blocks
     *
     * @param     array $categories
     * @param     \WP_Post $post
     */
    public function addBlockCategories($categories, $post)
    {
      return array_merge(
        [
          [
            'slug'  => $this::$blockCategorySlug,
            'title' => __('Lamb Blocks'),
            'icon'  => 'star-filled'
          ]
        ],
        $categories
      );
    }

    /**
     * Registers blocks for ACF to hook into based on modules glob
     */
    public function registerBlocks()
    {
        if (function_exists('acf_register_block_type')) {
            foreach ($this->modules as $module) {
                $moduleClass = '\\LambAgency\\Modules\\' . $module->moduleClassName;
                // die(var_dump($module, $moduleClass));
                \acf_register_block_type(
                    [
                        'name'              => $module->moduleName,
                        'category'          => $this::$blockCategorySlug,
                        'title'             => ucfirst(str_replace('-', ' ', $module->moduleName)),
                        'render_callback'   => [$this, 'renderBlock'],
                        'keywords'          => array( $module->moduleName, 'lamb', 'module' ),
                        'mode'              => 'edit',
                        'align'             => 'full',
                        'icon'              => class_exists($moduleClass) && !empty($moduleClass::$icon) ? $moduleClass::$icon : 'block-default',
                        'supports'          => [
                          'align' => false,
                          'mode'  => false
                        ]
                    ]
                );
            }
        }
    }

    /**
     * Outputs the template for a module based on the ACF block data passed in
     */
    public function renderBlock($block)
    {
        $blockName = str_replace($this::$blockKeyPrefix, '', $block['name']);
        $module = $this::getModule($blockName, $block);

        set_query_var('module', $module);
        set_query_var('fields', $module->getFields());

        // Module template gets the module instance as $module and contained fields as $fields.
        // No other (non-global) variables / functions / constructors can be accessed inside the module template.
        get_template_part(MODULES . "/{$blockName}/{$blockName}");
    }


    /**
     * Register the additional admin pages
     */
    public function registerOptionPages()
    {
        if (function_exists('acf_add_options_page')) {
            // Theme settings pages
            acf_add_options_page(
                [
                'page_title'    => 'Theme Settings',
                'menu_title'    => 'Theme Settings',
                'menu_slug'     => 'theme-settings',
                'capability'    => 'edit_posts',
                'redirect'      => false,
                'autoload'      => true
                ]
            );

            acf_add_options_sub_page(
                [
                  'page_title'  => __('Default Blocks'),
                  'menu_title'  => __('Default Blocks'),
                  'slug'        => 'default-modules',
                  'parent_slug' => 'theme-settings',
                  'autoload'    => true
                ]
            );

            acf_add_options_sub_page(
                [
                'page_title'  => __('Block Scoping'),
                'menu_title'  => __('Block Scoping'),
                'slug'        => 'module-scoping',
                'parent_slug' => 'theme-settings',
                'autoload'    => true
                ]
            );
        }
    }

    /**
     * Return all block keys that should be allowed for a given post type
     *
     * @param     string $postType
     * @return    string[]
     */
    public static function getCustomBlocksForPostType($postType) {
        $modules = self::getAllModules();
        $excludedModules = [];

        // Loop through each module
        foreach ($modules as $module) {
            // Get the module scope options
            $fields = get_field("modules_scope_{$module->getModuleName()}", 'option');

            // Ensure the location options are set
            if (!empty($fields['locations']) && is_array($fields['locations'])) {
                // Check whether the include or exclude option has been set
                $include = $fields['type'] == 'include';

                // If we should exclude the module, will get set to true if an appropriate match is found
                $excludeModule = $include ? true : false;

                // Let's loop through the values
                foreach ($fields['locations'] as $location) {
                    // We will split the location value to get both the type and name (e.g. posttype_event)
                    $options = explode('_', $location, 2);

                    // Ensure that the value has both a type (posttype/taxonomy) and a name
                    if (is_array($options) && count($options) > 1) {
                        // We will only continue if a location option matches the current taxonomy or post type
                        if (($options[0] == 'posttype'
                            && $options[1] == $postType)
                        ) {
                            // Update the excluded state
                            $excludeModule = $include ? false : true;

                            // We have a match so let's break the loop and go the next module
                            break;
                        }
                    }
                }

                // We will add the module to the list of excluded modules if it has matched the criteria
                if ($excludeModule) {
                    $excludedModules[] = $module->moduleName;
                }
            }
        }

        $includedModules = array_filter($modules, function($module) use ($excludedModules) {
          return !in_array($module->moduleName, $excludedModules);
        });

        $includedModuleKeys = array_map(function($module) {
          return self::$blockKeyPrefix . $module->moduleName;
        }, $includedModules);

        // die(var_dump($modules));

        return array_values($includedModuleKeys);
    }

    /**
     * Restrict blocks according to scoping
     *
     * @param     bool|array $allowed_block_types
     * @param     object $post
     */
    public function initBlockScoping($allowed_block_types, $post) {

      if (empty($post)
          || $post->post_type === 'acf-field-group'
          || $post->base === 'theme-settings_page_module-scoping'
          || $post->base === 'theme-settings_page_default-modules'
      ) {
          return $allowed_block_types;
      }

      return array_merge($this::getCustomBlocksForPostType($post->post_type), $this::$includedDefaultBlocks);
    }

    /**
     * Remove core block patterns
     *
     * @param array $settings Array of block editor settings to filter out.
     *
     * @return array Filtered array.
     */
    public function removeCorePatterns(array $settings): array
    {
      $patternsToKeep = array();

      $settings['__experimentalBlockPatterns'] = array_values($patternsToKeep);

      return $settings;
    }

    /**
     * Get the locations (post type or taxonomy) that the modules are enabled on
     *
     * @return array
     */
    private function getModuleLocations()
    {
        return $this->moduleLocations;
    }


    /**
     * Set the locations that the modules are enabled on
     */
    public function setModuleLocations()
    {
        $postTypes = get_post_types([
          'public' => true,
          'show_ui' => true
        ], 'objects');

        $postTypes = array_filter($postTypes, function($postType) {
          return $postType->name !== 'attachment';
        });

        $this->moduleLocations['post-types'] = $postTypes;
    }

    /**
     * Initialise the module scoping
     */
    public function registerACFGroupModuleScoping()
    {
        $moduleLocations = $this->getModuleLocations();

        $options = [];

        // Let's add the location options for post types
        if ($moduleLocations['post-types']) {
            foreach ($moduleLocations['post-types'] as $postType) {
                /**
                 * \WP_Post_Type $postType
                 */

                $options['Post Type']["posttype_{$postType->name}"] = "{$postType->label}";
            }
        }

        // Let's add the location options for taxonomies
        if ($moduleLocations['taxonomies']) {
            foreach ($moduleLocations['taxonomies'] as $taxonomy) {
                /**
                 * \WP_Taxonomy $taxonomy
                 */

                $options['Taxonomy']["taxonomy_{$taxonomy->name}"] = "{$taxonomy->label}";
            }
        }


        // Let's create the acf field group and add it to an options page

        foreach ($this->getModules() as $module) {
            $fields[] = [
                'key'       => "field_modules_scope_{$module->getModuleName()}",
                'label'     => $module->getModuleName(true),
                'name'      => "modules_scope_{$module->getModuleName()}",
                'type'      => 'group',
                'layout'    => 'table',
                'sub_fields' => [
                    [
                        'key'       => "field_modules_scope_{$module->getModuleName()}_type",
                        'label'     => 'Type',
                        'name'      => "type",
                        'type'      => 'button_group',
                        'choices'   => [
                            'include' => 'Include for',
                            'exclude' => 'Exclude from'
                        ],
                        'allow_null'    => 0,
                        'default_value' => 'include',
                        'layout'        => 'horizontal',
                        'return_format' => 'value'
                    ],
                    [
                        'key'       => "field_modules_scope_{$module->getModuleName()}_locations",
                        'label'     => 'Locations',
                        'name'      => "locations",
                        'type'      => 'select',
                        'choices'   => $options,
                        'multiple'  => 1,
                        'ui'        => 1,
                    ]
                ]
            ];
        }

        if (!empty($fields) && function_exists('acf_add_local_field_group')) {
            acf_add_local_field_group(
                [
                    'key'       => 'group_module_scoping',
                    'title'     => 'Module Scoping',
                    'fields'    => $fields,
                    'label_placement' => 'left',
                    'location' => [
                        [
                            [
                                'param'     => 'options_page',
                                'operator'  => '==',
                                'value'     => 'module-scoping'
                            ]
                        ]
                    ]
                ]
            );
        }
    }

    /**
     * Register the custom field group for ACF module locations
     * This creates an acf section for adding the default modules for each post-type
     */
    public function registerACFGroupModuleLocations()
    {

        // Let's only run this if ACF is enabled
        if (function_exists('acf_add_local_field_group')) {
            $moduleLocations = $this->getModuleLocations();

            $fields = [];

            if ($moduleLocations['post-types']) {
                foreach ($moduleLocations['post-types'] as $postType) {
                    /** \WP_Post_Type $postType */

                    $fields[] = [
                        'key'       => "field_modules_posttype_{$postType->name}_tab",
                        'label'     => $postType->label,
                        'name'      => '',
                        'type'      => 'tab',
                        'placement' => 'left',
                        'endpoint'  => 0
                    ];

                    $fields[] = [
                        'key'           => "field_blocks_posttype_{$postType->name}",
                        'label'         => __('Blocks'),
                        'name'          => "blocks_posttype_{$postType->name}",
                        'type'          => 'repeater',
                        'display'       => 'seamless',
                        'layout'        => 'block',
                        'prefix_name'   => 1,
                        'button_label'  => __('Add Block')
                    ];

                    $blocks = self::getCustomBlocksForPostType($postType->name);



                    $choices = [];

                    foreach($blocks as $block) {
                      $choices[$block] = ucfirst(str_replace('-', ' ', str_replace(self::$blockKeyPrefix, '', $block)));
                    }

                    \acf_add_local_field([
                      'key'     => "field_blocks_posttype_{$postType->name}_block",
                      'name'    => "field_blocks_posttype_{$postType->name}_block",
                      'parent'  => "field_blocks_posttype_{$postType->name}",
                      'label'   => 'Block',
                      'type'    => 'select',
                      'choices' => $choices,
                      'ui'      => 1
                    ]);

                }
            }

            acf_add_local_field_group([
                'key'       => 'group_module_framework',
                'title'     => 'Default Blocks',
                'fields'    => $fields,
                'location' => [
                    [
                        [
                            'param'     => 'options_page',
                            'operator'  => '==',
                            'value'     => 'default-modules'
                        ]
                    ]
                ]
            ]);
        }
    }

    /**
     * Looks at the snippet folder and picks out the matching snippet template file
     *
     * @param string $name
     * @param string $key
     */
    public static function getSnippet($name = '', $key = '')
    {
        do_action('get_layout', $name);

        $templates = [];
        $name = (string)$name;

        if ($name !== '') {
            // If a key is defined look for a file with the key appended
            if ($key !== '') {
                $templates[] = SNIPPETS . "/{$name}/{$name}-{$key}.php";
            }

            // By default look for a file that matches the layout name
            $templates[] = SNIPPETS . "/{$name}/{$name}.php";
        }

        locate_template($templates, true, false);
    }

}

// Create a new instance of the module framework
ModuleFramework::getInstance();
