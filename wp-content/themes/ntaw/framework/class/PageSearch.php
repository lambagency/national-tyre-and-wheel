<?php

namespace LambAgency\Pages;

/**
 * Handles all the search page functionality.
 * Used to render alternate page modules on the search.
 * @package LambAgency\Pages
 */
class PageSearch
{

    /**
     * Get the assigned search page
     * @return mixed|null
     */
    public static function getSearchPage()
    {
        $page = get_field('page_search', 'option');
        return $page;
    }


    /**
     * Redirect the base wordpress search page to the pretty search slug /search
     */
    public static function redirectWordpressSearchPage()
    {
        global $wp_rewrite;

        if (is_search() && isset($_GET['s'])) {
            $s = sanitize_text_field($_GET['s']); // or get_query_var( 's' )
            $location  = '/';
            $location .= trailingslashit($wp_rewrite->search_base);
            $location .= !empty($s) ? urlencode($s) : '';
            $location  = home_url($location);
            wp_safe_redirect($location, 301);
            exit;
        }
    }
}

add_action('template_redirect', __NAMESPACE__ . '\PageSearch::redirectWordpressSearchPage');
