<?php

namespace LambAgency\Framework;

/**
 * Class Autoloaded Module
 *
 * This class provides a neat method of structuring an autoloaded module
 *
 * @package LambAgency\Framework
 */
class AutoloadedModule
{

    public $modulePath = '';
    public $moduleName = '';
    public $moduleTemplatePath = '';
    public $moduleACFFieldGroupKey = '';
    public $moduleClassPath = '';
    public $moduleClassName = '';


    public function __construct($modulePath)
    {
        $this->setModulePath($modulePath);
        $this->setModuleName();
        $this->setModuleTemplatePath();
        $this->setModuleClassName();
        $this->setModuleACFFieldGroupKey();

        if ($this->moduleClassPath) {
            require_once $this->moduleClassPath;
        }
    }


    /**
     * Get the module path
     *
     * @return string
     */
    public function getModulePath()
    {
        return $this->modulePath;
    }


    /**
     * Set the module path
     *
     * @param string $modulePath
     */
    public function setModulePath($modulePath = '')
    {
        $this->modulePath = $modulePath;
    }


    /**
     * Set the module name
     *
     * @param bool $pretty
     *
     * @return string
     */
    public function getModuleName($pretty = false)
    {
        if ($pretty) {
            return ucwords(str_replace('-', ' ', $this->moduleName));
        } else {
            return $this->moduleName;
        }
    }


    /**
     * Get the module name
     */
    public function setModuleName()
    {
        $this->moduleName = basename($this->getModulePath());
    }


    /**
     * Get the module template path
     *
     * @return string
     */
    public function getModuleTemplatePath()
    {
        return $this->moduleTemplatePath;
    }


    /**
     * Set the module template path if a template file is found
     */
    public function setModuleTemplatePath()
    {
        $moduleTemplatePath = $this->getModulePath() . "/{$this->getModuleName()}.php";

        // Ensure the template file exists
        if (file_exists($moduleTemplatePath)) {
            $this->moduleTemplatePath = $moduleTemplatePath;
        }
    }


    /**
     * Get the Module Class name
     *
     * @return string
     */
    public function getModuleClassName()
    {
        return $this->moduleClassName;
    }


    /**
     * Set the Module Class name
     */
    public function setModuleClassName()
    {

        $moduleClasses = glob($this->getModulePath() . "/Module*.php");

        if ($moduleClasses && count($moduleClasses) > 0) {
            $this->moduleClassPath = $moduleClasses[0];
            $pathParts = pathinfo($this->moduleClassPath);
            $this->moduleClassName = $pathParts['filename'];
        }
    }


    /**
     * Get the module ACF field group key
     *
     * @return string
     */
    public function getModuleACFFieldGroupKey()
    {
        return $this->moduleACFFieldGroupKey;
    }


    /**
     * Set the Module Field Group Key
     */
    public function setModuleACFFieldGroupKey()
    {

        $jsonGroups = glob($this->getModulePath() . "/group*.json");

        if ($jsonGroups && count($jsonGroups) > 0) {
            $pathParts = pathinfo($jsonGroups[0]);
            $this->moduleACFFieldGroupKey = $pathParts['filename'];
        }
    }


    /**
     * Check if the module has an ACF field group
     *
     * @return bool
     */
    public function hasModuleACFFieldGroupKey()
    {
        return $this->moduleACFFieldGroupKey != '';
    }
}
