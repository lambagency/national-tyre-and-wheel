<?php

namespace LambAgency\Template;

use \LightnCandy\LightnCandy;

/**
 * Class Template
 *
 * @package LambAgency\Template
 */
abstract class Template {

    /** @var string The name of the template */
    public static $name = '';

    /** @var string The template name */
    public $template = '';

    /**
     * Path to handlebars template from templates directory
     * Exclude leading slash
     *
     * @var string
     *
     * @example
     * post/post.hbs
     */
    public $templatePath = '';

    /** @var array Holds the post data */
    public $postData = [];

    /** @var array Holds the post data to be used in the handlebars template */
    public $templateData = [];

    /** @var \WP_Post */
    public $post;

    /** @var int The index of the post */
    public $index = 0;

    /** @var array Post types which template is available for */
    public static $availablePostTypes = [];


    public function __construct($post)
    {
        $this->setPost($post);
        $this->setTemplate();
        $this->setTemplateData();
        $this->setPostData();
    }


    /**
     * Get the post
     *
     * @return \WP_Post
     */
    public function getPost()
    {
        return $this->post;
    }


    /**
     * Set the post
     *
     * @param $post \WP_Post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }


    /**
     * Set the template name
     */
    public function setTemplate()
    {
        $this->template = pathinfo($this->templatePath)['filename'];
    }


    /**
     * Get the index
     *
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }


    /**
     * Set the index
     *
     * @param int $index
     */
    public function setIndex($index)
    {
        $this->index = (int)$index;
    }


    /**
     * Set the post data
     */
    public function setPostData()
    {
        $this->postData = [
            'template'      => $this->template,
            'templateData'  => $this->getTemplateData()
        ];
    }


    /**
     * Get the post Data
     *
     * @return array
     */
    public function getPostData()
    {
        return $this->postData;
    }


    /**
     * Set the template data
     */
    public function setTemplateData()
    {
        // Override in extended class to populate
    }


    /**
     * Get the template Data
     *
     * @return array
     */
    public function getTemplateData()
    {
        return $this->templateData;
    }



    /**
     * Render the Handlebars template out as html
     *
     * @return mixed
     */
    public function getTemplateHTML()
    {
        $cacheKey = 'template-' . $this->templatePath . '-modified';

        $templatePath = TEMPLATES_PATH . trim($this->templatePath, '/');
        $templateModified = (string)filemtime($templatePath);
        $previousModified = get_transient($cacheKey);

        $renderPath = INTERFACE_BUILD_PATH . 'templates/';
        $renderPath .= trim(preg_replace('/\/|.hbs/i', '-', $this->templatePath), '-') . '.php';

        if (!is_dir(dirname($renderPath))) {
            mkdir(dirname($renderPath), 0777, true);
        }

        clearstatcache();
        if (!file_exists($renderPath) || (null !== APP_ENV && APP_ENV === 'local') || $templateModified !== $previousModified) {
            $phpStr = LightnCandy::compile(file_get_contents($templatePath), [
                'flags' => LightnCandy::FLAG_ELSE
            ]);  // set compiled PHP code into $phpStr
            file_put_contents($renderPath, '<?php ' . $phpStr . '?>');

            set_transient($cacheKey, (int)$templateModified);
        }

        // Get the render function from the php file
        $renderer = include($renderPath);

        $data = $renderer($this->getTemplateData());

        return $data;
    }
}
