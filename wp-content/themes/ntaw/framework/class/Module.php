<?php

namespace LambAgency\Modules;

/**
 * Class Module
 * Implements no additional class functionality for a more FP approach.
 *
 * @package LambAgency\Modules
 */
class Module extends AbstractModule
{
    public function __construct($block)
    {
        parent::__construct($block);
    }
}
