<?php

namespace LambAgency;

/**
 * Class Utility
 * This class contains static utility functions
 *
 * @package LambAgency
 */
abstract class Utility
{

    /**
     * Get a link from the links repeater in the config page by unique name.
     * Links are defined in the links repeater on the config page.
     *
     * @param $key
     *
     * @return string
     */
    public static function getLink($key)
    {
        $linkURL = '';

        if (!empty($key)) {
            $links = get_field('links', 'option') ? get_field('links', 'option') : [];

            foreach ($links as $link) {
                if ($link['key'] == $key) {
                    $linkURL = $link['link'];
                    break;
                }
            }
        }

        return $linkURL;
    }


    /**
     * Get the anti-spam formatted email address
     *
     * @param string $emailAddress
     *
     * @return string
     */

    public function getEmail($emailAddress = '')
    {
        return antispambot($emailAddress);
    }


    /**
     * Get the correctly formatted email link for an email address
     * Adds anti-spam formatting
     *
     * @param string $emailAddress
     *
     * @return string
     */
    public function getEmailURL($emailAddress = '')
    {
        return 'mailto:' . Utility::getEmail($emailAddress);
    }


    /**
     * Get the post featured image in an array
     * Follows the same return format as a standard ACF image
     * @param string $postID
     * @return array
     */
    public static function getFeaturedImageArray($postID = '')
    {
        $imageArray = [];

        if (empty($postID)) {
            return $imageArray;
        }

        if ($postID instanceof \WP_Post) {
            $postID = $postID->ID;
        }

        // Get all image sizes
        $imageSizes = wp_get_additional_image_sizes();

        // Loop through each size and assign url to image array
        foreach ($imageSizes as $imageSize => $attributes) {
            $featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id($postID), $imageSize);

            if ($featuredImage) {
                $imageArray['sizes'][$imageSize] = $featuredImage[0];
                $imageArray['sizes'][$imageSize . '-width'] = $featuredImage[1];
                $imageArray['sizes'][$imageSize . '-height'] = $featuredImage[2];
            }
        }

        return $imageArray;
    }


    /**
     * Get the Google directions url for a specified address
     *
     * @param string $address The destination address
     *
     * @return string
     */
    public static function getGoogleDirectionsURL($address = '')
    {
        return 'https://maps.google.com?q=' . urlencode($address);
    }


    /**
     * Get the Google Map API Key
     *
     * @return string
     */
    public static function getGoogleMapKey()
    {
        $mapKey = get_field('google_map_api_key', 'option');

        return $mapKey ?: '';
    }


    /**
     * Get the correctly formatted tel link for a phone number
     * Removes illegal characters, spaces and appends a tel:
     *
     * @param string $phoneNumber
     *
     * @return string
     */
    public static function getPhoneURL($phoneNumber = '')
    {
        return 'tel:' . preg_replace('/[^0-9]/', '', $phoneNumber);
    }
}
