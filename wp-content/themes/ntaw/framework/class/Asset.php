<?php
/**
 * Class Asset
 *
 * This class handles enqueueing of front-end assets built with Webpack.
 *
 * By using a manifest-based loader, we can use the same interface for loading
 * locally-hosted webpack-dev-server assets and also production built assets.
 *
 */

namespace LambAgency\Helpers;

use LambAgency\Framework\ModuleFramework;

class Asset {

	static private $asset_manifest;

	/**
	 * @return stdClass
	 */
	static private function get_asset_manifest() {
		try {
			if ( ! isset( self::$asset_manifest ) ) {
				self::$asset_manifest = \json_decode( file_get_contents( INTERFACE_BUILD_PATH . 'manifest.json' ) );
			}
		} catch ( \Exception $e ) {
			if ( true === WP_DEBUG ) {

				error_log( print_r( $e, true ) ); // phpcs:ignore
			}
		}

		return self::$asset_manifest;
	}

	/**
	 * @return bool
	 */
	static function has_asset( $asset ) {
		$asset_manifest = self::get_asset_manifest();

		return ! empty( $asset_manifest->{$asset} );
	}


	/**
	 * Finds hashed asset in manifest and strips WordPress path
	 * (because it's added by enqueue_script/style)
	 * @return string
	 */
	static function get_asset( $asset ) {
		$asset_manifest = self::get_asset_manifest();

		$asset_path_parts = explode( get_template(), $asset_manifest->{$asset} );

		return strpos( $asset_path_parts[0], 'http' ) > -1 ? $asset_manifest->{$asset} : get_theme_file_uri( $asset_path_parts[1] );
	}

	/**
	 * Fetches blocks within the page and creates preload tags for each
	 */
	public static function preload_page_blocks() {
		$block_prefix_length = strlen(ModuleFramework::$blockKeyPrefix);
		$_post = get_post();

		if ( isset($_post) && has_blocks($_post)) {

			$blocks = array_map(function($block){
				return $block['blockName'];
			}, parse_blocks($_post->post_content));
			$blocks = array_unique($blocks);

			$blocks = array_filter(
				$blocks,
				function($block_name) use ($block_prefix_length) {
					$block_prefix = substr($block_name, 0, $block_prefix_length);

					return $block_name !== null && $block_prefix === ModuleFramework::$blockKeyPrefix;

			});

			foreach($blocks as $block) {
				self::preload_block($block);
			}

		}
	}

	/**
	 * Gets the appropriate strings to find assets in the manifest based on their slug
	 *
	 * @param string $block_name
	 * @param string $asset_type One of "module", "component" or "snippet"
	 *
	 * @return stdClass
	 */
	public static function asset_name($block_name, $asset_type = 'module') {
		if ($block_name === 'core') {
			$asset = new \stdClass();
			$asset->js = 'production.js';
			$asset->css = 'style.css';
			return $asset;
		}

		$block_prefix_length = strlen(ModuleFramework::$blockKeyPrefix);
		$unprefixed = substr($block_name, $block_prefix_length);

		$asset = new \stdClass();
		$asset->js  = "$asset_type-$unprefixed-$unprefixed.js";
		$asset->css = "$asset_type-$unprefixed-$unprefixed-scss-styles.css";
		return $asset;
	}

	/**
	 * Generates a preload link for a given asset slug and type
	 *
	 * @param string $asset_slug
	 * @param string $asset_type One of "module", "component" or "snippet"
	 */
	public static function preload_block($asset_slug, $asset_type = 'module') {
		$assets = self::asset_name($asset_slug, $asset_type);
		if (self::has_asset($assets->js)) {
			$js = self::get_asset($assets->js);
			echo "<link rel='preload' href='$js' as='script'>\n";
		}
	}
}
