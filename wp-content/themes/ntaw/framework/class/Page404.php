<?php

namespace LambAgency\Pages;

/**
 * Handles all the 404 page functionality.
 * Used to render alternate page modules on the 404.
 * @package LambAgency\Pages
 */
class Page404
{

    /**
     * Get the assigned 404 page
     * @return mixed|null
     */
    public static function get404Page()
    {
        $page = get_field('page_404', 'option');
        return $page;
    }
}
