<?php

namespace LambAgency\Taxonomy;

/**
 * This class is used to remove taxonomy base slugs
 *
 * Class TaxonomyRemoveBase
 * @package LambAgency\Taxonomy
 */
class TaxonomyRemoveBase
{

    /**
     * Singleton instance
     *
     * @var null
     */
    private static $instance = null;

    /**
     * Taxonomies to remove base slug
     *
     * @return array
     */
    protected $taxonomies = [
        // 'sample-taxonomy'
    ];


    /**
     * TaxonomyRemoveBase constructor.
     */
    public function __construct()
    {
        if (count($this->taxonomies)) {
            add_action('init', [$this, 'removeTaxonomyBaseSlug'], 11);
        }
    }


    /**
     * Creates or returns an Singleton instance of this class.
     *
     * @return TaxonomyRemoveBase single instance of this class.
     */
    public static function getInstance()
    {
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    /**
     * The site's post types
     *
     * @return array
     */
    public function sitePostTypes()
    {
        $args = [
            'public'   => true,
            '_builtin' => false
        ];

        return get_post_types($args);
    }


    /**
     * Main function to start the process of removing the base slug from a taxonomy
     */
    public function removeTaxonomyBaseSlug()
    {
        global $wp_rewrite;

        $postTypes  = $this->sitePostTypes();

        // If not empty
        if (!empty($postTypes)) {
            foreach ($postTypes as $postType) {
                // Change custom post type rules
                add_filter($postType . '_rewrite_rules', 'removePostTypeFilter');
            }
        }

        // If not empty
        if (!empty($this->taxonomies)) {
            $globalTax = '';

            foreach ($this->taxonomies as $taxonomy) {
                $globalTax = $taxonomy;

                $blogBase = '';
                if (function_exists('is_multisite') && is_multisite() && !is_subdomain_install() && is_main_site()) {
                    $blogBase = 'blog/';
                }

                // Permastructs
                $wp_rewrite->extra_permastructs[$taxonomy]['struct'] = $blogBase . '%' . $taxonomy . '%';

                // Add our custom category rewrite rules
                add_filter($taxonomy . '_rewrite_rules', 'rewriteTaxonomyFilter');

                // Taxonomy created
                add_action('created_' . $taxonomy, 'flush_rewrite_rules');

                // Taxonomy edited
                add_action('edited_' . $taxonomy, 'flush_rewrite_rules');

                // Taxonomy Deleted
                add_action('delete_' . $taxonomy, 'flush_rewrite_rules');
            }
        }

        // If not empty
        if (!empty($taxonomies)) {
            if (!empty($globalTax)) {
                add_filter($taxonomy . '_rewrite_rules', 'insertPostTypeFilter');
            }
        }

        add_filter('rewrite_rules_array', 'removePostTypeBaseFilter');
    }


    /**
     * If the slug of the terms is the same as the slug of a post type then remove all rules of the post type
     *
     * @param $postTypeRewrite
     *
     * @return array
     */
    public function removePostTypeFilter($postTypeRewrite)
    {
        $count = 0;
        $valueWeNeed = '';

        foreach ($postTypeRewrite as $oneValue) {
            $count = $count + 1;
            if ($count == 6) {
                $valueWeNeed = $oneValue;
            }
        }

        preg_match('/\x{003F}(.*?)=/', $valueWeNeed, $postTypeArr);
        $postType = $postTypeArr[1];

        foreach ($this->taxonomies as $taxonomy) {
            $terms = get_terms($taxonomy, ['hide_empty' => false]);

            foreach ($terms as $term) {
                $postTypeObject = get_post_type_object($postType);

                if ($postTypeObject->rewrite['slug'] == $term->slug) {
                    $postTypeRewrite = [];
                }
            }
        }

        return $postTypeRewrite;
    }


    /**
     * Main function of the plugin to modify the rules
     *
     * @param $termRewrite
     *
     * @return array
     */
    public function rewriteTaxonomyFilter($termRewrite)
    {
        foreach ($termRewrite as $oneValue) {
            preg_match('#index.php.(?!attachment)(.*?)=.matches#', $oneValue, $taxonomyArr);

            if (!empty($taxonomyArr[1])) {
                $taxonomyName = $taxonomyArr[1];
            }
        }

        if (!empty($taxonomyName)) {
            $args = [
                'public'  => true,
                'show_ui' => true
            ];

            $taxonomies = get_taxonomies($args);

            foreach ($taxonomies as $tax) {
                $taxObj = get_taxonomy($tax);

                if ($taxonomyName == $taxObj->query_var) {
                    $taxonomy = $tax;
                }
            }

            $termRewrite = [];
            $terms = get_terms($taxonomy, ['hide_empty' => false]);

            $getTaxonomy = get_taxonomy($taxonomy);

            $hierarchical = $getTaxonomy->rewrite['hierarchical'];

            $blogBase = '';
            if (function_exists('is_multisite') && is_multisite() && !is_subdomain_install() && is_main_site()) {
                $blogBase = 'blog/';
            }

            foreach ($terms as $term) {
                $termNicename = $term->slug;

                if ($term->parent != 0 and $hierarchical) {
                    $termNicename = $this->getTermParents2($term->parent, $taxonomy, false, '/', true) . $termNicename;
                }

                $termRewrite[$blogBase . '(' . $termNicename . ')/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?' . $taxonomyName . '=$matches[1]&feed=$matches[2]';
                $termRewrite[$blogBase . '(' . $termNicename . ')/page/?([0-9]{1,})/?$'] = 'index.php?' . $taxonomyName . '=$matches[1]&paged=$matches[2]';
                $termRewrite[$blogBase . '(' . $termNicename . ')/?$'] = 'index.php?' . $taxonomyName . '=$matches[1]';
            }
        }

        return $termRewrite;
    }

    // Function for inserting the post type rules, below the tax rules
    public function insertPostTypeFilter($rewriteRules)
    {
        $postTypes  = $this->sitePostTypes();

        foreach ($this->taxonomies as $taxonomy) {
            $terms = get_terms($taxonomy, ['hide_empty' => false]);

            foreach ($terms as $term) {
                $termSlug = $term->slug;

                foreach ($postTypes as $postType) {
                    $postTypeObject = get_post_type_object($postType);
                    $postTypeObjectSlug = $postTypeObject->rewrite['slug'];

                    if ($postTypeObjectSlug == $termSlug) {
                        $rewriteRules[$postTypeObjectSlug . '/[^/]+/attachment/([^/]+)/?$'] = 'index.php?attachment=$matches[1]';
                        $rewriteRules[$postTypeObjectSlug . '/[^/]+/attachment/([^/]+)/trackback/?$'] = 'index.php?attachment=$matches[1]&tb=1';
                        $rewriteRules[$postTypeObjectSlug . '/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?attachment=$matches[1]&feed=$matches[2]';
                        $rewriteRules[$postTypeObjectSlug . '/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?attachment=$matches[1]&feed=$matches[2]';
                        $rewriteRules[$postTypeObjectSlug . '/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$'] = 'index.php?attachment=$matches[1]&cpage=$matches[2]';
                        $rewriteRules[$postTypeObjectSlug . '/([^/]+)/trackback/?$'] = 'index.php?' . $postType . '=$matches[1]&tb=1';
                        $rewriteRules[$postTypeObjectSlug . '/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?' . $postType . '=$matches[1]&feed=$matches[2]';
                        $rewriteRules[$postTypeObjectSlug . '/([^/]+)/(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?' . $postType . '=$matches[1]&feed=$matches[2]';
                        $rewriteRules[$postTypeObjectSlug . '/([^/]+)/page/?([0-9]{1,})/?$'] = 'index.php?' . $postType . '=$matches[1]&paged=$matches[2]';
                        $rewriteRules[$postTypeObjectSlug . '/([^/]+)/comment-page-([0-9]{1,})/?$'] = 'index.php?' . $postType . '=$matches[1]&cpage=$matches[2]';
                        $rewriteRules[$postTypeObjectSlug . '/([^/]+)(/[0-9]+)?/?$'] = 'index.php?' . $postType . '=$matches[1]&page=$matches[2]';
                        $rewriteRules[$postTypeObjectSlug . '/[^/]+/([^/]+)/?$'] = 'index.php?attachment=$matches[1]';
                        $rewriteRules[$postTypeObjectSlug . '/[^/]+/([^/]+)/trackback/?$'] = 'index.php?attachment=$matches[1]&tb=1';
                        $rewriteRules[$postTypeObjectSlug . '/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?attachment=$matches[1]&feed=$matches[2]';
                        $rewriteRules[$postTypeObjectSlug . '/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?attachment=$matches[1]&feed=$matches[2]';
                        $rewriteRules[$postTypeObjectSlug . '/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$'] = 'index.php?attachment=$matches[1]&cpage=$matches[2]';
                    }
                }
            }
        }

        return $rewriteRules;
    }

    // Remove post type slugs if needed
    public function removePostTypeBaseFilter($rewriteRules)
    {
        $postTypes  = $this->sitePostTypes();

        // If not empty
        if (!empty($this->taxonomies)) {
            foreach ($this->taxonomies as $taxonomy) {
                $terms = get_terms($taxonomy, ['hide_empty' => false]);

                foreach ($terms as $term) {
                    $termSlug = $term->slug;

                    // If not empty
                    if (!empty($postTypes)) {
                        foreach ($postTypes as $postType) {
                            $postTypeObject = get_post_type_object($postType);
                            $postTypeObjectSlug = $postTypeObject->rewrite['slug'];

                            if ($postTypeObjectSlug == $termSlug) {
                                unset($rewriteRules[$postTypeObjectSlug . '/?$']);
                                unset($rewriteRules[$postTypeObjectSlug . '/feed/(feed|rdf|rss|rss2|atom)/?$']);
                                unset($rewriteRules[$postTypeObjectSlug . '/(feed|rdf|rss|rss2|atom)/?$']);
                                unset($rewriteRules[$postTypeObjectSlug . '/page/([0-9]{1,})/?$']);
                            }
                        }
                    }
                }
            }
        }

        return $rewriteRules;
    }


    /**
     * Function that should be in WordPress core (apparently!)
     *
     * @param        $id
     * @param        $taxonomy
     * @param bool   $link
     * @param string $separator
     * @param bool   $nicename
     * @param array  $deprecated
     *
     * @return string
     */
    public function getTermParents2(
        $id,
        $taxonomy,
        $link = false,
        $separator = '/',
        $nicename = false,
        $deprecated = []
    ) {
        $chain = '';

        $parents = get_ancestors($id, $taxonomy);

        array_unshift($parents, $id);

        foreach (array_reverse($parents) as $term_id) {
            $term = get_term($term_id, $taxonomy);

            $name = $nicename ? $term->slug : $term->name;
            if ($link) {
                $chain .= '<a href="' . get_term_link($term->slug, $taxonomy) . '" title="' . esc_attr(sprintf(__("View all posts in %s"), $term->name)) . '">' . $name . '</a>' . $separator;
            } else {
                $chain .= $name . $separator;
            }
        }

        return $chain;
    }
}

TaxonomyRemoveBase::getInstance();
