<?php

namespace LambAgency\Components;

/**
 * Class Component
 *
 * @package LambAgency\Components
 */
abstract class Component
{

    public function __construct()
    {
    }
}
