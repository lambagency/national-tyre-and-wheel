<?php

namespace LambAgency\Modules;

/**
 * Class AbstractModule
 *
 * @package LambAgency\Modules
 */
abstract class AbstractModule
{
    protected $name;
    protected $type;
    protected $block = [];
    protected $fields = [];
    protected $moduleClass = [];

    /**
     * Default module theme
     *
     * @var string
     */
    public $defaultModuleTheme = 'theme-default';


    /**
     * Module constructor.
     */
    public function __construct($block = null)
    {
        $this->setBlock($block);
        $this->setFields();
        $this->setModuleClasses();
        $this->setLayoutIndex();
    }

    private function setBlock($block)
    {
        $this->block = $block;
    }


    /**
     * Get the module name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set the module name
     *
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * Get all the fields assigned to the module
     *
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }


    /**
     * Dynamically set module fields array based on the current module
     */
    public function setFields()
    {
        if (!empty($this->block)) {
            $this->setName(str_replace('acf/', '', $this->block['name']));
        }

        $this->fields = get_fields();
    }


    /**
     * Returns the module's classes in string format
     *
     * @param array|string $addClasses
     *
     * @return string
     */
    public function getModuleClass($addClasses = '')
    {
        if (is_array($addClasses)) {
            $addClasses = implode(' ', $addClasses);
        }

        return trim(implode(' ', $this->moduleClass) . ' ' . $addClasses);
    }


    /**
     * Sets's the modules CSS classes
     */
    public function setModuleClasses()
    {
        // Set base module classes
        $this->addModuleClass('module');
        $this->addModuleClass('mod-' . $this->name);

        // Set module theme class
        if (!empty($this->fields['module_theme'])) {
            $this->addModuleClass($this->fields['module_theme']);
        } else {
            $this->addModuleClass($this->defaultModuleTheme);
        }
    }


    /**
     * Add single class
     *
     * @param string $class
     */
    public function addModuleClass($class = '')
    {
        if (is_array($class)) {
            foreach ($class as $singleClass) {
                $this->moduleClass[] = $singleClass;
            }
        } else {
            $this->moduleClass[] = $class;
        }
    }


    /**
     * Returns the current layout index
     *
     * @return mixed
     */
    public function getLayoutIndex()
    {
        global $layoutIndex;

        return $layoutIndex;
    }


    /**
     * Set the layoutindex to 0 otherwise increment by 1 if already set
     */
    protected function setLayoutIndex()
    {
        global $layoutIndex;

        $layoutIndex = !is_null($layoutIndex) && ($layoutIndex >= 0) ? ++$layoutIndex : 0;
    }


    /**
     * Return if is first module on page
     *
     * @return bool
     */
    public function isFirstLayout()
    {
        global $layoutIndex;

        return $layoutIndex == 0;
    }
}
