<?php

namespace LambAgency\PostType;

/**
 * Class PostType
 *
 * @package LambAgency\PostType
 */
abstract class PostType
{
    public static $postType = '';
    public static $singularName = '';
    public static $pluralName = '';

    protected $postID;
    public $title;
    protected $featuredImage;
    protected $archiveLink;
    protected $fields;
    protected $author;

    public function __construct($postID = 0)
    {
        if ($postID instanceof \WP_Post) {
            $this->postID = $postID->ID;
        } else {
            $this->postID = $postID ? $postID : get_the_ID();
        }

        $this->title        = get_the_title($this->postID);
        $this->fields       = get_fields($this->postID);
        $this->author       = get_the_author();
        $this->setFeaturedImage();
    }

    /**
     * Get the post ID
     *
     * @return false|int
     */
    public function getPostID()
    {
        return $this->postID;
    }


    /**
     * Set the post ID
     *
     * @param false|int $postID
     */
    public function setPostID($postID)
    {
        $this->postID = $postID;
    }


    /**
     * Get the title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Set the author
     *
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * Get the author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }


    /**
     * Get the feature image
     *
     * @param string $size
     * @param bool $returnArray
     * @param bool $placeholder
     * @param bool $allSizes
     * @return array|string
     */
    public function getFeaturedImage(
        $size = 'square-medium',
        $returnArray = false,
        $placeholder = true,
        $allSizes = false
    ) {
        if ($returnArray) {
            if ($this->featuredImage) {
                if ($allSizes) {
                    return $this->featuredImage;
                } else {
                    return [
                        'url'       => $this->featuredImage['sizes'][$size],
                        'width'     => $this->featuredImage['sizes'][$size . '-width'],
                        'height'    => $this->featuredImage['sizes'][$size . '-height']
                    ];
                }
            } else {
                if ($placeholder) {
                    return [
                        'url'       => 'https://via.placeholder.com/600x450?text=.',
                        'width'     => 600,
                        'height'    => 400
                    ];
                }

                return [];
            }

        }
        return $this->featuredImage ? $this->featuredImage['sizes'][$size] : '';
    }

    /**
     * Set the feature image
     */
    public function setFeaturedImage()
    {
        $featuredImage = \LambAgency\Utility::getFeaturedImageArray(get_post($this->postID));
        $this->featuredImage = $featuredImage ? $featuredImage : '';
    }


    /**
     * Get the fields
     *
     * @return array|bool
     */
    public function getFields()
    {
        return $this->fields;
    }


    /**
     * Set the fields
     *
     * @param array|bool $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }


    /**
     * Get the post permalink
     *
     * @return false|string
     */
    public function getPermalink()
    {
        return get_permalink($this->getPostID());
    }
}
