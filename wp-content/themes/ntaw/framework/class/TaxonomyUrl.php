<?php


namespace LambAgency\Taxonomy;

/**
 * To use this file, all you need to do is assign the associative array for $mappedPostTypesToTaxonomies.
 *
 * @example:
 *
 *         protected $mappedPostTypesToTaxonomies = [
 *              'taxonomy-base' => [
 *                  'post-type' => [
 *                      'taxonomy',
 *                      'taxonomy2'   // optional: if you want something like /products/type/brand/product
 *                  ]
 *              ]
 *         ];
 *
 *
 * Class TaxonomyUrl
 * @package LambAgency\Taxonomy
 */
class TaxonomyUrl
{
    /**
     * Singleton instance
     *
     * @var null
     */
    private static $instance = null;


    // TODO - remove taxonomies
    /**
     * @var array - Associative array defining mapping between custom post types and their taxonomy
     * BASE SLUG => POST_TYPE => TAXONOMY_ID
     */
    protected $mappedPostTypesToTaxonomies = [
//        'apsa-nominees-winners' => [
//            'nomination' => [
//                'apsa-year',
//                'nomination-category'
//            ]
//        ]
    ];


    /**
     * This is map between resource-request and post-types
     * This is assigned within the constructor as long as $mappedPostTypesToTaxonomies is not empty
     *
     * @var array - Associative array [taxonomy slug] => [custom post type name]
     */
    protected $queriableCustomPostTypes;


    /**
     * This is a map between resource-request and taxonomy-types
     * This is assigned within the constructor as long as $mappedPostTypesToTaxonomies is not empty
     *
     * @var array - Associative array [taxonomy slug] => [taxonomy name]
     */
    // TODO - remove taxonomies
    protected $queriableTaxonomies;


    /**
     * This configures whether to make the URL hierarchical
     *
     * @example if set to true: /shop/category/sub-category/product
     * @example if set to false: /shop/sub-category/product
     *
     * @var bool
     */
    protected $taxonomyURLHierarchical = true;


    /**
     * TaxonomyUrl constructor.
     */
    public function __construct()
    {
        // TODO - remove taxonomies
        if (is_array($this->mappedPostTypesToTaxonomies) && !empty($this->mappedPostTypesToTaxonomies)) {

            /**
             * Assigns the custom post types and taxonomies which we want to query
             *
             * @var string $postType
             * @var string $taxonomy
             */
            // TODO - remove taxonomies
            foreach ($this->mappedPostTypesToTaxonomies as $base => $mappedPostTypes) {
                foreach ($mappedPostTypes as $postType => $taxonomy) {
                    $this->queriableCustomPostTypes[$base] = $postType;
                    $this->queriableTaxonomies[$base] = $taxonomy;
                }
            }

            add_filter('post_type_link', [$this, 'filterPostTypeLink'], 10, 3);
            add_action('parse_request', [$this, 'changePostPerPage']);
        }
    }


    /**
     * Creates or returns an Singleton instance of this class.
     *
     * @return TaxonomyUrl single instance of this class.
     */
    public static function getInstance()
    {
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }


    /**
     * Specifies the taxonomy for the post type
     *
     * @param string   $link
     * @param \WP_Post $post
     *
     * @return mixed
     */
    public function filterPostTypeLink($link, \WP_Post $post)
    {
        foreach ($this->mappedPostTypesToTaxonomies as $base => $mappedPostTypes) {

            // TODO - remove $taxonomy
            foreach ($mappedPostTypes as $postType => $taxonomy) {

                // make sure we're dealing with the right post type
                if ($post->post_type === $postType) {
                    $link = $this->taxonomyStringReplacements($link, $post);
                }
            }
        }

        return $link;
    }


    /**
     * Replace all taxonomy wildcards with actual taxonomy terms in supplied URL
     *
     * @param string $url
     * @param \WP_Post $post
     *
     * @return mixed
     */
    private function taxonomyStringReplacements($url, $post)
    {
        if (preg_match_all('/\%[a-z-]*%/', $url, $matches)) {

            if ($matches) {
                foreach ($matches[0] as $tax) {
                    $taxTerm = str_replace('%', '', $tax);

                    $terms = wp_get_object_terms($post->ID, $taxTerm);

                    if (!is_wp_error($terms) && $terms) {
                        $term = $terms[0]->slug;
                    }
                    else {
                        $term = '';
                    }

                    $url = str_replace($tax, $term, $url);
                }
            }
        }

        return $url;
    }



    /**
     * This function reevaluates routing, and only allows access to the resources when certain urls are entered.
     *
     * @param \WP_Query $query
     *
     * @return \WP_Query
     */
    public function changePostPerPage($query)
    {
        // only spring into action if we have an actual request to work on
        if (property_exists($query, 'request')) {

            $url = [];

            foreach ($this->queriableCustomPostTypes as $postBase => $postType) {
                if (substr($query->request, 0, strlen($postBase)) === $postBase) {
                    $urlNoBase = str_replace($postBase, '', $query->request);
                    $urlNoBase = ltrim($urlNoBase , '/');
                    $url = $urlNoBase ? explode('/', $urlNoBase) : [];
                    array_unshift($url, $postBase);
                }
            }

            // smaller urls will have to be handled by WP itself
            if (count($url) >= 2) {
                $backupUrl = $url;
                $postBase = array_shift($url);
                $postSlug = array_pop($url);

                // The resource type needs to be in the array of allowed categories otherwise we do work on the url
                if (array_key_exists($postBase, $this->queriableTaxonomies) && array_key_exists($postBase, $this->queriableCustomPostTypes)) {

                    $validUrl = $this->isValidUrl($backupUrl);

                    // we only need to handle the path if everything is correct and declare a 404 otherwise
                    if ($validUrl['valid']) {
                        $query = $this->handlePath($query, $postSlug, $this->queriableTaxonomies[$postBase], $this->queriableCustomPostTypes[$postBase], $validUrl['taxonomy']);
                    } else {
                        $query->query_vars = ['error' => 404];
                    }
                }
            }
        }

        return $query;
    }


    /**
     * This method makes sure that the url entered, is made up of an appropriate hierarchy,
     * because WP only concerns itself with the last url pattern to look after.
     * This method distinguishes between the requested resource being a taxonomy or a post
     * and takes that into account as well.
     *
     * For posts, all assigned taxonomies are considered in regards to the validation.
     *
     * @param array $url - the url we need to validate
     *
     * @return array
     */
    private function isValidUrl(array $url)
    {
        /** \wpdb $wpdb */
        global $wpdb;

        $backupURL = $url;
        $postBase = array_shift($url);
        $postSlug = array_pop($url);
        $pathIsValid = true;

        $posts = $wpdb->get_results(sprintf(
            'SELECT *
            FROM %s
            WHERE LOWER(post_name) like "%s"',
            $wpdb->posts,
            esc_sql($postSlug)
        ));

        $postID = null;

        if ($posts) {
            // Find matching post that has the correct base slug
            foreach ($posts as $post) {
                $postObject = get_post_type_object($post->post_type);

                $postObjectSlug = $this->taxonomyStringReplacements($postObject->rewrite['slug'], $post);

                if ($postObjectSlug == $postBase) {
                    $postID = $post->ID;
                    break;
                }
            }
        }


        $baseTaxonomyTerms = [];

        // If page is not a post page
        if (is_null($postID)) {

            // a taxonomy is requested
            $taxonomies = $wpdb->get_results(sprintf(
                'SELECT term.*, tax.taxonomy
                FROM %s term
                INNER JOIN %s tax ON term.term_id = tax.term_id
                WHERE LOWER(slug) LIKE "%s"',
                $wpdb->terms,
                $wpdb->term_taxonomy,
                esc_sql($postSlug)
            ));

            if ($taxonomies) {
                foreach ($taxonomies as $taxonomy) {

                    $index = array_search($taxonomy->taxonomy, $this->queriableTaxonomies[$postBase]);

                    $baseTaxonomyTermObject = get_term($taxonomy->term_id, $this->queriableTaxonomies[$postBase][$index]);

                    if ($baseTaxonomyTermObject) {
                        $baseTaxonomyTerms[] = $baseTaxonomyTermObject;
                        break;
                    }
                }
            }

            // If page is a post page
        } else {
            // Remove product slug from url array
            array_pop($backupURL);

            // Get all posts assigned taxonomies
            $baseTaxonomyTerms = wp_get_post_terms($postID, $this->queriableTaxonomies[$postBase]);
        }


        $currentTaxonomy = null;

        if ($baseTaxonomyTerms) {

            // Remove base slug from url array
            array_shift($backupURL);

            // it is easier to validate parents of taxonomies than the children, so we walk backwards
            $reverseUrl = array_reverse($backupURL);

            // Invalidate if base taxonomies set but url has not taxonomy slugs
            if (count($reverseUrl) == 0) {
                $pathIsValid = false;
            }


            // Iterate through current URL slugs in reverse order
            foreach ($reverseUrl as $index => $taxonomyTermSlug) {

                // For first taxonomy slug check if it's included in the base taxonomy terms
                if ($index == 0) {

                    // Check all base taxonomies for slug match with current slug
                    foreach ($baseTaxonomyTerms as $term) {
                        if ($term->slug == $taxonomyTermSlug) {
                            $currentTaxonomy = $term;
                            continue;
                        }
                    }

                    // Set invalid path if no base taxonomy slugs match the current slug
                    if (!$currentTaxonomy) {
                        $pathIsValid = false;
                        break;
                    }
                }
                // For taxonomy parents check if slug matches current taxonomies parent taxonomy slug
                else {

                    // Ensure that current taxonomy is set and that the current taxonomy has a parent taxonomy
                    if ($currentTaxonomy && $currentTaxonomy->parent > 0) {
                        $currentTaxonomy = get_term($currentTaxonomy->parent, $this->queriableTaxonomies[$postBase]);

                        // Check if the current taxonomies parent slug matches the current url slug
                        if ($currentTaxonomy->slug != $taxonomyTermSlug) {
                            $pathIsValid = false;
                            break;
                        }
                    }
                    elseif ($currentTaxonomy) {
                        continue;
                    }
                    else {
                        $pathIsValid = false;
                        break;
                    }
                }
            }
        }

        // If after traversing through the url slugs there is still a taxonomy with a parent then invalidate url
        if (!empty($currentTaxonomy) && $currentTaxonomy->parent > 0) {
            $pathIsValid = false;
        }

        return [
            'valid'     => $pathIsValid,
            'taxonomy'  => $currentTaxonomy
        ];
    }


    /**
     * This method updates the query according to it's findings and.
     * At this point we do not need to account for errors because we already know that
     * the url is valid.
     *
     * @param \WP_Query $query        - the query Wordpress is currently trying to evaluate
     * @param string    $resourceID   - the resource name as found in the url, identifying the requested resource
     * @param array     $taxonomyType - the taxonomy type associated with this resource
     * @param string    $postType     - the post type associated with this resource
     * @param \WP_Term  $taxonomy     - the matched taxonomy associated with this resource
     *
     * @return \WP_Query
     */
    private function handlePath($query, $resourceID, $taxonomyType, $postType, $taxonomy = null)
    {
        /** \wpdb $wpdb */
        global $wpdb;

        // we need to find out if we are dealing with a taxonomy term or a post, as this information is not transmitted
        $postID = $wpdb->get_var(sprintf(
            'SELECT ID
            FROM %s
            WHERE LOWER(post_name) LIKE "%s"
            AND post_type = "%s"',
            $wpdb->posts,
            esc_sql($resourceID),
            esc_sql($postType)
        ));

        // depending on if it is a post or a taxonomy we update the $query accordingly
        if (is_null($postID)) {

            $index = array_search($taxonomy->taxonomy, $taxonomyType);

            $query->query_vars = [
                $taxonomyType[$index] => $resourceID
            ];
            $query->matchedQuery = $taxonomyType[$index] . '=' . $resourceID;

        } else {
            $query->query_vars = [
                'name'        => $resourceID,
                'post_type'   => $postType,
                $taxonomyType[0] => $postType
            ];
            $query->matchedQuery = $postType . '=' . $resourceID;
        }

        return $query;
    }
}

TaxonomyUrl::getInstance();
