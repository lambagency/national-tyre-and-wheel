<?php
require_once get_template_directory() . '/vendor/autoload.php';

/**
 * Autoload the function files
 * This autoloader will automatically load any php files that are
 * added to the existing function folders.
 *
 * Load order follows:
 *  - Constant variables
 *  - Core files
 *  - Client files
 *
 * Note: Any new folders added to the functions folder will need
 * to be added to the autoloader
 */
function autoloadFramework()
{

    // Core variables
    require_once get_template_directory() . '/framework/constants.php';

    // Autoload paths array, files must have full path
    $autoloadPaths = [
        FRAMEWORK_PATH . 'class/*.php',
        FRAMEWORK_PATH . 'framework/*.php',
        FRAMEWORK_PATH . 'authentication/*.php',
        FRAMEWORK_PATH . 'vendor/*.php',
        FRAMEWORK_PATH . 'admin/*.php',
        FRAMEWORK_PATH . 'theme/*.php',
        FRAMEWORK_PATH . 'post-types/**/*.php',
        COMPONENTS_PATH . '**/Component*.php',
        TEMPLATES_PATH . '**/Template*.php',
    ];

    // Loop through each path and load any matched files
    foreach ($autoloadPaths as $autoloadPath) {
        foreach (glob($autoloadPath) as $filename) {
            require_once $filename;
        }
    }
}

autoloadFramework();
