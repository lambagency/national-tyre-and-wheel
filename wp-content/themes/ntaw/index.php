<?php
get_header();
?>
<div class="grid-container">
  <h1><?php single_post_title(); ?></h1>
</div>
<?php
$module = new LambAgency\Modules\ModulePostListing(['name'=> 'post-listing']);
$module->addModuleClass('no-border');
$fields = $module->getFields();

set_query_var('module', $module);
set_query_var('fields', $fields);
get_template_part('modules/post-listing/post-listing');

get_footer();
