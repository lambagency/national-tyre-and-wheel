<?php
get_header();

if ($page = \LambAgency\Pages\Page404::get404Page()) {
    $content = apply_filters('the_content', get_post_field('post_content', $page->ID));
    echo $content;
} else {
  ?>
  <div class="grid-container">
    <div class="404">
      <h1><?php esc_html_e('Error 404: Page Not Found'); ?></h1>
    </div>
  </div>
  <?php
}

get_footer();
