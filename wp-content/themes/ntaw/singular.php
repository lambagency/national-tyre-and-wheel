<?php
get_header();
if ($wp_query->have_posts()){
    while ($wp_query->have_posts()){
        $wp_query->the_post();
        the_content();
    }
}
get_footer();