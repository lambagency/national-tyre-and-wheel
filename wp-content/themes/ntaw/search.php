<?php
/*
Template Name: Search Page
*/
// FIXME
get_header();

if ($page = \LambAgency\Pages\PageSearch::getSearchPage()) {
    $content = apply_filters('the_content', get_post_field('post_content', $page->ID));
    echo $content;
}

get_footer();
