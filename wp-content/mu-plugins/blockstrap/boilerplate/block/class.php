<?php
namespace LambAgency\Modules;

// use \LambAgency\Components\TitleBlock;
// use \LambAgency\Components\Image;
// use \LambAgency\Components\Button;
// use \LambAgency\Components\Icon;
// use \LambAgency\Components\Link;

/**
 * Class _Module
 *
 * @package LambAgency\Modules
 */
class _Module extends AbstractModule
{
    public function __construct($block)
    {
        parent::__construct($block);
    }
}
