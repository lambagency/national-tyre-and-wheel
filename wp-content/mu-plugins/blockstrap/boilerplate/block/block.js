/* Provides on-demand loading and hot-reloading capabilities to this module */
import AbstractModule from "class/AbstractModule";

export default class _Module extends AbstractModule {
  constructor(m) {
    super(m);
  }
}
